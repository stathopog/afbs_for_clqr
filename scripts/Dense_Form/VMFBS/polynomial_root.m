function alpha = polynomial_root(tilde,v,tau,nU)

p = @(x) (x - sum(v.*max(0,(tilde.lambda-v*x)/tau))); % function handle for polynomial

TPs = sort(tilde.lambda./v); % find transition points of polynomial

if TPs(1) > 0
    alpha = TPs(1) - ( 1 / (p(TPs(1))-p(TPs(1)-1)) ) * p(TPs(1));
elseif TPs(nU) < 0
    alpha = TPs(nU) - ( 1 / (p(TPs(nU)+1)-p(TPs(nU))) ) * p(TPs(nU));
else
    % Bisection for identifying interval
    idx.lower = 1;  idx.upper = nU;
    idx.med = floor((idx.lower+idx.upper)/2);
    med = p(TPs(idx.med));

    while (idx.upper-idx.lower > 1)
        if (med <= 0)
            idx.lower = idx.med;
        elseif (med >= 0) 
            idx.upper = idx.med;
        end
        idx.med = floor((idx.lower+idx.upper)/2);
        med = p(TPs(idx.med));
    end
    
    % Find intersection with x-axis
    alpha = TPs(idx.lower) - ( p(TPs(idx.lower))*(TPs(idx.upper)-TPs(idx.lower)) / (p(TPs(idx.upper))-p(TPs(idx.lower))) );

end