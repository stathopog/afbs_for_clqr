function [n,m,A,B,C,Q,R,N,Cx,Cu,cx,cu,no] = small_random()

% Generating a random system
A = [1.988 -0.998; 1 0]; 
B = [0.125; 0];
n = size(A,1); m = size(B,2);
no.o = n;
C = eye(n);

Q = eye(n); R = 10*eye(m); N = zeros(n,m);

Cx = [C; -C];
Cu = [eye(m); -eye(m)];
no.con.u = size(Cu,1);
no.con.x = size(Cx,1);

% Constraints and matrices
umax = 8; % small
umin = -umax;
ymax = 3;
ymin = -ymax;
cx = [ones(no.o,1)*ymax; -ones(no.o,1)*ymin]; 
cu = [ones(m,1)*umax; -ones(m,1)*umin];