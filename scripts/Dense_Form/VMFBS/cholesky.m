function [temp,LH] = cholesky(hor,Q,S,bar,no,con,n,m,x_init)

temp = [];
temp.barQ = blkdiag(kron(eye(hor.length-1),Q), S);
temp.barR = bar.R(1:hor.length*m, 1:hor.length*m);
temp.barB = bar.B(1:n*hor.length,1:m*hor.length);
temp.barA = bar.A(1:n*hor.length,:);
temp.H = temp.barB'*temp.barQ*temp.barB + temp.barR;
temp.G1 = temp.barA'*temp.barQ*temp.barB;
temp.h = temp.G1'*x_init;
temp.C = con.C(1:no.con.all*hor.length,1:m*hor.length);
temp.c = con.c(1:no.con.all*hor.length,1);
LH = chol(temp.H,'lower');