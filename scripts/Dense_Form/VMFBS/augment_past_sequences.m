function old = augment_past_sequences(no,hor,old,dT,K,rho,A,B,m,n,nU,lambda)

old.lambda = [lambda; zeros(nU-size(lambda,1),1)];
old.hes.H = blkdiag(old.hes.H,rho*eye(no.con.all*dT));
old.hes.B = blkdiag(old.hes.B,1/rho*eye(no.con.all*dT));
% old.hes.H = blkdiag(rho*eye(no.con.all*hor.length));
% old.hes.B = blkdiag(1/rho*eye(no.con.all*hor.length));
old.v = [old.v; zeros(no.con.all*dT,1)];
old.u = reshape(old.u,m,old.hor.length);
% forward simulate the old trajectory x^{k-1} to get u^{k-1} up to T^k
old.u = [old.u zeros(m,dT)];    old.x = [old.x zeros(n,dT)];
for i = old.hor.length+1:hor.length
    old.u(:,i) = -K * old.x(:,i);
    old.x(:,i+1) = A*old.x(:,i) + B*old.u(:,i);
end

old.u = reshape(old.u,m*hor.length,1);

% s = con.C(1:nU,1:m*hor.length)' * ( lambda(1:nU,1) - old.lambda(1:nU,1) );
% 
% y = u(1:m*hor.length,1)-old.u(1:m*hor.length,1);

