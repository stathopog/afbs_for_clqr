function [hes,v,tau] = SR1(k,s,y,nU,rho)

gamma = 0.8;    tau = rho;
if k == 1
    D = tau*eye(nU);
    v = zeros(nU,1);
else
    tau_BB = (s'*y)/(y'*y);
    tau_BB = min(1e+10,max(1e-10,tau_BB)); % projection step
    tau = gamma*tau_BB;
    D = tau*eye(nU);
    if ( (s-D*y)'*y <= 1e-8*norm(y)*norm(s-D*y) || (s-D*y)'*y >= 1e+8*norm(y)*norm(s-D*y) )
        v = zeros(nU,1);
    else
        v = (s-D*y) / sqrt((s-D*y)'*y);
    end
end
hes.H = D + (v*v');

% % V = []; Lambda = [];
% % [V,Lambda] = eig(hes.H);
% % hes.H = V*(Lambda/rho)*V';

% Sherman - Morrison
hes.B = (1/tau)*eye(nU) - (1 / (tau^2*(1+(1/tau)*(v'*v)))) * (v*v');
% hes.B = inv(hes.H);