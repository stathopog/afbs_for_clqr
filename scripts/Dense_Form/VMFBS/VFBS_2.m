function [k,x,u,lambda,hor,time,res] = VFBS_2(N,x_init,K,A,B,Q,omega,Cx,Cu,cx,cu,S,n,m,no,time,bar,rho,con)

verbose = 3;
optTol1 = 1e-4;
% optTol2 = 1e-9;
% eps.abs = 1e-6;
% eps.rel = 1e-4;
MAXITER = 1e3;
suffDec = 1e-8;
adjustStep = 1;
progTol = 1e-9;

    for k = 1:MAXITER

    %% step 1
        switch (k)
             case 1  %@ first iteration solve unconstrained LQR
                hor.sim = N;
                hor.length = 0;
                x = []; u = []; 
                z(:,1) = x_init;
                for i = 1:hor.sim
                    v(:,i) = -K*z(:,i);
                    z(:,i+1) = A*z(:,i) + B*v(:,i);
                end
                
                
                values.check = max(omega.ineq_1*z - repmat(omega.ineq_2,1,hor.sim+1),[],1);
                hor.check = max(find(values.check>0));
                if isempty(hor.check), hor.check = 2; end
                values.length1 = max(Cx*z - repmat(cx,1,hor.sim+1),[],1);   
                values.length2 = max(Cu*v - repmat(cu,1,hor.sim),[],1);
                hor.length1 = max(find(values.length1>0));   
                hor.length2 = max(find(values.length2>0));
                if isempty(hor.length1), hor.length1 = 2; end
                if isempty(hor.length2), hor.length2 = 2; end
                hor.length = max(hor.length1, hor.length2);
                
                
                x = z(:,1:hor.length+1);  u = v(:,1:hor.length); 
                lambda = zeros(no.con.all*hor.length,1);
                s = zeros(hor.length*m,1); y = zeros(hor.length*m,1);
                old.lambda = lambda;
                u = reshape(u,m*hor.length,1);
                old.hor.length = hor.length;
                old.u = u;

                % cholesky for y computation
                [temp,LH] = cholesky(hor,Q,S,bar,no,con,n,m,x_init);

            otherwise  %@ subsequent iterations 
                old.hes.H = [];
                old.hes.H = hes.H;    old.hes.B = hes.B;    old.v = v;
                old.u = []; old.x = [];
                old.x = x; old.u = u;
                old.hor.length = hor.length;
                old.h = h; old.g = g;
                lambda = reshape(lambda,nU,1);

                    
                u = LH' \ (LH \ (temp.C'*lambda-temp.h));
                
                % Forward propagation of the dynamics for [hor.length+1,Ts]
                % and computation of running maximum of T(i)
                [hor,u,x] = sim_sys_dyns(old,omega,Cx,Cu,cx,cu,K,A,B,x_init,u,m,hor);
                
                %% keep latest horizon
                hor.length = max(old.hor.length,hor.length); %?
                hor.length_track(k) = hor.length;
                
                if (mod(k,15) == 0)
                    time.x{time.count} = x;
                    time.count = time.count + 1;
                end   
        end
                
                
        %% step 2
        dT = hor.length-old.hor.length;
        nU = no.con.all*hor.length;
        
        % augment_past_sequences
        if dT > 0
            old = augment_past_sequences(no,hor,old,dT,K,rho,A,B,m,n,nU,lambda);
            [temp,LH] = cholesky(hor,Q,S,bar,no,con,n,m,x_init);
        else
            old.lambda = lambda;
        end
        
        % evaluate function and gradient
        [g,h] = funObj(temp,LH,u,old.lambda);

        lambda = [];
        if ( (k==1) || (~any(v)) )
            hat.lambda = min(0,old.lambda - rho*g);
        else
            tilde.lambda = old.lambda - old.hes.H*g;
            alpha(k) = polynomial_root(tilde,old.v,tau,nU);
            hat.lambda = min(0,tilde.lambda-old.v*alpha(k));
        end
%         
%         % line search
%         [f,g,lambda] = line_search(g,f,temp,u,hat,LH,old,k,verbose,suffDec,adjustStep,progTol);
% [f,g,lambda] = line_search_2(g,f,temp,u,hat,LH,old,verbose,suffDec,progTol);
        lambda = hat.lambda;
        
        % check violation length
%         lambda = reshape(lambda,no.con.all,hor.length);
%         values.lambda = max(abs(lambda),[],1);
%         hor.violate = max(find(values.lambda>1e-15));
%         disp(sprintf('Violation length and entry length are %d,%d', hor.length,hor.check));
%         lambda = reshape(lambda,nU,1);

        %% terminate 
        if k > 10
% %             res(k) = norm((u-old.u)/u);
% %             res1 = sqrt(hor.length*m)*eps.abs + eps.rel*norm(u-old.u);   res2 = abs((f-old.f)/f);
            res = norm(old.hes.B*(-hat.lambda+old.lambda));  % generalized gradient check
            if  ( res < optTol1 ), break; end
%             (res1 < optTol1) || ( res2 < optTol2) ||
        end

        % compute SR1 update
        s = lambda - old.lambda;
        y = temp.C * (LH' \ (LH \ (temp.C'*(lambda-old.lambda))));
        [hes,v,tau] = SR1(k,s,y,nU,rho);

    end
end
        
        
        