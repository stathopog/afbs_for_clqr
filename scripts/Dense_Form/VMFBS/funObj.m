    function [g,h] = funObj(temp,LH,u,x)
        g = temp.C * u - temp.c;
        h = .5*(temp.C'*x)'*(LH' \ (LH \ ((temp.C'*x)))) - (temp.C'*x)'*(LH' \ (LH \ temp.h)) - x'*temp.c + .5*temp.h'*(LH' \ (LH \ temp.h)); % actually h-r
    end