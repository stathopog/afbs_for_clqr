%% Infinite horizon MPC using ADMM - main

clear all;
close all;
randn('state',4);
rand('state',0);

MAXITER = 2e2;

%% Defining the problem parameters
% %small system
% [n,m,A,B,C,Q,R,N,Cx,Cu,cx,cu,no] = small_random;
% all.x_init = load('../Data/X_INIT_2states');

%quad model
[n,m,A,B,C,Q,R,N,Cx,Cu,cx,cu,no] = quad_mpc;
% all.x_init = load('../../Data/X_INIT_quad');
   

%% Compute invariant set
[K,S,E] = dlqr(A,B,Q,R,N);
H = [Cx; -Cu*K]; h = [cx; cu];
Omega{1} = polytope(Cx,cx);
Omega{2} = polytope(H,h);
% add_ineq = H*(A-B*K);
%pre{1} = polytope(add_ineq,h);
i = 2;
omega.ineq_1 = H;
% % omega.ineq_1 = [omega.ineq_1; add_ineq];
% % omega.ineq_2 = [h;h];
% Omega{2} = polytope(omega.ineq_1,omega.ineq_2);
while ~(Omega{i} == Omega{i-1})
    add_ineq = H*(A-B*K)^(i-1);
    omega.ineq_1 = [omega.ineq_1; add_ineq];
    i = i+1;
    omega.ineq_2 = repmat(h,i-1,1);
    Omega{i} = polytope(omega.ineq_1,omega.ineq_2);
end
count = i;
[Hf,hf] = double(Omega{count});

X_INIT = load('../../Data/X_INIT_quad_dense');
HTs = zeros(size(X_INIT.X_INIT2,2),1);
ITERs = zeros(size(X_INIT.X_INIT2,2),1);
for j = 1:size(X_INIT.X_INIT2,2)
    j=5;
    x_init = X_INIT.X_INIT2(:,j);
    % % % choose x_init
    % % x_init = all.x_init.X_INIT(:,6);


    %% Dense formulation
    N = 150;  noise = 0*randn(n,N);
    bar.Cx = sparse(kron(eye(N),Cx));  bar.Cu = sparse(kron(eye(N),Cu));
    bar.Q =  blkdiag(kron(eye(N-1),Q), S);   bar.R = kron(eye(N),R);
    bar.A = A;
    for i = 2:N
        bar.A = [bar.A; A^i];
    end
    bar.B_coln = B;
    for i = 1:N-1
        bar.B_coln = [bar.B_coln; A^i*B];
    end
    bar.B = [bar.B_coln zeros(N*n,(N-1)*m)];
    for i = 1:N-1
        bar.B(:,i*m+1:(i+1)*m) = [zeros(i*n,m); bar.B_coln(1:end-i*n,:)];
    end
    noise_bar_coln = 0*eye(n);
    for i = 1:N-1
        noise_bar_coln = [noise_bar_coln; A^i];
    end
    noise_bar = [noise_bar_coln zeros(N*n,(N-1)*n)];
    for i = 1:N-1
        noise_bar(:,i*n+1:(i+1)*n) = [zeros(i*n,n); noise_bar_coln(1:end-i*n,:)];
    end
    H = bar.B'*bar.Q*bar.B + bar.R;
    G1 = bar.A'*bar.Q*bar.B; G2 = noise_bar'*bar.Q*bar.B;
    F1 = bar.A'*bar.Q*bar.A + Q; F2 = noise_bar'*bar.Q*noise_bar;
    % con.C = [bar.Cu; bar.Cx*bar.B];
    bar.cx = repmat(cx,N,1);  bar.cu = repmat(cu,N,1);
    % con.c = [bar.cu; bar.cx-bar.Cx*bar.A*x_init-bar.Cx*noise_bar*reshape(noise,n*N,1)];
    bar.Cxx = bar.Cx*bar.B;
    bar.cxx = bar.cx-bar.Cx*bar.A*x_init-bar.Cx*noise_bar*reshape(noise,n*N,1);
    con.C=[];   con.c = [];
    for i = 1:N
        con.C = [con.C; bar.Cu((i-1)*no.con.u+1:i*no.con.u,:); bar.Cxx((i-1)*no.con.x+1:i*no.con.x,:)];
        con.c = [con.c; bar.cu((i-1)*no.con.u+1:i*no.con.u,:); bar.cxx((i-1)*no.con.x+1:i*no.con.x,:)];
    end
    r = x_init'*F1*x_init + reshape(noise,n*N,1)'*F2*reshape(noise,n*N,1);
    h = G1'*x_init + G2'*reshape(noise,n*N,1);
    no.con.all = no.con.u+no.con.x;

    %% Compute AMA parameters
    tilde.Cx = [Cx; zeros(no.con.u,n)];
    tilde.Cu = [zeros(no.con.x,m);Cu];
    tilde.Acal = [tilde.Cx tilde.Cu];
    tilde.Acal = con.C;
    tau = max(eig(inv(H))); % Lipschitz constant of f*(u)
    Mu = max(svds(tilde.Acal')); % Bound on Acal operator
    rho = 1/(tau*Mu^2); % ama stepsize
%     rho=.5;

    % % % %% Verification
    % % % tic
    % % % cvx_begin
    % % % cvx_solver 'sdpt3'
    % % %     variables U_d(N*m)
    % % %     minimize ( .5*quad_form(U_d,H, 2*h,r) );
    % % %     subject to 
    % % %     con.C*U_d <= con.c;
    % % % %     norm(U_d,Inf)<=umax;
    % % % cvx_end
    % % % toc  
    % % % U_d = reshape(U_d,m,N);
    % % % X_d = [];
    % % %     X_d(:,1) = x_init;
    % % %     for t = 1:N
    % % %         X_d(:,t+1) = A*X_d(:,t) + B*U_d(:,t);
    % % %     end
    % % % pause;


    tic
    cvx_begin
    cvx_solver 'sdpt3'
        variables X(n,N+1) U(m,N)
        obj = 0;
        for t = 1:N
            obj = obj + .5*quad_form([X(:,t); U(:,t)], blkdiag(Q,R));
        end
        obj = obj + .5*quad_form(X(:,N+1), S);
        minimize ( obj )
        subject to 
        for t = 1:N
            X(:,t+1) == A*X(:,t) + B*U(:,t);
            Cx*X(:,t)<= cx;
            Cu*U(:,t)<= cu;
        end
        X(:,1) == x_init;
        Cx*X(:,N+1) <= cx;
    cvx_end
    toc
    U = reshape(U,N*m,1);

% %     %% solve dual with becker
% %     LH = chol(H,'lower');
% %     lambda = zeros(no.con.all*N,1); old.lambda = lambda;
% %     u = zeros(m*N,1);   old.u = u;
% %     nU = no.con.all*N;
% %     % nU = m*N;
% %     alpha = [];
% %     tic
% %     for k = 1:MAXITER 
% %         u = LH' \ (LH \ (con.C'*lambda-h));
% %         old.lambda = lambda;
% %         if ( (k==1) || (~any(v)) )
% %             lambda = old.lambda - rho*(con.C * u - con.c);
% %         else
% %             lambda = old.lambda - hes.H*(con.C * u - con.c);
% %                 tilde.lambda = lambda;
% %             alpha(k) = polynomial_root(tilde,v,tau,nU);
% %                 lambda = min(0,lambda-v*alpha(k)); 
% %         end    
% %         s = lambda - old.lambda;
% %         y = con.C* (LH' \ (LH \ (con.C'*s)));
% %         [hes,v,tau] = SR1(k,s,y,nU,rho);
% %     %         hes.B = 1/rho;    hes.H = rho;
% %         diff = .5*s'*y - (con.C*(LH' \ (LH \ h)) - con.c)'*s;
% %         object = .5*(con.C'*lambda)'*(LH' \ (LH \ ((con.C'*lambda)))) - (con.C*(LH' \ (LH \ h)) - con.c)'*lambda;
% %         if (norm(diff/object) <= 1e-10)
% %                 break;
% %         end
% %     end
% %     toc
% %     orig.lambda = lambda;
% %     ulong = LH' \ (LH \ (con.C'*orig.lambda-h));


    %% VFBS  
%     HTs = zeros(size(all.x_init.X_INIT,2),1);
%     ITERs = zeros(size(all.x_init.X_INIT,2),1);
    % for j = 1:size(all.x_init.X_INIT,2)
%         x_init = all.x_init.X_INIT(:,j);
        time.x = {};
        time.count = 1;  
        %% Run VMAMA
        tic
        [k,x,u,lambda,hor,time,res] = VFBS_2(N,x_init,K,A,B,Q,omega,Cx,Cu,cx,cu,S,n,m,no,time,bar,rho,con);
        toc
        HTs(j) = hor.length;
        ITERs(j) = k;
        
        fprintf('COMPARISONS     \n');
%         MAT{j} = [U(1:HTs(j)*m,1) ulong(1:HTs(j)*m,1) u];
%         fprintf('%10.4f\t%10.4f\t%10.4f\n', MAT{j}');
        MAT{j} = [U(1:HTs(j)*m,1) u];
        fprintf('%10.4f\t%10.4f\n', MAT{j}');
        fprintf('HITTING TIMES AND ITERATION COUNT     \n');
        fprintf('%3d\t%3d\n', HTs(j), ITERs(j));
        pause;
end



