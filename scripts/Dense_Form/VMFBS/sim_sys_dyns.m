function [hor,u,x] = sim_sys_dyns(old,omega,Cx,Cu,cx,cu,K,A,B,x_init,u,m,hor)

% Forward propagation of the dynamics for [hor.length+1,Ts] 
u = reshape(u,m,old.hor.length);
x(:,1) = x_init;
for i = 1:old.hor.length
    x(:,i+1) = A*x(:,i) + B*u(:,i);
end
i = old.hor.length;
while ( max(omega.ineq_1*x(:,i) - omega.ineq_2) > 0 )
    i = i + 1;
    hor.check = i;
    u(:,i)   = -K*x(:,i);
    x(:,i+1) = A*x(:,i) + B*u(:,i);
end
hor.check = i;
for i = hor.length:hor.check
        if ( (max(Cx*x(:,i) - cx) <= 0) && (max(Cu*u(:,i) - cu) <= 0) )
        hor.length = i;
        end
end
u = reshape(u,m*hor.length,1);


% add violation horizon

