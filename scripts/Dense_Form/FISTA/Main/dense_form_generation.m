function [bar,con,H,F1,G1,G2] = dense_form_generation(T,dat,set)

% Generated the dense form MPC problem for a given horizon
if strcmp(set,'Terminal')
    bar.Cx = sparse(blkdiag(kron(eye(T-1),dat.Cx),dat.Hf));
    bar.cx = [repmat(dat.cx,T-1,1); dat.hf];  
elseif strcmp(set,'No Terminal')
    bar.Cx = sparse(kron(eye(T),dat.Cx));
    bar.cx = repmat(dat.cx,T,1);  
else
    error('Provide description: Terminal or No Terminal')
end
    
bar.Cu = sparse(kron(eye(T),dat.Cu));
bar.Q =  blkdiag(kron(eye(T-1),dat.Q), dat.S);   bar.R = kron(eye(T),dat.R);
bar.A = dat.A;
for i = 2:T
    bar.A = [bar.A; dat.A^i];
end
bar.B_coln = dat.B;
for i = 1:T-1
    bar.B_coln = [bar.B_coln; dat.A^i*dat.B];
end
bar.B = [bar.B_coln zeros(T*dat.nx,(T-1)*dat.nu)];
for i = 1:T-1
    bar.B(:,i*dat.nu+1:(i+1)*dat.nu) = [zeros(i*dat.nx,dat.nu); bar.B_coln(1:end-i*dat.nx,:)];
end
noise_bar_coln = 0*eye(dat.nx);
for i = 1:T-1
    noise_bar_coln = [noise_bar_coln; dat.A^i];
end
bar.noise = [noise_bar_coln zeros(T*dat.nx,(T-1)*dat.nx)];
for i = 1:T-1
    bar.noise(:,i*dat.nx+1:(i+1)*dat.nx) = [zeros(i*dat.nx,dat.nx); noise_bar_coln(1:end-i*dat.nx,:)];
end
H = bar.B'*bar.Q*bar.B + bar.R;
G1 = bar.A'*bar.Q*bar.B; G2 = bar.noise'*bar.Q*bar.B;
F1 = bar.A'*bar.Q*bar.A + dat.Q;
bar.cu = repmat(dat.cu,T,1);
bar.Cxx = bar.Cx*bar.B;
con.C = []; 
for i = 1:T-1
    con.C = [con.C; bar.Cu((i-1)*dat.no.con.u+1:i*dat.no.con.u,:); bar.Cxx((i-1)*dat.no.con.x+1:i*dat.no.con.x,:)];
end
con.C = [con.C; bar.Cu((T-1)*dat.no.con.u+1:T*dat.no.con.u,:); bar.Cxx((T-1)*dat.no.con.x+1:end,:)];