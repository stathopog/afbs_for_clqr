function [] = PostProcessToyStats()
clear CompHorzs; clear CompCost; clear CompItersWTC; clear count; clear CompItersWOTC; 
clear PlotHorzs; clear PlotItersWTC; clear PlotItersWOTC; clear PlotItersCLQR;
load('MPC_Stats_Toy_alpha4.mat');
load('CLQR_Weights_Stats_Toy_alpha25');
CLQRW = CLQR;
load('CLQR_Stats_Toy_alpha4.mat');
hold;
count=1;
for j = 2:2:8
    switch j
        case {2}
            for i = 1:nnz(MPC.HORZS(2,:))
                CompHorzs(count,:) = [7 MPC.HORZS(2,i) CLQR.HORZS(2,i) CLQRW.HORZS(2,i) 14]; % [Tmin, T*, Tinf, 2Tmin]
                CompCost(count,:) = [7 MPC.J{2,i}(7) MPC.J{2,i}(MPC.HORZS(2,i)) CLQR.J(2,i) CLQRW.J(2,i)]; 
                CompItersWTC(count,:) = [7 MPC.HORZS(2,i) MPC.ITERS_WTC{j,i}(7) MPC.ITERS_WTC{j,i}(MPC.HORZS(2,i))...
                   MPC.ITERS_WTC{j,i}(14) CLQR.ITERS(2,i) CLQRW.ITERS(2,i)];
                CompItersWOTC(count,:) = [7 MPC.HORZS(2,i) MPC.ITERS_WOTC{j,i}(7) MPC.ITERS_WOTC{j,i}(MPC.HORZS(2,i))...
                   MPC.ITERS_WOTC{j,i}(14) CLQR.ITERS(2,i) CLQRW.ITERS(2,i)];
                count=count+1;
            end
            countTstar = 1;
            for iii = 7:max(MPC.HORZS(j,:))
                idx = find(CompItersWTC(:,2)==iii);
                if ~isempty(idx)
                PlotHorzs(countTstar,:) = [mean(CompHorzs(idx,3))/mean(CompHorzs(idx,2))...
                    mean(CompHorzs(idx,4))/mean(CompHorzs(idx,2))...
                    mean(CompHorzs(idx,1))/mean(CompHorzs(idx,2))];
                PlotItersWTC(countTstar,:) = [iii mean(CompItersWTC(idx,3))...
                    mean(CompItersWTC(idx,4)) mean(CompItersWTC(idx,5))];
                PlotItersWOTC(countTstar,:) = [iii mean(CompItersWOTC(idx,3))...
                    mean(CompItersWOTC(idx,4)) mean(CompItersWOTC(idx,5))];
                PlotItersCLQR(countTstar,:) = [iii mean(CLQR.ITERS(j,idx)) mean(CLQRW.ITERS(j,idx))];
                countTstar = countTstar + 1;
                end
            end
        case {4}
            for i = 1:nnz(MPC.HORZS(4,:))
                CompHorzs(count,:) = [15 MPC.HORZS(j,i) CLQR.HORZS(j,i) CLQRW.HORZS(j,i) 30]; % [Tmin, T*, Tinf, 2Tmin]
                CompCost(count,:) = [15 MPC.J{j,i}(15) MPC.J{j,i}(MPC.HORZS(j,i)) CLQR.J(j,i) CLQRW.J(j,i)]; 
                CompItersWTC(count,:) = [15 MPC.HORZS(j,i) MPC.ITERS_WTC{j,i}(15) MPC.ITERS_WTC{j,i}(MPC.HORZS(j,i))...
                   MPC.ITERS_WTC{j,i}(30) CLQR.ITERS(j,i) CLQRW.ITERS(j,i)];
                CompItersWOTC(count,:) = [15 MPC.HORZS(j,i) MPC.ITERS_WOTC{j,i}(15) MPC.ITERS_WOTC{j,i}(MPC.HORZS(j,i))...
                   MPC.ITERS_WOTC{j,i}(30) CLQR.ITERS(j,i) CLQRW.ITERS(j,i)];
                count=count+1;
            end 
            for iii = 15:max(MPC.HORZS(j,:))
                idx = find(CompItersWTC(:,2)==iii);
                if ~isempty(idx)
                PlotHorzs(countTstar,:) = [mean(CompHorzs(idx,3))/mean(CompHorzs(idx,2))...
                    mean(CompHorzs(idx,4))/mean(CompHorzs(idx,2))...
                    mean(CompHorzs(idx,1))/mean(CompHorzs(idx,2))];
                PlotItersWTC(countTstar,:) = [iii mean(CompItersWTC(idx,3))...
                    mean(CompItersWTC(idx,4)) mean(CompItersWTC(idx,5))];
                PlotItersWOTC(countTstar,:) = [iii mean(CompItersWOTC(idx,3))...
                    mean(CompItersWOTC(idx,4)) mean(CompItersWOTC(idx,5))];
                PlotItersCLQR(countTstar,:) = [iii mean(CompItersWOTC(idx,6)) mean(CompItersWOTC(idx,7))];
                countTstar = countTstar + 1;
                end
            end
        case {6}
            for i = 1:nnz(MPC.HORZS(6,:))
                CompHorzs(count,:) = [22 MPC.HORZS(j,i) CLQR.HORZS(j,i) CLQRW.HORZS(j,i) 44]; % [Tmin, T*, Tinf, 2Tmin]
                CompCost(count,:) = [22 MPC.J{j,i}(22) MPC.J{j,i}(MPC.HORZS(j,i)) CLQR.J(j,i) CLQRW.J(j,i)]; 
                CompItersWTC(count,:) = [22 MPC.HORZS(j,i) MPC.ITERS_WTC{j,i}(22) MPC.ITERS_WTC{j,i}(MPC.HORZS(j,i))...
                   MPC.ITERS_WTC{j,i}(44) CLQR.ITERS(j,i) CLQRW.ITERS(j,i)];
                CompItersWOTC(count,:) = [22 MPC.HORZS(j,i) MPC.ITERS_WOTC{j,i}(22) MPC.ITERS_WOTC{j,i}(MPC.HORZS(j,i))...
                   MPC.ITERS_WOTC{j,i}(44) CLQR.ITERS(j,i) CLQRW.ITERS(j,i)];
                count=count+1;
            end
            for iii = 22:max(MPC.HORZS(j,:))
                idx = find(CompItersWTC(:,2)==iii);
                if ~isempty(idx)
                PlotHorzs(countTstar,:) = [mean(CompHorzs(idx,3))/mean(CompHorzs(idx,2))...
                    mean(CompHorzs(idx,4))/mean(CompHorzs(idx,2))...
                    mean(CompHorzs(idx,1))/mean(CompHorzs(idx,2))];
                PlotItersWTC(countTstar,:) = [iii mean(CompItersWTC(idx,3))...
                    mean(CompItersWTC(idx,4)) mean(CompItersWTC(idx,5))];
                PlotItersWOTC(countTstar,:) = [iii mean(CompItersWOTC(idx,3))...
                    mean(CompItersWOTC(idx,4)) mean(CompItersWOTC(idx,5))];
                PlotItersCLQR(countTstar,:) = [iii mean(CompItersWOTC(idx,6)) mean(CompItersWOTC(idx,7))];
                countTstar = countTstar + 1;
                end
            end
        case {8}
            for i = 1:nnz(MPC.HORZS(8,:))
                CompHorzs(count,:) = [29 MPC.HORZS(j,i) CLQR.HORZS(j,i) CLQRW.HORZS(j,i) 58]; % [Tmin, T*, Tinf, 2Tmin]
                CompCost(count,:) = [29 MPC.J{j,i}(29) MPC.J{j,i}(MPC.HORZS(j,i)) CLQR.J(j,i) CLQRW.J(j,i)]; 
                CompItersWTC(count,:) = [29 MPC.HORZS(j,i) MPC.ITERS_WTC{j,i}(29) MPC.ITERS_WTC{j,i}(MPC.HORZS(j,i))...
                   MPC.ITERS_WTC{j,i}(58) CLQR.ITERS(j,i) CLQRW.ITERS(j,i)];
                CompItersWOTC(count,:) = [29 MPC.HORZS(j,i) MPC.ITERS_WOTC{j,i}(29) MPC.ITERS_WOTC{j,i}(MPC.HORZS(j,i))...
                   MPC.ITERS_WOTC{j,i}(58) CLQR.ITERS(j,i) CLQRW.ITERS(j,i)];
                count=count+1;
            end
            for iii = 29:max(MPC.HORZS(j,:))
                idx = find(CompItersWTC(:,2)==iii);
                if ~isempty(idx)
                PlotHorzs(countTstar,:) = [mean(CompHorzs(idx,3))/mean(CompHorzs(idx,2))...
                    mean(CompHorzs(idx,4))/mean(CompHorzs(idx,2))...
                    mean(CompHorzs(idx,1))/mean(CompHorzs(idx,2))];
                PlotItersWTC(countTstar,:) = [iii mean(CompItersWTC(idx,3))...
                    mean(CompItersWTC(idx,4)) mean(CompItersWTC(idx,5))];
                PlotItersWOTC(countTstar,:) = [iii mean(CompItersWOTC(idx,3))...
                    mean(CompItersWOTC(idx,4)) mean(CompItersWOTC(idx,5))];
                PlotItersCLQR(countTstar,:) = [iii mean(CompItersWOTC(idx,6)) mean(CompItersWOTC(idx,7))];
                countTstar = countTstar + 1;
                end
            end
    end
end

% plotting iterations
figure();
P1 = semilogy(PlotItersWTC(:,1),PlotItersWTC(:,2));
hold on;
P2 = semilogy(PlotItersWOTC(:,1),PlotItersWOTC(:,2),'--');
P3 = semilogy(PlotItersWTC(:,1),PlotItersWTC(:,3),'m');
P4 = semilogy(PlotItersWOTC(:,1),PlotItersWOTC(:,3),'m--');
P5 = semilogy(PlotItersWTC(:,1),PlotItersWTC(:,4),'r');
P6 = semilogy(PlotItersWOTC(:,1),PlotItersWOTC(:,4),'r--');
P7 = semilogy(PlotItersCLQR(:,1),PlotItersCLQR(:,3),'k-*');
P8 = semilogy(PlotItersCLQR(:,1),PlotItersCLQR(:,2),'k-o');
hold off;
hcg1 = title('Iteration count comparison');
% axis([-7 1 -0.1 0.7]) 
xlabcg1 = xlabel('Optimal horizon T^\ast'); ylabcg1 = ylabel('Average No. of iters');
set(hcg1,'Interpreter','Tex');  
set(xlabcg1,'Interpreter','Tex');
set(ylabcg1,'Interpreter','Tex');
hleg1 = legend([P1,P3,P5,P7,P8],'T_{min} w/ T.C.',...
    'T^\ast w/ T.C.','2T_{min} w/ T.C.',...
    'T_w^{\infty} CLQR','T^{\infty} CLQR');
set(hcg1, 'FontSize', 16);
set(xlabcg1, 'FontSize', 16);
set(ylabcg1, 'FontSize', 16);
set(hleg1,'Interpreter','Tex','FontSize', 12);

% plotting horizon ratio
figure();
P1 = plot(PlotItersWTC(:,1),PlotHorzs(:,1),'k--o');
hold on;
P2 = plot(PlotItersWTC(:,1),PlotHorzs(:,2),'k--*');
P3 = plot(PlotItersWTC(:,1),PlotHorzs(:,3),'--^');
hold off;
hcg2 = title('Horizons length comparison');
xlabcg2 = xlabel('Optimal horizon T^\ast'); ylabcg2 = ylabel('Ratio T / T^\ast');
set(hcg2,'Interpreter','Tex');  
set(xlabcg2,'Interpreter','Tex');
set(ylabcg2,'Interpreter','Tex');
hleg2 = legend([P1,P2,P3],'T^\infty / T^\ast', 'T_{w}^\infty / T^\ast', 'T_{min} / T^\ast');
set(hcg2, 'FontSize', 16);
set(xlabcg2, 'FontSize', 16);
set(ylabcg2, 'FontSize', 16);
set(hleg2,'Interpreter','Tex','FontSize', 12);


end
    

