function [dat,model] = quad_mpc()

quad = load('../../../Data/quadForGiorgos');
dat.nx = 12; dat.nu = 4;
quad.Cc = eye(dat.nx); quad.Dc = zeros(dat.nx,dat.nu);
system = ss(quad.Ac,quad.Bc,quad.Cc,quad.Dc);
systemd = c2d(system,.1,'zoh');
[dat.A,dat.B,dat.C,~] = ssdata(systemd);
dat.Cx = kron([1;-1],eye(dat.nx));
dat.cx = [kron([1e0;1e0],ones(5,1)); 1; 1;...
      kron([10*(pi/180); 10*(pi/180)],ones(2,1));...
      pi; pi; kron([15*(pi/180); 15*(pi/180)],ones(2,1));...
      60*(pi/180); 60*(pi/180)];
dat.Cu = kron([1;-1],eye(dat.nu));
dat.cu = kron([.3;.7], ones(dat.nu,1));

dat.Q = 2*(dat.C'*dat.C+1e-4*eye(dat.nx));   
dat.R = 2*eye(dat.nu);
dat.N = zeros(dat.nx,dat.nu);

dat.no.con.u = size(dat.Cu,1);
dat.no.con.x = size(dat.Cx,1);
dat.no.con.all = dat.no.con.u+dat.no.con.x;  

%% Compute invariant set
[dat.K,dat.P,~] = dlqr(dat.A,dat.B,0.5*dat.Q,0.5*dat.R,dat.N);
dat.S = 2*dat.P;
H = [dat.Cx; -dat.Cu*dat.K]; h = [dat.cx; dat.cu];
Omega = Polyhedron(H,h);
% Compute the maximal positively invariant set
iii = 1;
while 1
	Omegaprev = Omega;
	F = Omega.H(:,1:end-1);
    f = Omega.H(:,end);
	% Compute the pre-set
	Omega = Polyhedron([F;F*(dat.A-dat.B*dat.K)],[f;f]);
    Omega.minHRep();
	if Omega == Omegaprev, break; end
	iii = iii + 1;
end
dat.Omega = Omega;

% MPT3 form and terminal cost and set computation
model = LTISystem('A', dat.A, 'B', dat.B);
model.u.max = 0.3*ones(dat.nu,1);
model.u.min = -0.7*ones(dat.nu,1);
Px = Polyhedron('A',dat.Cx,'b',dat.cx);
model.x.with('setConstraint');
model.x.setConstraint = Px;
% model.setDomain('x',Polyhedron(Cx,cx));
model.x.penalty = QuadFunction(0.5*dat.Q,zeros(1,dat.nx),0);
model.u.penalty = QuadFunction(0.5*dat.R,zeros(1,dat.nu),0);

P = model.LQRPenalty;
% TSet = model.LQRSet;
% TSet.minHrep();
TSet = dat.Omega;
model.x.with('terminalPenalty');
model.x.with('terminalSet');
model.x.terminalPenalty = P;
model.x.terminalSet = TSet;
% S = 2 * P.H;
dat.Hf = model.x.terminalSet.H(:,1:end-1);
dat.hf = model.x.terminalSet.H(:,end);
no.con.xf = length(dat.hf);