function plotting(n, m, X, x, x_init, A, B, K, Omega, quad_system, fixed_x_init, value_function_compute, toy_system, warm, Cx,cx,Cu,cu, Jonline)

if toy_system == 1
    load('warm_toy_05.mat');
    load('cold_toy_05.mat');
    warm = warm_toy;
    cold = cold_toy;
else
    load('warm_05.mat');
    load('cold_05.mat');
end
load('function_values_cplex.mat');

%% Histogram of hitting times & iteration count for some initial conditions
figure(2);
hist(CLQR.HORZS);
hcg2 = title('Hitting times distribution');
xlabcg2 = xlabel('Hitting times T^\infty'); ylabcg2 = ylabel('# of problems');
set(hcg2,'Interpreter','Tex');  
set(xlabcg2,'Interpreter','Tex');
set(ylabcg2,'Interpreter','Tex');
set(get(gca,'child'),'FaceColor',0.35*[1 1 1],'EdgeColor','k');
set(hcg2, 'FontSize', 14);
muHTs = mean(CLQR.HORZS); %The mean
%Overlay the mean
hold on
plot([muHTs,muHTs],ylim,'k--','LineWidth',2)
hold off

figure(3);
idx = find(cold.ITERs~=2e4);
hist(cold.ITERs(idx));
hcg3 = title('Iterations needed for convergence');
xlabcg3 = xlabel('No. of iterations'); ylabcg3 = ylabel('$\#$ of problems');
set(hcg3,'Interpreter','Latex');  
set(xlabcg3,'Interpreter','Latex');
set(ylabcg3,'Interpreter','Latex');
set(get(gca,'child'),'FaceColor',0.35*[1 1 1],'EdgeColor','k');
set(hcg3, 'FontSize', 14);
muITERs = mean(cold.ITERs(idx)); %The mean
%Overlay the mean
hold on
plot([muITERs,muITERs],ylim,'k--','LineWidth',2)
hold off

%% Value function
if quad_system == 0
    figure(4);
    S1 = semilogy(J{458}(1,:),'k','LineWidth', 2);
    hold on
    S2 = semilogy([zeros(cold.HTs(458)-1,1); ones(29-cold.HTs(458)+1,1)*cold.value_fnct(458)], 'r', 'LineWidth', 2);
    hold off
    hcg4 = title('Value function for increasing horizon');
    xlabcg4 = xlabel('Horizon length'); ylabcg4 = ylabel('$J^\star(x_0)$');
    set(hcg4,'Interpreter','Latex');  
    set(xlabcg4,'Interpreter','Latex');
    set(ylabcg4,'Interpreter','Latex');
    hleg4 = legend([S1,S2],'Cost for increasing horizon', 'Optimal cost');
    set(hleg4,'FontSize', 12);
    set(hcg4, 'FontSize', 14);
    set(ylabcg4, 'FontSize', 14);
end

%% Residual plotting 
if (store_val_fnct == 1)
    figure(5);
    res{1} = abs(Jonline{1}-cold.value_fnct(1));
    P(1) = semilogy(res{1});
    hold;
    for i=2:length(cold.ITERs)
        res{i} = abs(Jonline{i}-cold.value_fnct(i));
        P(i) = semilogy(res{i});
    end
    hcg5 = title('Function value residuals for several initial conditions');
    xlabcg5 = xlabel('Iterations k'); ylabcg5 = ylabel('|f^k-f^\ast|');
    set(hcg5,'Interpreter','Tex');  
    set(xlabcg5,'Interpreter','Tex');
    set(ylabcg5,'Interpreter','Tex');
    set(hcg5, 'FontSize', 14);
    set(xlabcg5, 'FontSize', 14);
    set(ylabcg5, 'FontSize', 14);
    muITERs = mean(cold.ITERs); %The mean
    %Overlay the mean
    plot([muITERs,muITERs],ylim,'k--','LineWidth',2)
    prctile(cold.ITERs,90);
end
        
    

%% Trajectory evolution with increasing horizon
if ( quad_system == 0 && fixed_x_init == 1 && value_function_compute == 1 )
    figure(6);
    Pomega = Omega.plot('color', 'green');
    grid off;
    hold on
    for ttt = 11:19
        x(:,ttt+1) = (A-B*K)*x(:,ttt);
    end
    for ttt = 6:19
        X{5}(:,ttt+1) = (A-B*K)*X{5}(:,ttt);
    end
    for ttt = 7:19
        X{6}(:,ttt+1) = (A-B*K)*X{6}(:,ttt);
    end
    for ttt = 9:19
        X{8}(:,ttt+1) = (A-B*K)*X{8}(:,ttt);
    end
    for ttt = 10:19
        X{10}(:,ttt+1) = (A-B*K)*X{10}(:,ttt);
    end
    P1 = plot(x(1,:), x(2,:),'-ok','LineWidth', 2.5);
    P2 = plot(X{5}(1,:), X{5}(2,:),'-*m','LineWidth', 2.5);
    P3 = plot(X{6}(1,:), X{6}(2,:),'-*r','LineWidth', 2.5);
    P4 = plot(X{8}(1,:), X{8}(2,:),'-*g','LineWidth', 2.5);
    P5 = plot(X{10}(1,:), X{10}(2,:),'-*b','LineWidth', 2.5);
    hold off
    hcg6 = title('State trajectories for several horizons');
    axis([-7 1 -0.1 0.7]) 
    xlabcg6 = xlabel('x_1'); ylabcg6 = ylabel('x_2');
    set(hcg6,'Interpreter','Tex');  
    set(xlabcg6,'Interpreter','Tex');
    set(ylabcg6,'Interpreter','Tex');
    hleg6 = legend([P1,P2,P3,P4,P5],'N=\infty', 'N=5', 'N=6', 'N=8', 'N=10');
    set(hcg6, 'FontSize', 14);
    set(xlabcg6, 'FontSize', 14);
    set(ylabcg6, 'FontSize', 14);
    set(hleg6,'Interpreter','Tex');
end


%% comparison to finite Lorenz - horizon 3/4*Nopt, terminal set - No. of iters
if quad_system == 0
    figure(7);
    load('K_fista_toy.mat');
    Comparison_Matrix_finiteVSinfinite = [[Kfinite{:,3}]' cold.ITERs' cold.HTs'];
    Comparison_Matrix_finiteVSinfinite2 = Comparison_Matrix_finiteVSinfinite;
    idx = find(Comparison_Matrix_finiteVSinfinite2 == 3e4);
    Comparison_Matrix_finiteVSinfinite2(idx,:) = 0;
    Comparison_Matrix_finiteVSinfinite = Comparison_Matrix_finiteVSinfinite2(any(Comparison_Matrix_finiteVSinfinite2,2),:);
    performance = ((Comparison_Matrix_finiteVSinfinite(:,1) - Comparison_Matrix_finiteVSinfinite(:,2)) ./ Comparison_Matrix_finiteVSinfinite(:,2))*100;
    performance_neg = min(performance,0);       performance_pos = max(performance,0);
    S1 = plot(performance_neg, 'r'); hold; S2 = plot(performance_pos, 'g'); 
    axis([0 length(performance) -100 1000]) 
    hcg7 = title('Iterations finite VS infinite horizon');
    xlabcg7 = xlabel('No. of problems'); ylabcg7 = ylabel('Percentage [%] w.r.t. CLQR solution'); 
    set(hcg7, 'FontSize', 14);
    set(xlabcg7, 'FontSize', 14);
    set(ylabcg7, 'FontSize', 14);
    hold on
    plot([1:length(performance)],ones(length(performance),1)'*mean(performance),'k--','LineWidth',2)
    hold off
end


%% Effect of warm-starting on many initial conditions
if quad_system == 1
    figure(8);
    load('warm_zero_01.mat');
    warm0.idx1 = find(all(warm.ITERs~=20000,2));
    warm0.idx2 = find(~any(isnan(warm.error),2));
    warm0.idx = unique([warm0.idx1;warm0.idx2]);
    warm0.HTs = warm.HTs(warm0.idx,:);
    warm0.ITERs = warm.ITERs(warm0.idx,:);
    warm0.error = warm.error(warm0.idx,:);

    load('warm_nonzero_01_1half.mat');
    warmnnz.HTs(1:151,:) = warm2.HTs;
    warmnnz.ITERs(1:151,:) = warm2.ITERs;
    warmnnz.error(1:151,:) = warm2.error;
    load('warm_nonzero_01_2half.mat');
    warmnnz.HTs(152:151+86,:) = warm.HTs;
    warmnnz.ITERs(152:151+86,:) = warm.ITERs;
    warmnnz.error(152:151+86,:) = warm.error;
    load('warm_nonzero_01_3half.mat');
    warmnnz.HTs(151+87:334,:) = warm.HTs;
    warmnnz.ITERs(151+87:334,:) = warm.ITERs;
    warmnnz.error(151+87:334,:) = warm.error;

    warmnnz.HTs = warmnnz.HTs(warm0.idx,:);
    warmnnz.ITERs = warmnnz.ITERs(warm0.idx,:);
    warmnnz.error = warmnnz.error(warm0.idx,:);

    warmnnz.idx = find(~any(isnan(warmnnz.error),2));
    warmnnz.HTs = warmnnz.HTs(warmnnz.idx,:);
    warmnnz.ITERs = warmnnz.ITERs(warmnnz.idx,:);
    warmnnz.error = warmnnz.error(warmnnz.idx,:);
    warm0.HTs = warm0.HTs(warmnnz.idx,:);
    warm0.ITERs = warm0.ITERs(warmnnz.idx,:);
    warm0.error = warm0.error(warmnnz.idx,:);
    hold  
    s1 = plot(mean(warm3.ITERs,2), 'r');
    s2 = plot(mean(warm4.ITERs,2), 'k');
    
    hleg8 = legend([s2,s1],'Cold start', 'Warm start');
%     set(hLine1, 'Color', 'k');
%     set(hLine2, 'Color', 'r');
    set(s1,'LineStyle','-', 'LineWidth', 2);
    set(s2,'LineStyle','-', 'LineWidth', 2); 
%     set(h01(1),'YColor','k');      set(h01(2),'YColor','r');
%     set(get(h01(1), 'Ylabel'),'String','Average increase in iterations', 'FontSize',14, 'Color', 'k' );
%     set(get(h01(2),'Ylabel'),'String','Increase in horizon length', 'FontSize',14, 'Color', 'r'); 

    hcg8 = title('Cold VS warm-starting for 2% perturbation');
    xlabcg8 = xlabel('No. of problems'); 
    ylabcg8 = ylabel('Average No. of Iterations'); 
    set(hcg8, 'FontSize', 14);
    set(xlabcg8, 'FontSize', 14);
    set(ylabcg8, 'FontSize', 14);
%     set(get(h01(1), 'Ylabel'),'Interpreter','Tex'); 
%     set(get(h01(2), 'Ylabel'),'Interpreter','Tex');
%     set(h1t, 'FontSize', 14);
end

%% Hitting times evolution for receding horizon control
if (quad_system == 1)
    load('warm_nonzero_01.mat');
    figure(1);
    hold on;
    plot(warm4.HTs(11,:)-1, 'k','LineWidth', 2);
    plot(warm4.HTs(38,:)-1, 'm','LineWidth', 2);
    plot(warm4.HTs(39,:)-1, 'r','LineWidth', 2);
    plot(warm4.HTs(59,:)-1, 'b','LineWidth', 2);
    plot([0:30],[30:-1:0],'V')
    hold off;
    axis([0 50 -1e-3 35]);
    h1y = ylabel('Hitting times T^\infty'); h1x = xlabel('Warm-started iterations');
    h1t = title('Hitting times evolution for receding horizon control');
    set(h1y,'Interpreter','Tex','FontSize', 14); 
    set(h1x,'Interpreter','Tex','FontSize', 14);
    set(h1t, 'FontSize', 14);
end
    
    
    
    

