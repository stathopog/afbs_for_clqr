function pts = HitAndRun(dat)

%% Sample points "uniformly" in the volume (hit&run)
poly.dim = size(dat.Cx,2);
disp('Start sampling...');
var = 0.025;
NPTS = 1e2;
pts = zeros(poly.dim,NPTS);
%%%%%%
% Take an inner ellipsoid and scale it by sqrt(n), gives an outer approx
%%%%%%
% c = chebyCenter(Polyhedron(Cx,cx));
pts(:,1) = .12*randn(poly.dim,1);
nchks = zeros(1,NPTS);
tic
for k=1:NPTS-1
   dir = randn(poly.dim,1);dir=dir/norm(dir);
   nwpt = pts(:,k)+var*randn*dir;  % Take a gaussian on the line (so not uniform, pbs at the bnds)
   while any(dat.Cx*nwpt > dat.cx)
        nwpt = pts(:,k) + var*randn*dir;
        nchks(k) = nchks(k)+1;
   end
   pts(:,k+1) = nwpt;
end

disp('Hit and Run time:');
disp(toc);