function [k,x,u,lambda,rPrimal,rho,val] = fista_original(N,beta,dat,xinit,lambda0,LH,temp,MAXITER)

optTol1 = 1e-4;
% lambda = zeros(no.con.all*N,1);
lambda = lambda0;
old.lambda = lambda;
hat.lambda = lambda;
Lip = 1e-4;
t = 1;  a = 20;

for k = 1:MAXITER  
%% acceleration step
    alpha = (k-1) / (k+a);
    hat.lambda = lambda + alpha*(lambda-old.lambda);
%         hat.lambda = lambda;
    u = LH' \ (LH \ (temp.C'*hat.lambda-temp.h));
    Lip = backtracking(temp, LH, u, hat.lambda, Lip, beta);
    [g,~] = funObj(temp,LH,u,hat.lambda);
    rho = 1 / Lip;
    old.lambda = lambda;
    lambda = min(0, hat.lambda - rho*g);
    u = reshape(u,dat.nu,N);
    x(:,1) = xinit;
    for i = 1:N
        x(:,i+1) = dat.A*x(:,i) + dat.B*u(:,i);
    end
    old.x = x; old.u = u;

    res = (-hat.lambda+old.lambda);

    %% terminate 
    if k > 2
        rPrimal = norm(res);  % generalized gradient check
        if  ( rPrimal < optTol1 )
            val = 0;
            for ttt = 1:N
                val = val + 0.5*x(:,ttt)'*dat.Q*x(:,ttt) + 0.5*u(:,ttt)'*dat.R*u(:,ttt);
            end
            val = val + 0.5*x(:,ttt+1)'*dat.S*x(:,ttt+1);
            break; 
        elseif (rPrimal > 1e4) || (k==MAXITER)
            val = 0;
            sprintf('Infeasible problem or too many iterations')
            break;
        end
    end
end
                
        
        
        