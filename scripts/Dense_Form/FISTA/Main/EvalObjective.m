    function [g,h] = EvalObjective(temp,LH,u,x)
    %% Computes gradient and function value of h^*(x)                    %%
    %  gradient -- g                                                      %
    %  function value -- h                                                %
        g = temp.C * u - temp.c;
        h = .5*(temp.C'*x)'*(LH' \ (LH \ ((temp.C'*x)))) -...
            (temp.C'*x)'*(LH' \ (LH \ temp.h)) - x'*temp.c +...
            .5*temp.h'*(LH' \ (LH \ temp.h)) - temp.r; 
    end