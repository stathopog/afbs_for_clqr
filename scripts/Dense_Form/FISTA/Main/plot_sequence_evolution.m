function [] = plot_sequence_evolution(Cx,Cu,cx,cu,n,m,A,B,K,x,u,Omega,hor)

figure(1); % first in 3d; (x,u)-sequence
% Write in 'projection' form Cxu * [x; u] < cxu
Cxu = [[Cx; zeros(size(Cu,1),n)] [zeros(size(Cx,1),m); Cu]];
cxu = [cx;cu];
P = Polyhedron(Cxu,cxu);
P.plot('color', 'lightblue', 'Alpha', 0.5)
hold;

% Write constraint set for LQR controller (Cx+CuK)x < cx
P2 = Polyhedron('A', Cxu, 'b', cxu, 'Ae', [-K -1], 'be', 0); %keeps it in 3D
P2.plot('color', 'magenta','Alpha', 0.8)
% P3 = Polyhedron('A', [Omega.H(:,1:end-1) zeros(size(Omega.H,1),1)], 'b', Omega.H(:,end), 'Ae', [zeros(1,n) 1], 'be', 0); %keeps it in 3D
% P3.plot('color', 'magenta')

% Formulate sequence in 3D space
seq = [x;[u(1) u]];
for iii = 1:30
    u(size(seq,2)+iii-1) = -K * x(:,size(seq,2)+iii-1);
    x(:,size(seq,2)+iii) = A * x(:,size(seq,2)+iii-1) + B * u(size(seq,2)+iii-1);
end
seq = [x;[u(1) u]];

% plot least squares sequence and lqr sequence
N = hor.length;
plot3(seq(1,:),seq(2,:),seq(3,:),'k','LineWidth',2)
plot3(seq(1,1:N),seq(2,1:N),seq(3,1:N),'+r','LineWidth',3)
plot3(seq(1,N+1:end),seq(2,N+1:end),seq(3,N+1:end),'+g','LineWidth',3)
hcg = title('State-input evolution');
xlabcg = xlabel('x_1'); ylabcg = ylabel('x_2'); zlabcg = zlabel('u');
set(hcg,'Interpreter','Tex');  
set(xlabcg,'Interpreter','Tex');
set(ylabcg,'Interpreter','Tex');
set(zlabcg,'Interpreter','Tex');
set(hcg, 'FontSize', 14);
set(xlabcg, 'FontSize', 14);
set(ylabcg, 'FontSize', 14);
set(zlabcg, 'FontSize', 14);

figure(2); % then in 2d; x-sequence
P = Polyhedron(Cx,cx);
P.plot('color', 'Lightblue','Alpha', 0.5)
hold;
P = Polyhedron([Cx; zeros(size(Cu,1),n)]+[zeros(size(Cx,1),m); Cu]*K, cxu); %keeps it in 3D
P.plot('color', 'magenta','Alpha', 0.5)
Omega.plot('color', 'k','Alpha', 0.3)
plot(seq(1,:),seq(2,:),'k','LineWidth',2)
plot(seq(1,1:N),seq(2,1:N),'+r','LineWidth',3)
plot(seq(1,N+1:end),seq(2,N+1:end),'+g','LineWidth',3)



