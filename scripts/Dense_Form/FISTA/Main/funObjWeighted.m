    function [g,h] = funObjWeighted(temp,LH,u,x)
    %% Computes gradient and function value of h^*(x)                    %%
    %  gradient -- g                                                      %
    %  function value -- h                                                %
        g = diag(temp.W)*(temp.C * u - temp.c);
        h = .5*(temp.C'*diag(temp.W)*x)'*(LH' \ (LH \ ((temp.C'*diag(temp.W)*x)))) -...
            (temp.C'*diag(temp.W)*x)'*(LH' \ (LH \ temp.h)) - x'*diag(temp.W)*temp.c +...
            .5*temp.h'*(LH' \ (LH \ temp.h)) - temp.r; 
    end