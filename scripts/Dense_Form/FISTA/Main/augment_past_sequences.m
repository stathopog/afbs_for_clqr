function aug = augment_past_sequences(hor,old,dT,dat,nU,lambda,hat)

aug.old.lambda = [old.lambda; zeros(nU-size(old.lambda,1),1)];
aug.lambda = [lambda; zeros(nU-size(lambda,1),1)];
aug.hat.lambda = [hat.lambda; zeros(nU-size(hat.lambda,1),1)];

