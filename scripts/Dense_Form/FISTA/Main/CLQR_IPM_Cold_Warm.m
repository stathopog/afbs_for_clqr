% function [] = CLQR_IPM_Cold_Warm(filestring,terminal)

%if strcmp(system, 'toy')
    %% Read data
    data;
    load('X_INIT_toy.mat');
    global X_INIT;
    global clqrStats;
    terminal = 'Terminal';
    for i = 1:length(X_INIT)  
        
        %% Solve once with CLQR
        solveCLQR(dat,bar,i);
        simLength = size(clqrStats.Xopt{i},2);
        T = clqrStats.HORZS(i);
        
        %% IPM - Parsing
        [con,yal_obj,yal_xinit,yal_x,yal_u] = parseIPM(dat,T,terminal);
        
        %% IPM - Solving
        % Solving  - Case I: CPLEX
        ops1 = sdpsettings('verbose',0,'solver','+cplex');
        opt_pair1 = optimizer(con,yal_obj,ops1,yal_xinit,[yal_x; yal_u]);
        [solver] = solveIPM(dat,i,simLength,opt_pair1,'cplex',clqrStats);
        cplexStats.tElapsed(i) = solver.tElapsed;
        cplexStats.averageTime(i) = solver.averageTime;
        cplexStats.errorX(i) = solver.errorX;
        cplexStats.errorU(i) = solver.errorU;
        
        % Solving  - Case II: SEDUMI
        ops2 = sdpsettings('verbose',0,'solver','+sedumi');
        opt_pair2 = optimizer(con,yal_obj,ops2,yal_xinit,[yal_x; yal_u]);
        [solver] = solveIPM(dat,i,simLength,opt_pair2,'sedumi',clqrStats);
        sedumiStats.tElapsed(i) = solver.tElapsed;
        sedumiStats.averageTime(i) = solver.averageTime;
        sedumiStats.errorX(i) = solver.errorX;
        sedumiStats.errorU(i) = solver.errorU;
        
        % Solving  - Case III: MOSEK
        ops3 = sdpsettings('verbose',0,'solver','+mosek');
        opt_pair3 = optimizer(con,yal_obj,ops3,yal_xinit,[yal_x; yal_u]);
        [solver] = solveIPM(dat,i,simLength,opt_pair2,'mosek',clqrStats);
        mosekStats.tElapsed(i) = solver.tElapsed;
        mosekStats.averageTime(i) = solver.averageTime;
        mosekStats.errorX(i) = solver.errorX;
        mosekStats.errorU(i) = solver.errorU;
        
        %% SA - Parsing
        if strcmp(terminal, 'No Terminal')
            [QP_reform,alg_data] = parseSA(dat,T);
            % Solving - QPgen
           [solver] = solveSA(dat,i,simLength,QP_reform.QP.MPC,'qpgen',clqrStats,T);
            qpgenStats.tElapsed(i) = solver.tElapsed;
            qpgenStats.averageTime(i) = solver.averageTime;
            qpgenStats.errorX(i) = solver.errorX;
            qpgenStats.errorU(i) = solver.errorU;
            qpgenStats.averageIter(i) = solver.averageIter;
        end
    end
      
plotNumerics(cplexStats, sedumiStats, mosekStats, qpgenStats);
    
    
% end






