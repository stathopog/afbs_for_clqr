function [k,x,u,lambda,hor,time,rPrimal,rho,J,U,X] = fista_chambolle_weighted(T,dat,xinit,time,bar,con,MAXITER,store_val_fnct,store_xu,warm,lambda0,horlen)

optTol1 = 1e-2;
J = []; U = []; X = [];
Lip = 1e-2;
a = 5;
hor.sim = T;

    for k = 1:MAXITER
        
    %% step 1
    if (k==1)  %@ first iteration solve unconstrained LQR
        switch warm
            case 'cold'
                % forward simulation 
                hor.length = 0;
                x = []; u = []; 
                z(:,1) = xinit;
                for i = 1:hor.sim
                    v(:,i) = -dat.K*z(:,i);
                    z(:,i+1) = dat.A*z(:,i) + dat.B*v(:,i);
                end
                % recover hitting time from solution        
                values.check = max(dat.Hf*z - repmat(dat.hf,1,hor.sim+1),[],1);
                hor.check = max(find(values.check>=0));
                if isempty(hor.check), hor.check = 0; end
                values.length1 = max(dat.Cx*z - repmat(dat.cx,1,hor.sim+1),[],1);   
                values.length2 = max(dat.Cu*v - repmat(dat.cu,1,hor.sim),[],1);
                hor.length1 = max(find(values.length1>=0));   
                hor.length2 = max(find(values.length2>=0));
                if isempty(hor.length1), hor.length1 = 0; end
                if isempty(hor.length2), hor.length2 = 0; end
                hor.length = max(hor.length1, hor.length2);
                if  ( hor.length == 0 ),
                    rPrimal = 0; 
                    rho = 0;
                    break; 
                end
                lambda = zeros(dat.no.con.all*hor.length,1);
            case 'warm'
                hor.length = horlen;
                x = []; u = []; 
                lambda = lambda0(1:dat.no.con.all*hor.length,1);
                % form matrices
                [temp,LH] = cholesky(hor.length,dat,bar,con);
                % solve intitial QP
                temp.W = dat.W(dat.no.con.all+1:dat.no.con.all*(hor.length+1));
                v = LH' \ (LH \ (temp.C'*diag(temp.W)*lambda-temp.h));
                v = reshape(v,dat.nu,hor.length);
                % forward simulation with initial guess for the input
                z(:,1) = xinit;
                for i = 1:hor.length
                    z(:,i+1) = dat.A*z(:,i) + dat.B*v(:,i);
                end
                for i = hor.length+1:hor.sim
                    v(:,i) = -dat.K*z(:,i);
                    z(:,i+1) = dat.A*z(:,i) + dat.B*v(:,i);
                end
                % recover hitting time from solution        
                values.check = max(dat.Hf*z - repmat(dat.hf,1,hor.sim+1),[],1);
                hor.check = max(find(values.check>=0));
                if isempty(hor.check), hor.check = 0; end
                values.length1 = max(dat.Cx*z - repmat(dat.cx,1,hor.sim+1),[],1);   
                values.length2 = max(dat.Cu*v - repmat(dat.cu,1,hor.sim),[],1);
                hor.length1 = max(find(values.length1>=0));   
                hor.length2 = max(find(values.length2>=0));
                if isempty(hor.length1), hor.length1 = 0; end
                if isempty(hor.length2), hor.length2 = 0; end
                hor.length = max(hor.length1, hor.length2);
                if  ( hor.length == 0 ),
                    rPrimal = 0; 
                    rho = 0;
                    break; 
                end
                lambda = lambda0(1:dat.no.con.all*hor.length,1);
        end
        
        % initializations
        old.lambda = lambda;
        hat.lambda = lambda;
        x = z(:,1:hor.length+1);  u = v(:,1:hor.length);
        u = reshape(u,dat.nu*hor.length,1);
        old.hor.length = hor.length;
        old.u = u;
        % cholesky for y computation
        [temp,LH] = cholesky(hor.length,dat,bar,con);

    else  %@ subsequent iterations 
        if (hor.length) > 3*hor.sim/4, break; end
        %% acceleration step
        alpha = (k-1) / (k+a);
        hat.lambda = lambda + alpha*(lambda-old.lambda);
%         hat.lambda = lambda;
        old.u = []; old.x = [];
        old.x = x; old.u = u;
        old.hor.length = hor.length;
        hat.lambda = reshape(hat.lambda,nU,1);

        u = LH' \ (LH \ (temp.C'*diag(temp.W)*hat.lambda-temp.h));

        % Forward propagation of the dynamics for [hor.length+1,Ts]
        % and computation of running maximum of T(i)
        [hor.length,hor.check,u,x] = sim_sys_dyns(dat,u,old.hor.length);

        %% keep latest horizon
        hor.length = max(old.hor.length,hor.length); %?
        hor.length_track(k) = hor.length;

        if (mod(k,15) == 0)
            time.x{time.count} = x;
            time.count = time.count + 1;
        end   
    end
                
                
        %% step 2
        dT = hor.length-old.hor.length;
        nU = dat.no.con.all*hor.length;
        
        % augment_past_sequences
        if dT > 0
            aug = augment_past_sequences(hor,old,dT,dat,nU,lambda,hat);
            [temp,LH] = cholesky(hor.length,dat,bar,con);
        else
            aug.lambda = lambda;
            aug.old.lambda = old.lambda;
            aug.hat.lambda = hat.lambda;
        end
        
        % backtracking for Lipschitz evaluation
        temp.W = dat.W(1:dat.no.con.all*hor.length);
        Lip = backtracking_weighted(temp, dat, LH, u, aug.hat.lambda, Lip);
        rho = 1 / Lip; % condition for convergence
%         rho = 1/dat.beta;
        [g,~] = funObjWeighted(temp,LH,u,aug.hat.lambda);
        old.lambda =  aug.lambda;
        hat.lambda =  aug.hat.lambda;
        lambda = min(0, aug.hat.lambda - rho*g);
        
        %% step 3 -  Residual computation
        res = (lambda-old.lambda);
        % terminate 
%         if k > 2
            rPrimal = norm(res);  % generalized gradient check
            active.t = find(lambda);
            active.C = full(temp.C(active.t,:));
            if  ( rPrimal < optTol1 ) || ( k == MAXITER )
                KKT = [temp.H active.C';
                       active.C zeros(length(active.t))];
                solKKT = KKT \ [-temp.h; temp.c(active.t)];
                u = solKKT(1:dat.nu*hor.length);
                val = 0;
                u = reshape(u,dat.nu,hor.length);
                for ttt = 1:hor.length
                    val = val + 0.5*x(:,ttt)'*dat.Q*x(:,ttt) + 0.5*u(:,ttt)'*dat.R*u(:,ttt);
                end
                val = val + 0.5*x(:,ttt+1)'*dat.S*x(:,ttt+1);
                J(k) = val;
                break; 
            end
%         end
        
        %% Store function value
%         if (store_val_fnct==1)
%             val = 0;
%             u = reshape(u,m,hor.length);
%             for ttt = 1:hor.length
%                 val = val + 0.5*x(:,ttt)'*Q*x(:,ttt) + 0.5*u(:,ttt)'*R*u(:,ttt);
%             end
%             val = val + 0.5*x(:,ttt+1)'*S*x(:,ttt+1);
%             J(k) = val;
%         end
        
        %% Store x-u trajectories
        if (store_xu==1)
            val = 0;
            U{k} = u; 
            X{k} = x;
        end
    end
end
        
        
        