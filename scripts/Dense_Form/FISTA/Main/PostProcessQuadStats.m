function [] = PostProcessQuadStats()
load('warm_zero_005.mat');
AvZer005 = mean(warm3.ITERs(:,1:15),2);
load('warm_nonzero_005.mat');
AvNonZer005 = mean(warm4.ITERs(:,1:15),2);
x005 = cat(2,AvZer005,AvNonZer005);
aboxplot(x005);
figure

load('warm_zero_01.mat');
AvZer01 = mean(warm4.ITERs(:,1:15),2);
load('warm_nonzero_01.mat');
AvNonZer01 = mean(warm3.ITERs(:,1:15),2);
x01 = cat(2,AvZer01,AvNonZer01);
aboxplot(x01);
figure

h = {x005; x01};

aboxplot(h,'labels',{'cold', 'warm'},'WidthE',0.5);
hcg2 = title('Box Plots: Cold VS Warm starting');
ylabcg2 = ylabel('Number of iterations');
set(hcg2,'Interpreter','Tex');  
set(ylabcg2,'Interpreter','Tex');
hleg2 = legend('0.5 %', '1 %');
set(hcg2, 'FontSize', 16);
set(ylabcg2, 'FontSize', 16);
set(hleg2,'Interpreter','Tex','FontSize', 12);
ylim([0 200]) 