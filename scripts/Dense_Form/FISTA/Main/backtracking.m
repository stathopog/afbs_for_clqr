    function Lip = backtracking(temp, LH, u, x, Lk, beta)
    %% Computes Lipschitz constant of F(x) = { h^*(x), x<=0 } using      %%
    %% backtracking as suggested in FISTA                                %%
    
    eta = 1.2; 
    [g,h] = funObj(temp,LH,u,x); % @ hat.lambda
    p_L = min( 0, x - (1/Lk)*g ); 
    [~,F] = funObj(temp,LH,u,p_L); % @ p_L(hat.lambda)
    Q_L = h + (p_L-x)' * g + (Lk/2) * (p_L-x)'*(p_L-x);
%     idx = 1;
    if ( F <= Q_L+eps )
        Lip = Lk;
    end
    kkk = 0;
    while ( F >= Q_L ) && (Lk <= beta) && (kkk <= 1e2)
        Lk   = eta*Lk;
        p_L = min( 0, x - (1/Lk)*g );
        [~,F] = funObj(temp,LH,u,p_L);
        Q_L = h + (p_L-x)' * g + (Lk/2) * (p_L-x)'*(p_L-x);
        if ( Lk >= beta )
            Lip = beta;
        elseif (F <= Q_L)
            Lip = Lk;
        end
        kkk = kkk + 1;
    end