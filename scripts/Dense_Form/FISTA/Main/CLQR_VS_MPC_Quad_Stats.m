function [CLQR,MPC,XINIT_FEAS] = CLQR_VS_MPC_Quad_Stats(dat,noise,MAXITER,store_val_fnct,store_xu,model)

CLQR.ITERS = [];   CLQR.X = [];   CLQR.U = [];  CLQR.HORZS = [];
MPC.ITERS = [];   MPC.X = [];   MPC.U = [];
temp = []; 
XINIT = HitAndRun(dat);   % sampling the constraint set
XINIT_FEAS = [];

for iii = 1:size(XINIT,2)  
    if (mod(iii,5)==0)
        save('DataQuadCLQR_VS_MPC.mat','CLQR','MPC','XINIT_FEAS');
    end
    dat.x_init = XINIT(:,iii);
    sprintf('State %d', iii)
    ctrl = MPCController(model, 5);
     
    %---------------solve w/ FISTA-Chambolle---------------------%
%     if ( feasible == 0 ), break; end
    Nsim = 100;
    [bar,con,~,F1,G1,F2,G2] = dense_form_generation(Nsim,dat,'No Terminal');
    bar.cxx = bar.cx-bar.Cx*bar.A*dat.x_init-bar.Cx*bar.noise*reshape(noise,dat.nx*Nsim,1);
    con.c = [];
    for i1 = 1:Nsim
        con.c = [con.c; bar.cu((i1-1)*dat.no.con.u+1:i1*dat.no.con.u,:); bar.cxx((i1-1)*dat.no.con.x+1:i1*dat.no.con.x,:)];
    end
    r = dat.x_init'*F1*dat.x_init + reshape(noise,dat.nx*Nsim,1)'*F2*reshape(noise,dat.nx*Nsim,1);
    h = G1'*dat.x_init + G2'*reshape(noise,dat.nx*Nsim,1);
    time.x = {};
    time.count = 1; 
    % Weighted
    [inf.k,inf.x,inf.u,~,inf.hor,inf.time,~,~,J,~,~] = fista_chambolle_weighted(Nsim,dat,dat.x_init,time,bar,con,MAXITER,store_val_fnct,store_xu);
    inf.u = reshape(inf.u,dat.nu,inf.hor.length);
    if ~(any(inf.u <= -0.71)) & ~(any(inf.u >= 0.31)) 
        CLQR.COST(iii) = J(end);
        CLQR.HORZS(iii) = inf.hor.length;
        CLQR.ITERS(iii) = inf.k;
        CLQR.X{iii} = inf.x;
        CLQR.U{iii} = inf.u;
        %-------------compute optimal horizon length-------------------------%
        checkHorzs = [ceil(CLQR.HORZS(iii)/4);ceil(CLQR.HORZS(iii)/2);...
            3*ceil(CLQR.HORZS(iii)/4);CLQR.HORZS(iii)];
        temp_OptCost(1) = 0;
        for jjj = 1:4
            M = checkHorzs(jjj)
            ctrl.N = M;
            loop = ClosedLoop(ctrl, model);
            data{jjj} = loop.simulate(dat.x_init, 50*CLQR.HORZS(iii));
            old.temp_OptCost = temp_OptCost;
            if ~isempty(data{jjj}.cost)
                XINIT_FEAS(:,iii,jjj) = dat.x_init;
                val = 0;
                for ttt = 1:size(data{jjj}.U,2)
                    val = val + 0.5*data{jjj}.X(:,ttt)'*dat.Q*data{jjj}.X(:,ttt) +...
                        0.5*data{jjj}.U(:,ttt)'*dat.R*data{jjj}.U(:,ttt);
                end
                MPC.COST(iii,jjj) = val; 
                %---------------solve w/ original FISTA ---------------------%
                [bar,con,H,F1,G1,~,~] = dense_form_generation(M,dat,'No Terminal');
                LH        = chol(H,'lower');
                temp.C    = con.C(1:dat.no.con.all*M,1:dat.nu*M);
                beta2 = (1 / min(eig(H))) * (norm(full(temp.C)))^2;
                x_init2 = dat.x_init;
                MPC.ITERS{iii,jjj} = 0;
                lambda0 = zeros(dat.no.con.all*M,1);
                for i3 = 1:max(2*CLQR.HORZS(iii),80)
                    i3
                    bar.cxx = bar.cx-bar.Cx*bar.A*x_init2; % omit noise
                    con.c = [];
                    for i2 = 1:M
                        con.c = [con.c; bar.cu((i2-1)*dat.no.con.u+1:i2*dat.no.con.u,:); bar.cxx((i2-1)*dat.no.con.x+1:i2*dat.no.con.x,:)];
                    end
                    temp.r    = .5*x_init2'*F1*x_init2;
                    temp.h    = G1'*x_init2;
                    temp.c    = con.c(1:dat.no.con.all*M,1);
                    [fin.k,fin.x,fin.u,fin.lambda,~,~,~] = fista_original(M,beta2,dat,x_init2,lambda0,LH,temp,MAXITER);
                    MPC.ITERS{iii,jjj}(i3) = fin.k;
                    MPC.X{iii,i3} = fin.x(:,1);
                    MPC.U{iii,i3} = fin.u(:,1);
                    x_init2 = dat.A*fin.x(:,1) + dat.B*fin.u(:,1);
                    lambda0 = fin.lambda;
                end  
                clear MPC.X; clear MPC.U;
            else
                MPC.COST(iii,jjj) = 0;
            end
        end
    end
end
end