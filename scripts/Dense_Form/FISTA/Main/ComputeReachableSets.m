function [InvSet, Horzs] = ComputeReachableSets(dat,model)

%% i-step reachable sets
    figure();  
%     Horzs  = [6; 7; 14; 15; 21; 22; 28; Inf];
        Horzs  = [7; 15; 22; Inf];
%     colors = jet(length(Horzs)+1);%
    colors = {'darkred', 'orange', 'yellow', 'darkgreen', 'darkblue'}; 
    hold;
    legend_names = cell(1,length(Horzs));
    for iii = length(Horzs):-1:1
        [InvSet{iii}] = feasible_set(Horzs(iii),dat,model);
        fprintf('%i-step reachable set\n', Horzs(iii));
        h(iii) = InvSet{iii}.plot('color',colors{iii});
        legendtext = [num2str(Horzs(iii)),'-step reachable set'];
        legend_names{iii} = legendtext;
        pause;
    end
    Omega = Polyhedron(dat.Hf,dat.hf);
    h(length(Horzs)+1) = Omega.plot('color', colors{5});
    legend_names{length(Horzs)+1} = 'Positively invariant set';
    hleg = legend([fliplr(legend_names(1:end-1)) legend_names(end)]);
    set(hleg,'Interpreter','Tex');
    htit = title('Reach sets for given terminal set');
    h1x = xlabel('x_1');    h1y = ylabel('x_2');
    set(htit,'Interpreter','Tex');
    set(h1x,'Interpreter','Tex');
    set(h1y,'Interpreter','Tex');
    set(hleg, 'FontSize', 14);
    set(htit, 'FontSize', 16);
    set(h1x, 'FontSize', 16);
    set(h1y, 'FontSize', 16);
    grid off;

% %     %% generate traj
% %     hold;
% %     for N=15
% %     [X,U,value_fnct2] = generate_sets(InvSet,Omega,x_init,n,m,A,B,Cx,Cu,cx,cu,Q,R,S,K,N,6);
% %     end
end