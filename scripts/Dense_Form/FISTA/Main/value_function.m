function [J, X, U, result] = value_function(n,m,N,x_init,Cx,cx,Cu,cu,Hf,hf,Q,R,S,A,B) 
% solve with cplex (fixed horizon)
yal_u = sdpvar(m,N);
yal_x = sdpvar(n,N+1);
con = []; 
for i = 1:N
    con = con + [yal_x(:,i+1) == A*yal_x(:,i) + B*yal_u(:,i)];
end
con = con + [yal_x(:,1) == x_init];
for i = 1:N
    con = con + [Cx*yal_x(:,i) <= cx];
    con = con + [Cu*yal_u(:,i) <= cu];
end
con = con + [Hf*yal_x(:,N+1) <= hf];
yal_obj = 0;
for i = 1:N
    yal_obj = yal_obj + 0.5*yal_x(:,i)'*Q*yal_x(:,i) + 0.5*yal_u(:,i)'*R*yal_u(:,i);
end
yal_obj = yal_obj + 0.5*yal_x(:,N+1)'*S*yal_x(:,N+1);

ops = sdpsettings('solver','cplex','verbose',0);
result = solvesdp(con, yal_obj, ops);
J = double(yal_obj);    X = double(yal_x);  U = double(yal_u);