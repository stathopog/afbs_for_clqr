%% Infinite horizon MPC using FISTA - main

clear all;
close all;
randn('state',4);
rand('state',0);

toy_system = 1; % flag for toy or quad
quad_system = 0; % flag for toy or quad
RoA_VS_ITERS = 1; % flag for RoA and iters comparison
warm_starting = 0; % flag for warm-starting
value_function_compute = 0; % flag for computing cost
fixed_x_init = 0; % flag for plotting evolution of a predefined initial state
compare_to_fista = 0; % flag for comparing with fixed horizon using FISTA
store_val_fnct   = 1; % flag for storing value function per iteration of the algorithm
store_xu   = 0; % flag for storing x-u trajectories per iteration of the algorithm

%% Defining the problem parameters
if ( toy_system == 1 )
    % toy system
%     N = 30;
    [dat,model] = small_random;
elseif ( quad_system == 1 )
    % quad model
%     N = 50;
    [dat,model] = quad_mpc; 
elseif ( chain_system == 1 )
    % quad model
%     N = 50;
    [dat,model] = integrator_chain; 
% load datChainT02;
% load modelChainT02;
end

N = 100; 
noise = 0*randn(dat.nx,N);
[dat] = upper_bound_beta(N,model,dat); 

%% Feasible set
if ( toy_system == 1 )
    % toy system
    load('X_INIT_toy.mat');
    HTs = zeros(size(X_INIT,2),1);
    ITERs = zeros(size(X_INIT,2),1); 
elseif ( quad_system == 1 )
    % quad model
    load('X_INIT_quad.mat');
    HTs = zeros(size(XINIT_QUAD,2),1);
    ITERs = zeros(size(XINIT_QUAD,2),1); 
    X_INIT = XINIT_QUAD;
end


 %% Dense formulation
con = [];
[bar,con,H,F1,G1,G2] = dense_form_generation(N,dat,'No Terminal');
MAXITER = 1e4;
J = [];

% % %% Simulations with CLQR
% % for jjj = 1:size(X_INIT,2)
% %     %% IPM - Parsing
% %     [constraints,yal_obj,yal_xinit,yal_x,yal_u] = parseIPM(dat,N,'Terminal','toy');
% %     ops1 = sdpsettings('verbose',0,'solver','+mosek');
% %     opt_pair1 = optimizer(constraints,yal_obj,ops1,yal_xinit,[yal_x; yal_u]);
% %     kkk = 1;
% %     dat.x_init = X_INIT(:,jjj);
% % % %     lambda0 = zeros(dat.no.con.all*N,1);
% %     lambda0 = zeros(dat.no.con.all,N);
% %     init = 'cold';
% %     horlen = 0;
% %     while (any(dat.Hf*dat.x_init-dat.hf>0))
% %         disp(kkk)
% %         bar.cxx = bar.cx-bar.Cx*bar.A*dat.x_init-bar.Cx*bar.noise*reshape(noise,dat.nx*N,1);
% %         con.c = [];
% %         for i1 = 1:N
% %             con.c = [con.c; bar.cu((i1-1)*dat.no.con.u+1:i1*dat.no.con.u,:); bar.cxx((i1-1)*dat.no.con.x+1:i1*dat.no.con.x,:)];
% %         end
% %         r = dat.x_init'*F1*dat.x_init + reshape(noise,dat.nx*N,1)'*F2*reshape(noise,dat.nx*N,1);
% %         h = G1'*dat.x_init + G2'*reshape(noise,dat.nx*N,1);
% %         time.x = {};
% %         time.count = 1; 
% % % %         [inf.k,inf.x,inf.u,inf.lambda,inf.hor,~,inf.rPrimal,~,~,~,~] =...
% % % %             fista_chambolle_weighted(N,dat,dat.x_init,time,bar,con,MAXITER,store_val_fnct,store_xu,init,lambda0,horlen);
% %         [inf.k,inf.x,inf.u,inf.lambda,inf.hor,~,inf.rPrimal,~,~,~,~] =...
% %         FastDualGradient_Riccati(N,dat,dat.x_init,time,MAXITER,store_val_fnct,store_xu,init,lambda0,horlen-1);
% %         if (inf.hor.length<=1), break; end
% %         
% % % %         idx = min(reshape(inf.lambda,dat.no.con.all,inf.hor.length),[],1);
% %         idx = min(inf.lambda,[],1);
% %         if idx(end)<0
% %             horlen = inf.hor.length;
% %         else
% %             horlen = max(find(idx~=0));
% %         end
% %         CLQR.OPT_HORZ{jjj}(kkk) = horlen; % optimal horizon length (dual variable)
% %         CLQR.HORZS{jjj}(kkk) = inf.hor.length; % horizon length as output of CLQR
% %         CLQR.ITERS{jjj}(kkk) = inf.k;
% %         CLQR.X{jjj}(:,kkk) = inf.x(:,1);
% %         CLQR.U{jjj}(:,kkk) = inf.u(:,1);
% %         CLQR.res{jjj}(kkk) = inf.rPrimal;
% % % %         lambda0 = [inf.lambda(dat.no.con.all+1:end,1); zeros((N+1)*dat.no.con.all-length(inf.lambda),1)];
% %         lambda0 = dat.w*[inf.lambda(:,2:end) zeros(dat.no.con.all,N+1-size(inf.lambda,2))];
% % 
% %         % Solving  - Case I: CPLEX
% %         tStart = tic;
% %         [yal_z,errorcode] = opt_pair1{dat.x_init};
% %         if errorcode
% %            yalmiperror(errorcode)
% %            break
% %         end
% %         % Store trajectories
% %         IPM.X{jjj}(:,kkk) = dat.x_init;
% %         IPM.U{jjj}(:,kkk) = double(yal_z(dat.nx+1:end,1));
% %         IPM.error{jjj}(kkk) = errorcode;
% %         kkk = kkk + 1;
% %         init = 'warm';
% %         dat.x_init = inf.x(:,2) + 0.1*(1-2*rand)*inf.x(:,2);
% %     end
% % end

%% Comparison with finite horizon
if ( toy_system == 1 && RoA_VS_ITERS == 1 )
    if ( store_val_fnct == 0 )
% %         [CLQR,MPC] = CLQR_VS_MPC_Toy_Stats(n,m,no,K,A,B,Cx,Cu,cx,cu,...
% %             Hf,hf,Q,R,S,noise,MAXITER,beta,store_val_fnct,store_xu); 
    else
        [CLQR,MPC] = CLQR_VS_MPC_Toy_Stats(dat,noise,MAXITER,store_val_fnct,store_xu,model);
    end
elseif ( quad_system == 1 && RoA_VS_ITERS == 1 )
    model.x.without('terminalSet');
    [CLQR,MPC,XINIT_FEAS] = CLQR_VS_MPC_Quad_Stats(dat,noise,MAXITER,store_val_fnct,store_xu,model);
end

pause;

    
    temp = [];
    for iii = 1:size(MPC.ITERS,1)
            for jjj = 1:size(MPC.ITERS,2)
                if isempty(MPC.ITERS{iii,jjj})% || any(MPC.ITERS{iii,jjj}==2e4)
                    temp(iii,jjj) = 0;
                else
                    temp(iii,jjj) = mean(MPC.ITERS{iii,jjj}(1:80));
                end
            end
    end
    clear MPC.ITERS;   MPC.ITERS = temp;
    ItersCompare = [MPC.ITERS(:,1:3) CLQR.ITERS./CLQR.HORZS];
    ClqrVSMpc.iters = ItersCompare(all(ItersCompare,2),:);
    ClqrVSMpc.cost = COST_QUAD(all(COST_QUAD,2),:);

pause;

% % load('cold_quad.mat')
% % if (compare_to_fista == 1)
% %     for j = 1:size(X_INIT,2)
% %         count = 1;
% %         M_vec = [ceil(cold.HTs(j)/4); ceil(cold.HTs(j)/2); ceil(3*cold.HTs(j)/4); cold.HTs(j)];
% %         while ( count <= 4 )
% %             M = M_vec(count);
% %             x_init = X_INIT(:,j);
% %             [bar,con,H,F1,G1,F2,G2] = dense_form_generation(M,n,m,no,A,B,Cx,Cu,cx,cu,Hf,hf,Q,R,S);
% %             bar.cxx = bar.cx-bar.Cx*bar.A*x_init; % omit noise
% %             con.c = [];
% %             for i = 1:M
% %                 con.c = [con.c; bar.cu((i-1)*no.con.u+1:i*no.con.u,:); bar.cxx((i-1)*no.con.x+1:i*no.con.x,:)];
% %             end
% %             temp.r    = .5*x_init'*F1*x_init;
% %             temp.h    = G1'*x_init;
% %             temp.C    = con.C(1:no.con.all*M,1:m*M);
% %             temp.c    = con.c(1:no.con.all*M,1);
% %             LH        = chol(H,'lower');
% %             beta = (1 / min(eig(H))) * (norm(bar.Cxx) + norm(Cu))^2;
% %             [K,X,U,~,rPrimal,rho,J] = fista_original(M,x_init,LH,temp,A,B,Q,R,S,m,no,MAXITER,beta);
% %             Kfinite_fista{j,count} = K;
% %             Xfinite_fista{j,count} = X;
% %             Ufinite_fista{j,count} = U;
% %             Jfinite_fista{j,count} = J;
% %             count = count + 1;
% %             M
% %         end
% %     end
% % end
% % 
% % if (quad_system==1)
% %     for i = 1:length(finite.J)
% %         if all(abs(finite.J{i}(1,:)-cold.value_fnct(i))>0.1)
% %             Nreal(i) = 0;
% %         elseif (~isempty(find(finite.J{i}(1,:)>=(1+1e-6)*finite.J{i}(1,end))))
% %             Nreal(i) = max(find(finite.J{i}(1,:)>=(1+1e-6)*finite.J{i}(1,end))) + 1;
% %         else
% %             Nreal(i) = min(find(finite.J{i}(1,:)==finite.J{i}(1,end)));        
% %         end
% %     end
% % elseif (toy_system==1)
% %         for i = 1:length(Jfinite_cplex)
% %         if all(abs(Jfinite_cplex{i}(1,:)-cold.value_fnct(i))>0.1)
% %             Nreal(i) = 0;
% %         elseif (~isempty(find(Jfinite_cplex{i}(1,:)>=(1+1e-6)*Jfinite_cplex{i}(1,end))))
% %             Nreal(i) = max(find(Jfinite_cplex{i}(1,:)>=(1+1e-6)*Jfinite_cplex{i}(1,end))) + 1;
% %         else
% %             Nreal(i) = min(find(Jfinite_cplex{i}(1,:)==Jfinite_cplex{i}(1,end)));        
% %         end
% %         end
% % end
% %     
% % mat = [Nreal' cold.HTs(1:100)'-1];
% % idx = find(mat(:,1)~=0);
% % mat = mat(idx,:);
% % 

% % 
% % 
% % 
% % 
% % plotting(n, m, X, x, x_init, A, B, K, Omega, quad_system, fixed_x_init, value_function_compute, inexact_clqr, INEXACT, warm, Cx,cx,Cu,cu)
% % 
% %     
% % 




