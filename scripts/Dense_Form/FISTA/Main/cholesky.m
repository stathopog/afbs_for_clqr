function [temp,LH] = cholesky(horizon,dat,bar,con)

temp = [];
temp.barQ = blkdiag(kron(eye(horizon-1),dat.Q), dat.S);
temp.barR = bar.R(1:horizon*dat.nu, 1:horizon*dat.nu);
temp.barB = bar.B(1:dat.nx*horizon,1:dat.nu*horizon);
temp.barA = bar.A(1:dat.nx*horizon,:);
temp.H    = temp.barB'*temp.barQ*temp.barB + temp.barR;
temp.G1   = temp.barA'*temp.barQ*temp.barB;
temp.F1   = temp.barA'*temp.barQ*temp.barA + dat.Q;
temp.r    = .5*dat.x_init'*temp.F1*dat.x_init;
temp.h    = temp.G1'*dat.x_init;
temp.C    = con.C(1:dat.no.con.all*horizon,1:dat.nu*horizon);
temp.c    = con.c(1:dat.no.con.all*horizon,1);
LH        = chol(temp.H,'lower');