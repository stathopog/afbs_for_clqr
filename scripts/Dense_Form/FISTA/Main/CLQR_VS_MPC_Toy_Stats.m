function [CLQR,MPC] = CLQR_VS_MPC_Toy_Stats(dat,noise,MAXITER,store_val_fnct,store_xu,model)
%-------------------------------------------------------------------------%
% Gather statistics for CLQR VS MPC for the toy example. MPC w/ and w/o
% terminal set are tested against the CLQR approach. 
%-------------------------------------------------------------------------%

% Initialize
CLQR.ITERS = [];   CLQR.X = [];   CLQR.U = [];  CLQR.HORZS = [];    CLQR.J = [];
MPC.ITERS = [];   MPC.X = [];   MPC.U = [];  MPC.J = []; MPC.HORZS = [];

% Compute reachable sets
[InvSet, Horzs] = ComputeReachableSets(dat,model);
ctrl = MPCController(model, 5);

for jjj = 2:2:8 % for each slice
        count = 1;
        if jjj==8
%             jjj = jjj-1;
            M = 29; 
            sprintf('Horizon length %d', Horzs(jjj))
        else
            M = Horzs(jjj);
            sprintf('Horizon length %d', Horzs(jjj))
        end
        for iii = 1:1e4 % generate 1e4 initial conditions
            sprintf('State %d', iii)
            dat.x_init = [-10+10*rand; -1+5*rand];
            % inclusion in slice
            if (InvSet{jjj}.H(:,1:end-1)*dat.x_init <= InvSet{jjj}.H(:,end)) 
               if any((InvSet{jjj-1}.H(:,1:end-1)*dat.x_init > InvSet{jjj-1}.H(:,end)))
                    %---------------solve w/ FISTA-Chambolle---------------------%
                    Nsim = 100;
                    [bar,con,~,F1,G1,G2] = dense_form_generation(Nsim,dat,'No Terminal');
                    bar.cxx = bar.cx-bar.Cx*bar.A*dat.x_init-bar.Cx*bar.noise*reshape(noise,dat.nx*Nsim,1);
                    con.c = [];
                    for i1 = 1:Nsim
                        con.c = [con.c; bar.cu((i1-1)*dat.no.con.u+1:i1*dat.no.con.u,:); bar.cxx((i1-1)*dat.no.con.x+1:i1*dat.no.con.x,:)];
                    end
                    r = dat.x_init'*F1*dat.x_init;
                    h = G1'*dat.x_init + G2'*reshape(noise,dat.nx*Nsim,1);
                    time.x = {};
                    time.count = 1; 
%                   % Weighted
                    [inf.k,inf.x,inf.u,~,inf.hor,inf.time,~,~,J,~,~] =...
                        fista_chambolle_weighted(Nsim,dat,dat.x_init,time,bar,con,MAXITER,store_val_fnct,store_xu,'cold',[],[]);
                    CLQR.HORZS(jjj,count) = inf.hor.length;
                    CLQR.ITERS(jjj,count) = inf.k;
                    CLQR.X{jjj,count} = inf.x;
                    CLQR.U{jjj,count} = inf.u;
                    CLQR.J(jjj,count) = J(end);                   
                   %---------identification of optimal horizon length and solution with finite horizon FISTA-----%
                   for i2 = M:CLQR.HORZS(jjj,count) % for each horizon between [Tmin, T^inf] solve the closed loop problem
                        ctrl = MPCController(model, i2); 
                        loop = ClosedLoop(ctrl, model);
                        data{i2} = loop.simulate(dat.x_init, 10*inf.hor.length);
                        [~, ~, openloop] = ctrl.evaluate( dat.x_init );
                        if ~isempty(data{i2}.U)
                            val = 0;
                            for ttt = 1:size(data{i2}.U,2)
                                val = val + 0.5*data{i2}.X(:,ttt)'*dat.Q*data{i2}.X(:,ttt) +...
                                    0.5*data{i2}.U(:,ttt)'*dat.R*data{i2}.U(:,ttt);
                            end 
                            MPC.J{jjj,count}(i2) = val;
                            MPC.X{jjj,count,i2} = data{i2}.X;
                            MPC.U{jjj,count,i2} = data{i2}.U;
                            if (all((dat.Hf*openloop.X(:,i2+1)-dat.hf) <=-1e-5)) 
                                MPC.HORZS(jjj,count) = i2;
                                break;
                            end
                        end
                   end
                   TEST_HORZS = [M, MPC.HORZS(jjj,count), 2*M];
                   for i2 = TEST_HORZS
                    if ~isempty(data{TEST_HORZS(1)}.U)
                        %---------------solve w/ original FISTA w/ T.S. ---------------------%
                        model.x.with('terminalSet');
                        model.x.terminalSet = dat.Omega;
                        ctrl = MPCController(model, i2); 
                        [bar,con,H,F1,G1,~,~] = dense_form_generation(i2,dat,'Terminal');
                        LH        = chol(H,'lower');
%                         dat.x_init2 = dat.x_init;
%                         MPC.ITERS{jjj,count}(i2) = 0;
                        lambda0 = zeros(dat.no.con.all*(i2-1)+dat.no.con.xf+dat.no.con.u,1);
                        % for each horizon between [Tmin, T^inf] solve
                        % the closed loop problem with FISTA
%                         for i3 = 1:max(2*CLQR.HORZS(jjj,count),80)
                            bar.cxx = bar.cx-bar.Cx*bar.A*dat.x_init; % omit noise
                            con.c = [];
                            for i4 = 1:i2-1
                                con.c = [con.c; bar.cu((i4-1)*dat.no.con.u+1:i4*dat.no.con.u,:); bar.cxx((i4-1)*dat.no.con.x+1:i4*dat.no.con.x,:)];
                            end
                            con.c = [con.c; bar.cu((i2-1)*dat.no.con.u+1:i2*dat.no.con.u,:); dat.hf-dat.Hf*dat.A^i2*dat.x_init];
                            temp.r    = .5*dat.x_init'*F1*dat.x_init;
                            temp.h    = G1'*dat.x_init;
                            temp.C    = con.C(1:dat.no.con.all*(i2-1)+dat.no.con.xf+dat.no.con.u,1:dat.nu*i2);
                            temp.c    = con.c(1:dat.no.con.all*(i2-1)+dat.no.con.xf+dat.no.con.u,1);
                            beta2 = (1 / min(eig(H))) * (norm(full(temp.C)))^2;
                            [fin.k,fin.x,fin.u,fin.lambda,~,~,~] = fista_original(i2,beta2,dat,dat.x_init,lambda0,LH,temp,MAXITER);                        
                            MPC.ITERS_WTC{jjj,count}(i2) = fin.k;
%                             dat.x_init = A*fin.x(:,1) + B*fin.u(:,1);
%                                 lambda0 = fin.lambda;
                    else
                        MPC.ITERS_WTC{jjj,count}(i2) = 0;
                    end
                   end
                        
                    for i2 = TEST_HORZS % for each horizon between [Tmin, T^inf] solve the closed loop problem
                        model.x.without('terminalSet');
                        ctrl = MPCController(model, i2); 
                        [~, ~, openloop] = ctrl.evaluate( dat.x_init );
                        loop = ClosedLoop(ctrl, model);
                        data{i2} = loop.simulate(dat.x_init, 10*inf.hor.length);
                        if ~isempty(data{i2}.U) 
                            %---------------solve w/ original FISTA w/o T.S. ---------------------%
                            [bar,con,H,F1,G1,~,~] = dense_form_generation(i2,dat,'No Terminal');
                            LH        = chol(H,'lower');
                            lambda0 = zeros(dat.no.con.all*(i2-1)+dat.no.con.x+dat.no.con.u,1);
                            % for each horizon between [Tmin, T^inf] solve
                            % the closed loop problem with FISTA
    %                         for i3 = 1:max(2*CLQR.HORZS(jjj,count),80)
                                bar.cxx = bar.cx-bar.Cx*bar.A*dat.x_init; % omit noise
                                con.c = [];
                                for i4 = 1:i2-1
                                    con.c = [con.c; bar.cu((i4-1)*dat.no.con.u+1:i4*dat.no.con.u,:); bar.cxx((i4-1)*dat.no.con.x+1:i4*dat.no.con.x,:)];
                                end
                                con.c = [con.c; bar.cu((i2-1)*dat.no.con.u+1:i2*dat.no.con.u,:); dat.cx-dat.Cx*dat.A^i2*dat.x_init];
                                temp.r    = .5*dat.x_init'*F1*dat.x_init;
                                temp.h    = G1'*dat.x_init;
                                temp.C    = con.C(1:dat.no.con.all*(i2-1)+dat.no.con.x+dat.no.con.u,1:dat.nu*i2);
                                temp.c    = con.c(1:dat.no.con.all*(i2-1)+dat.no.con.x+dat.no.con.u,1);
                                beta2 = (1 / min(eig(H))) * (norm(full(temp.C)))^2;
                                [fin.k,fin.x,fin.u,fin.lambda,~,~,~] = fista_original(i2,beta2,dat,dat.x_init,lambda0,LH,temp,MAXITER);
 
                                MPC.ITERS_WOTC{jjj,count}(i2) = fin.k;
    %                                 lambda0 = fin.lambda;
                        else
                            MPC.ITERS_WOTC{jjj,count}(i2) = 0;
                        end 
                    end
                   count = count + 1;
               end
            end
        end
end
end