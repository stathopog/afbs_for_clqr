%% understand sets
function [X,U,value_fnct] = generate_sets(InvSet,Omega,x_init,n,m,A,B,Cx,Cu,cx,cu,Q,R,S,K,N,I)

% Hf = Omega.H(:,1:end-1);
% hf = Omega.H(:,end);
Hf = InvSet{I}.H(:,1:end-1);
hf = InvSet{I}.H(:,end);

%% cplex parsing for fixed horizon
% solve with cplex (fixed horizon)
yal_x0 = sdpvar(n,1);
yal_u = sdpvar(m,N);
yal_x = sdpvar(n,N+1);
con = []; 
for i = 1:N
    con = con + set(yal_x(:,i+1) == A*yal_x(:,i) + B*yal_u(:,i));
end
con = con + set(yal_x(:,1) == yal_x0);
for i = 1:N
    con = con + set(Cx*yal_x(:,i) <= cx);
    con = con + set(Cu*yal_u(:,i) <= cu);
end
con = con + set(Hf*yal_x(:,N+1) <= hf);
yal_obj = 0;
for i = 1:N
    yal_obj = yal_obj + 0.5*yal_x(:,i)'*Q*yal_x(:,i) + 0.5*yal_u(:,i)'*R*yal_u(:,i);
end
yal_obj = yal_obj + 0.5*yal_x(:,N+1)'*S*yal_x(:,N+1);

ops = sdpsettings('solver','cplex','verbose',0);
opt_pair = optimizer(con,yal_obj,ops,[yal_x0],[reshape(yal_x,(N+1)*n,1);reshape(yal_u,N*m,1)]);

%-------- solve w/ cplex----------------------------------%
X(:,1) = x_init;
for ttt = 1:50 
    if ttt>1
        x_init = XX(:,1);
    end
    [xu,errorcode] = opt_pair{[x_init]};
    if errorcode
       yalmiperror(errorcode)
       return;
    end
    %% Store trajectories
%     X(:,2:N+1) = reshape(xu(n+1:(N+1)*n),n,N);
%     U(:,1:N) = reshape(xu((N+1)*n+1:end),m,N);
    XX = reshape(xu(n+1:(N+1)*n),n,N);
    X(:,ttt+1) = XX(:,1);
    UU = reshape(xu((N+1)*n+1:end),m,N);
    U(:,ttt) = UU(:,1);
end
TTT = ttt;%N+1;

value_fnct = 0;
        for ttt = 1:TTT-1
            value_fnct = value_fnct + 0.5*X(:,ttt)'*Q*X(:,ttt) + 0.5*U(:,ttt)'*R*U(:,ttt);
        end
        value_fnct = value_fnct + 0.5*X(:,TTT)'*S*X(:,TTT);
        
figure(1);
InvSet{end}.plot('color','r');
hold on
InvSet{I}.plot('color','g');
Omega.plot('color', 'y')
% for ttt = N+1:35
%     X(:,ttt+1) = (A-B*K)*X(:,ttt);
% end
P1 = plot(X(1,:), X(2,:),'-ok','LineWidth', 2);
hold off
hcg1 = title('State trajectories');
xlabcg1 = xlabel('$x_1$'); ylabcg1 = ylabel('$x_2$');
set(hcg1,'Interpreter','Latex');  
set(xlabcg1,'Interpreter','Latex');
set(ylabcg1,'Interpreter','Latex');
set(hcg1, 'FontSize', 14);
set(xlabcg1, 'FontSize', 14);
set(ylabcg1, 'FontSize', 14);