function [newhor,checkhor,u,x] = sim_sys_dyns(dat,u,oldhor)

% Forward propagation of the dynamics for [hor.length+1,Ts] 
% checkhor : last time epoch before entering the invariant set
% oldhor   : last time epoch before entering the constraint set
% newhor   : last time epoch before entering the constraint set
u = reshape(u,dat.nu,oldhor);
x(:,1) = dat.x_init;
for i = 1:oldhor
    x(:,i+1) = dat.A*x(:,i) + dat.B*u(:,i);
end
j = i+1;

if ( max(dat.Hf*x(:,j) - dat.hf) < 0 )
    checkhor = j-1;
end

% j = oldhor;
while ( max(dat.Hf*x(:,j) - dat.hf) >= 0 )
    checkhor = j;
    u(:,j)   = -dat.K*x(:,j);
    x(:,j+1) = dat.A*x(:,j) + dat.B*u(:,j);
    j = j + 1;
end
u(:,j)   = -dat.K*x(:,j);

% % if ( checkhor == oldhor )
% %     newhor = checkhor;
% % end


for i = oldhor:checkhor+1
        if ( (max(dat.Cx*x(:,i) - dat.cx) < 0) && (max(dat.Cu*u(:,i) - dat.cu) < 0) )
            newhor = i-1;
        end
end
u = reshape(u(:,1:newhor),dat.nu*newhor,1);

