function controller = feas_x_init(m,n,N,A,B,Q,R,S,Cu,cu,Cx,cx,Hf,hf)
% X_INIT = feas_x_init(model,n)

%% Initial state - check feasibility!

% %     temp = [];
% %     ctrl = MPCController(model,40);
% %     loop = ClosedLoop(ctrl, model);
% %     Nsim = 30;
% %     for i = 1:5e2
% %             i
% %             x_init = .1*randn(n,1); % quad
% % %             x_init = [-3 + 4*randn(1,1); 0.3 + 0.4*randn(1,1)]; % small
% %             data{i} = loop.simulate(x_init, Nsim);
% %             if ~isempty(data{i}.cost)
% %                 temp{i} = x_init;
% %             end
% % %             [~,errorcode] = controller{x_init};
% % %             if ( errorcode == 0 ), X_INIT(:,count) = x_init; end
% %     end
% %     
% %     X_INIT = [];
% %     count = 1;
% %     for i = 1:length(temp)
% %         if ~isempty(temp{i})
% %             X_INIT(:,count) = temp{i};
% %             count = count + 1;
% %         end
% %     end
    
    
    u = sdpvar(m,N);    x = sdpvar(n,N+1);
    x0 = sdpvar(n,1);   temp = [];

    Constr = [];
    Object = 0;
    x(:,1) = x0;
    for k = 1:N
     Object = Object + .5*x(:,k)'*Q*x(:,k) + .5*u(:,k)'*R*u(:,k);
     Constr = [Constr, Cu*u(:,k)<= cu, Cx*x(:,k) <= cx];
     x(:,k+1) = A*x(:,k) + B*u(:,k);
    end
    Object = Object + .5*x(:,N+1)'*S*x(:,N+1);
    Constr = [Constr, Hf*x(:,N+1) <= hf];

    % g=solvesdp([Constr,x0 == x_init], Object, sdpsettings('solver','cplex'));
    ops = sdpsettings('solver','cplex','verbose',2);
    controller = optimizer(Constr,Object,ops,x0,u);
    
%     for count = 1:1e3
%             count
%             x_init = .1*randn(n,1); % quad
% %             x_init = [-3 + 4*randn(1,1); 0.3 + 0.4*randn(1,1)]; % small
%             [~,errorcode] = controller{x_init};
%             if ( errorcode == 0 ), temp(:,count) = x_init; end
%     end
    
%     X_INIT = [];
%     count = 1;
%     for i = 1:size(temp,2)
%         if (temp(1,i)~=0)
%             X_INIT(:,count) = temp(:,i);
%             count = count + 1;
%         end
%     end
% % %             
