function [InvSet] = feasible_set(T,dat,model)

% Computes feasible set of intial states (region of attraction) using 
% i-step controllable sets Ki(X,Xf) (from the inside) or i-step admissible
% sets Ci(X) (from the outside) for a given horizon N.
% Generates comparison plots with maximal control invariant set.

if ( T == Inf )
    InvSet = model.invariantSet();
else
    O = Polyhedron(dat.Hf,dat.hf);
    F = dat.Hf;
    f = dat.hf;

    for ttt = T:-1:1
%         Oprev = O;
        % Compute the pre-set
        temp_preO = projection(Polyhedron([F*dat.A F*dat.B; zeros(size(dat.Cu,1),dat.nx) dat.Cu],[f;dat.cu]), 1:dat.nx);
        % Intersect pre-set w/ constraints
        F = [temp_preO.H(:,1:end-1); dat.Cx];
        f = [temp_preO.H(:,end); dat.cx];
    end
        InvSet = Polyhedron(F,f);
end



