function [x,u] = TV_Riccati(horlen,dat,lam,lamf,xinit)
% Riccati recursion for the augmented time-varying LQR
    k = zeros(dat.nu,horlen+1);
    p = [zeros(dat.nx,horlen) -dat.Hf'*lamf];
    %r(:,horOld+1) = 0;
    for ttt = horlen:-1:1
        k(:,ttt) = dat.L'\(dat.L\(-dat.w^(ttt-1)*dat.Cu'*lam(dat.no.con.x+1:end,ttt)+dat.B'*p(:,ttt+1)));
        p(:,ttt) = -dat.w^(ttt-1)*dat.Cx'*lam(1:dat.no.con.x,ttt) + dat.A'*p(:,ttt+1) - dat.K'*(dat.R+dat.B'*dat.S*dat.B)*k(:,ttt);  
    end
    u = zeros(dat.nu,horlen);
    x(:,1) = xinit;
    for ttt = 1:horlen
        u(:,ttt)   = -dat.K*x(:,ttt)-k(:,ttt);
        x(:,ttt+1) = dat.A*x(:,ttt) + dat.B*u(:,ttt);
    end
end