function [obj,gradL,gradLf] = funOracle(x, u, dat, lam, lamf, horNew)
% Returns function value and gradient of dual function h*(lam)

tempSpan = 0:horNew-1;
tempW = diag(dat.w.^tempSpan);

obj = (1/2)*trace(x(:,1:horNew)'*dat.Q*x(:,1:horNew)) + (1/2)*trace(u'*dat.R*u) -...
    trace([dat.Cx*x(:,1:horNew)-repmat(dat.cx,1,horNew);dat.Cu*u-repmat(dat.cu,1,horNew)]'*(lam*tempW));
obj = -(obj + (1/2)*x(:,horNew+1)'*dat.S*x(:,horNew+1) - (dat.Hf*x(:,horNew+1)-dat.hf)'*lamf); %h*(lam)=-L(lam,x*(lam),u*(lam))

gradL = [dat.Cx*x(:,1:horNew)-repmat(dat.cx,1,horNew);dat.Cu*u-repmat(dat.cu,1,horNew)]*tempW;
gradLf = dat.Hf*x(:,horNew+1)-dat.hf;