function [x,u] = SolveKKT(dat,hor,xinit,lambda)
% Projection on the active constraint sets

[active.r,active.c] = find(lambda);
% build objective matrix K11
temp1 = blkdiag(dat.Q,dat.R);
E = sparse([]);
for t = 1:hor.length
    E = blkdiag(E,temp1);
end
E = blkdiag(E, dat.S);
% build dynamics matrix K21
temp2 = sparse([-dat.A -dat.B]);
temp3 = sparse([speye(dat.nx) sparse(dat.nx,dat.nu)]);
M1 = sparse([]); M2 = sparse([]);
for t = 1:hor.length
    M1 = blkdiag(M1, temp2);
    M2 = blkdiag(M2, temp3);
end
M1 = [M1 sparse(hor.length*dat.nx,dat.nx)];
M2 = blkdiag(M2, speye(dat.nx));
% build active constraints matrix K31
tempC = blkdiag(dat.Cx,dat.Cu);
tempc = [dat.cx; dat.cu];
M3 = sparse([]);
m3 = sparse([]);
for t = 1:hor.length
    if  ismember(t,active.c)
        M3 = blkdiag(M3,tempC(active.r(active.c==t),:));
        m3 = [m3; tempc(active.r(active.c==t),:)];
    else
        M3 = blkdiag(M3,zeros(size(tempC)));
        m3 = [m3; zeros(size(tempc,1),1)];
    end
end
if  ismember(hor.length+1,active.c)
    M3 = blkdiag(M3,dat.Cx(active.r(active.c==hor.length+1),:));
    m3 = [m3; dat.cx(active.r(active.c==hor.length+1),:)];
else
    M3 = blkdiag(M3,zeros(size(dat.Cx)));
    m3 = [m3; zeros(size(dat.cx,1),1)];
end
 M3(all(M3==0,2),:) = [];
 m3(all(m3==0,2),:) = [];

M1 = [sparse(dat.nx,(dat.nx+dat.nu)*hor.length+dat.nx); M1];
G = [M2 + M1; M3];
KKT = [ E G'; G zeros(size(G,1),size(G,1)) ];
RHS = [zeros((dat.nx+dat.nu)*hor.length+dat.nx,1); xinit; zeros(hor.length*dat.nx,1); m3];
solKKT = KKT \ RHS;
z = solKKT(1:(dat.nx+dat.nu)*hor.length+dat.nx,1);
xu = reshape(z(1:(dat.nx+dat.nu)*hor.length,1),dat.nx+dat.nu,hor.length);
x = full([xu(1:dat.nx,:) z((dat.nx+dat.nu)*hor.length+1:end,1)]);
u = full(xu(dat.nx+1:end,:));

end