function [newhor,u,x] = SimSys(dat,x,u,horOld)

% Forward propagation of the dynamics for [hor.length+1,Ts] 
% checkhor : last time epoch before entering the invariant set
% oldhor   : last time epoch before entering the constraint set
% newhor   : last time epoch before entering the constraint set
% u = reshape(u,m,horOld);
% x(:,1) = x_init;
% for i = 1:horOld
%     x(:,i+1) = dat.A*x(:,i) + dat.B*u(:,i);
% end

if ( max(dat.Hf*x(:,end) - dat.hf) <= 0 )
    newhor = horOld;
    u = u;
    x = x;
else
    j = size(x,2);
    while ( max(dat.Hf*x(:,j) - dat.hf) > 0 )
        horCheck = j;
        u(:,j)   = -dat.K*x(:,j);
        x(:,j+1) = dat.A*x(:,j) + dat.B*u(:,j);
        j = j + 1;
    end
%     u(:,j)   = -dat.K*x(:,j);
    newhor = j-1;
end
