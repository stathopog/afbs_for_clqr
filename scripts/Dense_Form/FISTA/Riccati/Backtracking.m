    function Lip = Backtracking(xinit, x, u, dat, lam, lamf, horNew, Lk)
    %% Computes Lipschitz constant of F(x) = { h^*(x), x<=0 } using      %%
    %% backtracking as suggested in FISTA                                %%
        
    eta = 1.2; 
    
    [h,g_L,g_Lf] = funOracle(x, u, dat, lam, lamf, horNew); % h*(lam) and grad(h*(lam))
    p_L = min(0, lam - (1/Lk)*g_L); % updated iterate of quadratic model
    % F function of x(p_L), u(p_L). Have to run Riccati @ new duals
    p_Lf = min(0, lamf - (1/Lk)*g_Lf);
    [x,u] = TV_Riccati(horNew,dat,p_L,p_Lf,xinit);
    [F,~] = funOracle(x, u, dat, p_L, p_Lf, horNew); % h*(p_L)
    Q_L = modelOracle(h, p_L, p_Lf, g_L, g_Lf, dat, lam, lamf, horNew, Lk);

    if ( F <= Q_L+eps )
        Lip = Lk;
    end
    kkk = 0;
    while ( F >= Q_L ) && (Lk <= dat.beta) && (kkk <= 1e2)
        Lk   = eta*Lk;
        p_L = min(0, lam - (1/Lk)*g_L);
        p_Lf = min(0, lamf - (1/Lk)*g_Lf);
        [x,u] = TV_Riccati(horNew,dat,p_L,p_Lf,xinit);
        [F,~] = funOracle(x, u, dat, p_L, p_Lf, horNew);
        Q_L = modelOracle(h, p_L, p_Lf, g_L, g_Lf, dat, lam, lamf, horNew, Lk);
        if ( Lk >= dat.beta )
            Lip = dat.beta;
        elseif (F <= Q_L)
            Lip = Lk;
        end
        kkk = kkk + 1;
    end