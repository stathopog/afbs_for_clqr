function [Q_L] = modelOracle(F, p_L, p_Lf, g, g_Lf, dat, lam, lamf, horNew, Lk)
% Returns function value and gradient of quadratic majorizer Q_L(lam)

tempSpan = 0:horNew-1;
tempW = diag(dat.w.^tempSpan);
Q_L = F + trace((p_L-lam)'*(g*tempW)) + (p_Lf-lamf)'*g_Lf + (Lk/2)*trace( ((p_L-lam)'*(p_L-lam))*tempW + (Lk/2)*(p_Lf-lamf)'*(p_Lf-lamf) );
