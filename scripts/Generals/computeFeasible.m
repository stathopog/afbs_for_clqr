%% Function to compute feasible set of an MPC controller

function [feas]=computeFeasible(mpc,sys,invar,h)
    
    [Klqr,Slqr,Elqr]=dlqr(sys.A,sys.B,mpc.Q,mpc.R);

    opts.proj=3; % Block elimination
    
    [H,K]=double(invar);

    bk_lw=zeros((mpc.N-2)*size(sys.Hx,1),(mpc.N-2)*sys.nu);
    for i=0:(mpc.N-3)
        disp(bk_lw);
        disp(kron(diag(ones(mpc.N-1-i,1),-i),sys.Hx*sys.A^i*sys.B));
        bk_lw=bk_lw+kron(diag(ones(mpc.N-2-i,1),-i),sys.Hx*sys.A^i*sys.B);
    end
    Apow=[H*sys.B];
    for i=1:(mpc.N-2)
        Apow=[H*sys.A^i*sys.B,Apow];
    end
    bk_lw=[bk_lw,zeros(size(sys.Hx,1)*(mpc.N-2),sys.nu)];
    bk_lw=[bk_lw;Apow];
    
    Apow=[sys.Hx*sys.A];
    for i=2:mpc.N-1
        if (i==mpc.N-1)
            Apow=[Apow;H*sys.A^i];
        else
            Apow=[Apow;sys.Hx*sys.A^i];
        end
    end
      
    Abig=[sys.Hx,zeros(size(sys.Hx,1),(mpc.N-1)*sys.nu);[Apow,bk_lw]];
    Bbig=h; 
    Bbig=[Bbig;K];
    
    Abig=[Abig;[sys.Hu*Klqr,zeros(size(sys.Hu,1),(mpc.N-1)*sys.nu)]];
    Abig=[Abig;[zeros(size(sys.Hu,1)*(mpc.N-1),sys.nx),kron(eye(mpc.N-1),sys.Hu)]];
    Bbig=[Bbig;kron(ones(mpc.N,1),sys.hu)];
    
    feas=projection(polytope(Abig,Bbig),1:sys.nx,opts);
end
