%% Generate data for osc_box_control
% data set up (time-invariant):
clear all;
randn('state',0);
rand('state',0);

% set to 1 to run admm in matlab
test_on = 1;

%% Defining the problem's parameters
% System's dimensions and control horizon
alpha = 1.8;
% n - states, m - controls
n = 2; m = 1; T = 20; rho = 20; x_init = randn(n,1);
% n = 20; m = 5; T = 20; rho = 50; x_init = 5*randn(n,1);
% n = 50; m = 20; T = 30; rho = 50; x_init = 5*randn(n,1);

% % A = randn(n);
% % A = A/max(abs(eig(A)));
% % B = randn(n,m);
% % B(1,1) = .5; B(2,1) = 0;
c = 0*randn(n,1);

A = [1.988 -.998; 1 0]; B = [.125; 0];
Q = eye(n); R = 10*eye(m);
mat=blkdiag(Q,R);

% % mat = randn(n+m); mat = mat*mat';
% % mat(1:n,n+1:end) = zeros(n,m);
% % mat(n+1:end,1:n) = zeros(m,n);
% % mat = sparse(mat);

% % Q = mat(1:n,1:n);
% % R = mat(n+1:end,n+1:end) + 10*eye(m);
% % S = mat(1:n,n+1:end);

q = zeros(n,1);
r = zeros(m,1);

RHS = [-repmat([q;r],T+1,1); x_init; repmat(c,T,1)];

% prox data
umax = 1.5;
umin = -umax;
xmax = 3;
xmin = -xmax;
%

%% set up matrix - pre-factorize
temp1 = sparse(mat+rho*eye(n+m));
E = sparse([]);
for t = 1:T+1
    E = blkdiag(E,temp1);
end
temp2 = sparse([-A -B]);
temp3 = sparse([speye(n) sparse(n,m)]);
M1 = sparse([]); M2 = sparse([]);
for t = 1:T
    M1 = blkdiag(M1, temp2);
    M2 = blkdiag(M2, temp3);
end
M2 = blkdiag(M2, temp3);
M1 = ( [[sparse(n,(n+m)*T); M1] sparse((T+1)*n,n+m)] );
G = M2 + M1;
% regularized:
M = [ E G'; G 0*-1e-6*speye((T+1)*n,(T+1)*n) ];
M = sparse(M);

% % %% solve using cvx
% % tic
% % cvx_begin
% %     cvx_solver 'sdpt3'
% %     variables X(n,T+1) U(m,T+1)
% %     obj=0;
% %     for t=1:T+1
% %         obj = obj + 0.5 * [X(:,t);U(:,t)]'*mat*[X(:,t);U(:,t)]...
% %             + q'*X(:,t) + r'*U(:,t);
% %     end
% %     for t=1:T
% %         X(:,t+1) == A*X(:,t) + B*U(:,t) + c;
% %     end
% %     minimize (obj)
% %     subject to 
% %     X(:,1) == x_init;
% %     U <= umax;
% %     U >= umin;
% %     X <= xmax;
% %     X >= xmin;
% % cvx_end
% % t_cvx = toc;
% % disp(sprintf('CVX took %f seconds to solve',t_cvx))


%% run admm
if test_on
    Mat = kron(eye(T+1), mat);
    q_obj = kron(ones(T+1,1), 2*[q; r]);
    QUIET    = 0;
    MAX_ITER = 3000;
    EPS_ABS   = 1e-3;
    EPS_REL   = 1e-3;
    disp('running factorization')
    tic
    [L,D,P] = ldl(M,10*1e-6);    
    disp('running box constrained example')
    tic
    % Dimensions
    x = zeros(n,T+1);  % First primal variable - state
    u = zeros(m,T+1);  % First primal variable - input
    x_t = zeros(n,T+1);  % Second primal variable - state
    u_t = zeros(m,T+1);  % Second primal variable - input
    z = zeros(n,T+1);  % Dual variable - state
    y = zeros(m,T+1);  % Dual variable - input
    sol = zeros((n+m)*(T+1),1);  % Vector of KKT solution

    if ~QUIET
        fprintf('%3s\t%10s\t%10s\t%10s\t%10s\t%10s\n', 'iter', ...
          'r norm', 'eps pri', 's norm', 'eps dual', 'objective');
    end
    
    % Initializing the KKT right-hand side vector
    f = zeros((n+m)*(T+1),1);
    h = [x_init; repmat(c,T,1)];
    
    for k=1:MAX_ITER
        
        %% (x, u) - update

        f = reshape(-rho*[x_t+z; u_t+y],(n+m)*(T+1),1) + ...
                repmat([q; r],T+1,1);

        sol = P*(L'\(D\(L\(P'*[-f; h]))));
        sol = reshape(sol(1:(n+m)*(T+1)),n+m,T+1);

        x = sol(1:n,:);
        u = sol(n+1:end,:);
        x_all(:,:,k) = reshape(x,n,T+1);
        
       %% (x_t, u_t) - update
       
       x_t_old = x_t;
       u_t_old = u_t;
       % relaxation
       x = alpha*x + (1-alpha)*x_t_old;
       u = alpha*u + (1-alpha)*u_t_old; 
       v = x - z;
       w = u - y;

       u_t = max( min(w,umax), umin );
       x_t = max( min(v,xmax), xmin );
       x_t(:,1) = x_init;
       x_t_all(:,:,k) = reshape(x_t,n,T+1); 
       %% (z, y) - update
       
       z = z + x_t - x;
       y = y + u_t - u;
       
       %% diagnostics, reporting, termination checks    
       xu = reshape([x_t; u_t], (T+1)*(n+m),1);  
       history.objval(k)  = 0.5 * ( xu'*Mat*xu + q_obj'*xu );
        
       history.r_norm(k) = norm( [x - x_t; u - u_t], 'fro' );
       history.s_norm(k) = norm( rho * [x_t - x_t_old; u_t - u_t_old],'fro' );
       history.eps_pri(k) = sqrt((n+m)*(T+1))*EPS_ABS + ...
                     EPS_REL*max( norm([x_t; u_t ], 'fro'), norm([x; u ], 'fro'));
       history.eps_dual(k) = sqrt((n+m)*(T+1))*EPS_ABS + EPS_REL*norm([z; y], 'fro');
       if ~QUIET
            fprintf('%3d\t%10.4f\t%10.4f\t%10.4f\t%10.4f\t%10.2f\n', k, ...
            history.r_norm(k), history.eps_pri(k), history.s_norm(k),...
            history.eps_dual(k), history.objval(k));
        end
       
       if (history.r_norm(k)  < history.eps_pri(k) &&...
               history.s_norm(k) < history.eps_dual(k))
             break;
       end   
    end
    toc
end

% % % %% Reporting
% % % K = length(history.objval);
% % % 
% % % l = figure;
% % % semilogy(1:K, abs(history.objval - cvx_optval), 'k--', 'MarkerSize', 10, 'LineWidth', 2);
% % % l1 = ylabel('$\Pi_{\mathcal D}\Big(\phi(x^k,u^k) + \psi(\tilde x^k,\tilde u^k)\Big)-p^*$', 'fontsize', 14); 
% % % xlabel('iter (k)');
% % % legend('Objective residual');
% % % set(l1,'Interpreter','LaTex');
% % % 
% % % h = figure;
% % % plot(1:K, history.objval, 'k', 1:K, cvx_optval, 'k--', 'MarkerSize', 10, 'LineWidth', 2);
% % % l1 = ylabel('$\Pi_{\mathcal D}\Big(\phi(x^k,u^k) + \psi(\tilde x^k,\tilde u^k)\Big)$', 'fontsize', 14); 
% % % xlabel('iter (k)');
% % % legend('Objective','Optimal value');
% % % set(l1,'Interpreter','LaTex');
% % % 
% % % g = figure;
% % % subplot(2,1,1);
% % % semilogy(1:K, max(1e-8, history.r_norm), 'k', ...
% % %     1:K, history.eps_pri, 'k--',  'LineWidth', 2);
% % % l21 = ylabel('$\|r\|_2$', 'fontsize', 16);
% % % l22 = legend('$\|r\|_2$','$\epsilon^\mathrm{pri}$');
% % % set(l21,'Interpreter','LaTex');
% % % set(l22,'Interpreter','LaTex');
% % % 
% % % subplot(2,1,2);
% % % semilogy(1:K, max(1e-8, history.s_norm), 'k', ...
% % %     1:K, history.eps_dual, 'k--', 'LineWidth', 2);
% % % l31 = ylabel('$\|s\|_2$', 'fontsize', 16); xlabel('iter (k)');
% % % l32 = legend('$\|s\|_2$','$\epsilon^\mathrm{dual}$');
% % % set(l31,'Interpreter','LaTex');
% % % set(l32,'Interpreter','LaTex');

for i = 1:floor(k/5)
figure;
hold on;
plot(x_init(1),x_init(2),'+k','MarkerSize',10);
rectangle('Position',[xmin,xmin,2*xmax,2*xmax], 'LineWidth', 3);
for t = 1:T+1    
plot(x_all(1,t,i),x_all(2,t,i),'*k')
plot(x_t_all(1,t,i),x_t_all(2,t,i),'og')
end
title('Black: LQR, Green: Prox')
hold off;
pause
end

