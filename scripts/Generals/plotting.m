% hitting times for 2-states
figure(1); 
HTsmall = load('Hitting_Times_2states_ama.mat');
range = 7:1:13;
hist(HTsmall.HTs,range);
hcg1 = title('Hitting times distribution for the 2 states system');
xlabcg1 = xlabel('Hitting times $T^{\infty}$'); ylabcg1 = ylabel('$\#$ of problems');
set(hcg1,'Interpreter','Latex');
set(xlabcg1,'Interpreter','Latex');
set(ylabcg1,'Interpreter','Latex');
set(get(gca,'child'),'FaceColor',0.35*[1 1 1],'EdgeColor','k');

% hitting times for quad
figure(2);
HTquad = load('Hitting_Times_quad_ama.mat');
hist(HTquad.HTs);
hcg2 = title('Hitting times distribution for the quad system');
xlabcg2 = xlabel('Hitting times $T^{\infty}$'); ylabcg2 = ylabel('$\#$ of problems');
set(hcg2,'Interpreter','Latex');  
set(xlabcg2,'Interpreter','Latex');
set(ylabcg2,'Interpreter','Latex');
set(get(gca,'child'),'FaceColor',0.35*[1 1 1],'EdgeColor','k');

% ama iterations for small
figure(3);
ITsmallama = load('Iters_2states_ama.mat');
hist(ITsmallama.ITERs);
hcg3 = title('$\#$ of iterations for convergence of AMA for the 2 states system');
ylabcg3 = ylabel('$\#$ of problems'); xlabcg3 = xlabel('Iterations');
set(hcg3,'Interpreter','Latex');
set(xlabcg3,'Interpreter','Latex'); set(ylabcg3,'Interpreter','Latex');
muama = mean(ITsmallama.ITERs); %The mean
set(get(gca,'child'),'FaceColor',0.35*[1 1 1],'EdgeColor','k');
%Overlay the mean
hold on
plot([muama,muama],ylim,'k--','LineWidth',2)
hold off

% fama iterations for small
figure(4);
ITsmallfama = load('Iters_2states_fama.mat');
hist(ITsmallfama.ITERs);
hcg3 = title('$\#$ of iterations for convergence of FAMA for the 2 states system');
ylabcg3 = ylabel('$\#$ of problems'); xlabcg3 = xlabel('Iterations');
set(hcg3,'Interpreter','Latex');
set(xlabcg3,'Interpreter','Latex'); set(ylabcg3,'Interpreter','Latex');
muama = mean(ITsmallfama.ITERs); %The mean
set(get(gca,'child'),'FaceColor',0.35*[1 1 1],'EdgeColor','k');
%Overlay the mean
hold on
plot([muama,muama],ylim,'k--','LineWidth',2)
hold off

% speedup for small
figure(5);
ITsmallfama = load('Iters_2states_fama.mat');
rel_diff = ITsmallama.ITERs ./ ITsmallfama.ITERs;
hist(rel_diff,20);
hcg4 = title('Acceleration gained with FAMA for the 2 states system');
ylabcg4 = ylabel('$\#$ of problems'); xlabcg4 = xlabel('Speedup FAMA/AMA');
set(xlabcg4,'Interpreter','Latex'); set(ylabcg4,'Interpreter','Latex');
set(hcg4,'Interpreter','Latex');  
murel = mean(rel_diff); %The mean
set(get(gca,'child'),'FaceColor',0.35*[1 1 1],'EdgeColor','k');
%Overlay the mean
hold on
plot([murel,murel],ylim,'k--','LineWidth',2)
hold off

% ama and fama iterations for quad
figure(6);
ITquadama = load('Iters_quad_ama.mat');
hist(ITquadama.ITERs,15);
axis([0 22000 0 35])
hcg5 = title('$\#$ of iterations for convergence of AMA for the quad system');
ylabcg5 = ylabel('$\#$ of problems'); xlabcg5 = xlabel('Iterations');
set(xlabcg5,'Interpreter','Latex'); set(ylabcg5,'Interpreter','Latex');
set(hcg5,'Interpreter','Latex'); 
set(get(gca,'child'),'FaceColor',0.35*[1 1 1],'EdgeColor','k');
figure(6);
ITquadfama = load('Iters_quad_fama.mat');
rel_diff = ITquadama.ITERs ./ ITquadfama.ITERs;
hist(rel_diff,20);
hcg6 = title('Acceleration gained with FAMA for the quad system');
ylabel('# of problems'); xlabcg6 = xlabel('Speedup FAMA/AMA');
set(get(gca,'child'),'FaceColor',0.35*[1 1 1],'EdgeColor','k');


% set j=22 and run quad
figure(7);
plot(hor.length_track,'*');
hcg7 = title('Convergence of the hitting time sequence to $T^{\infty}$');
ylabcg7 = ylabel('$T^k$'); xlabcg7 = xlabel('Iterations');
set(xlabcg7,'Interpreter','Latex'); set(ylabcg7,'Interpreter','Latex');
set(hcg7,'Interpreter','Latex');  