function controller = RoA(A,B,C,D,n,m,p,umin,umax,ymin,ymax,xmin,xmax,T,Q,R,S)

% System structure in mpt format
sysStruct.A = A;
sysStruct.B = B;
sysStruct.C = C;
sysStruct.D = D;
sysStruct.umin = kron(ones(m,1),umin);
sysStruct.umax = kron(ones(m,1),umax);
sysStruct.ymin = kron(ones(p,1),ymin);
sysStruct.ymax = kron(ones(p,1),ymax);
% sysStruct.xmin = kron(ones(n,1),xmin);
% sysStruct.xmax = kron(ones(n,1),xmax);

% Problem structure in mpt format
probStruct.N = T;
probStruct.Q = Q;
probStruct.R = R;
probStruct.norm = 2;
probStruct.subopt_lev=0;
probStruct.P_N = S;
probStruct.Tconstraint = 1;

controller = mpt_control(sysStruct, probStruct);