%% Infinite horizon MPC using ADMM - main

clear all;
close all;

for i = 1:3
    randn('state',3);
    rand('state',2);

    %% Defining the problem parameters
    % System's dimensions
    n = 2; m = 2;
%     n = 2; m = 1;
    N = 1+i-1;

    % Generating a random system
    A = randn(n);
    A = A / max(abs(eig(A)));
    B = randn(n,m);

    Q1 = randn(n,n);    R1 = randn(m,m);
    Q = Q1'*Q1;
    R = R1'*R1;
    S = kron(eye(N-1),blkdiag(Q,R));
    S = blkdiag(S,Q);
    G = KKT_system(A,B,n,m,N);
    PN = eye(size(S))-G'*((G*G')\G);
    PD = G'*((G*G')\G);
    M = PN*S*PN;
    L = PD*inv(S)*PD;
    sprintf('The trace is %12.3f\n',sum(diag(M)))
    sprintf('The norm of the matrix M is %12.3f\n',norm(M))
    sprintf('The rank of the matrix M is %12.3f\n',rank(M))
end