function G = KKT_system(A,B,n,m,N)      

temp2 = sparse([-A -B]);
temp3 = sparse([speye(n) sparse(n,m)]);
M1 = sparse([]); M2 = sparse([]);
for t = 1:N-1
    M1 = blkdiag(M1,temp2);
    M2 = blkdiag(M2,temp3);
end
M2 = blkdiag(M2,temp3);
M2 = M2(:,1:end-m);
M1 = ([[sparse(n,(n+m)*(N-1)); M1] sparse(N*n,n)]);
G = M2 + M1;