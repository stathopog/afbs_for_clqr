function [sol,x_T,LS,g2,A21,A11inv] = solve_lin_sys_subs(dT,A111inv,Sinv,n,m,A,B,x_init,A21,barCx,barCu,rho,tildew,horlength)
%% Solves the linear system at k=3,... AMA iterate

A11inv = kron(eye(horlength),A111inv); % inv(A11)
A11inv = blkdiag(A11inv,Sinv);
A21 = [A21 zeros(size(A21,1),dT*(m+n))];
Add = [];
temp2 = sparse([-A -B]);
temp3 = sparse([speye(n) sparse(n,m)]);
M1 = sparse([]); M2 = sparse([]);
for t = 1:dT
    M1 = blkdiag(M1,temp2);
    M2 = blkdiag(M2,temp3);
end
M1t = ([[sparse(dT*n,(n+m)*(horlength-dT)) M1] sparse(dT*n,n)]);
M2t = [sparse(dT*n,(n+m)*(horlength-(dT-1))) M2];
M2t = M2t(:,1:end-m);
Add = M2t + M1t;
A21 = [A21; Add];
A22 = 0*speye(size(A21,1));

Schur = A22-A21*A11inv*A21';
LS = chol(-Schur,'lower');

g1 = [];
% g2 = [g2; zeros(dT*n,1)];
g2 = [x_init; zeros(horlength*n,1)];
for t = 1:horlength
    g1(:,t) = [rho*barCx'*tildew(:,t); ...
               rho*barCu'*tildew(:,t)];
end
g1 = reshape(g1, (n+m)*horlength, 1);
g1 = [g1; zeros(n,1)];

nu  =  -LS' \ (LS \ (g2-A21*A11inv*g1));
sol = A11inv*(g1-A21'*nu);
x_T = sol(horlength*(n+m)+1:horlength*m+(horlength+1)*n);
sol = reshape(sol(1:(n+m)*horlength),n+m,horlength);