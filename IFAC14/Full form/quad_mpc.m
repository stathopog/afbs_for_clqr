function [n,m,Ad,Bd,Cd,Cx,Cu,cx,cu] = quad_mpc()

quad = load('../Data/quadForGiorgos');
n = 12; m = 4;
quad.Cc = eye(n); quad.Dc = zeros(n,m);
system = ss(quad.Ac,quad.Bc,quad.Cc,quad.Dc);
systemd = c2d(system,.1,'zoh');
[Ad,Bd,Cd,Dd] = ssdata(systemd);
Cx = kron([1;-1],eye(n));
cx = [kron([1e0;1e0],ones(5,1)); 1; 1;...
      kron([10*(pi/180); 10*(pi/180)],ones(2,1));...
      pi; pi; kron([15*(pi/180); 15*(pi/180)],ones(2,1));...
      60*(pi/180); 60*(pi/180)];
Cu = kron([1;-1],eye(m));
cu = kron([.3;.7], ones(m,1));