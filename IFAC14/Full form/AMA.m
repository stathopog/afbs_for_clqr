function [k,x,u,sigma,w,hor,time] = AMA(MAXITER,x_init,K,A,B,omega,A111inv,Sinv,n,m,no,time,bar,rho,alpha)

    for k = 1:MAXITER

    %% step 1
        switch (k)

            case 1  %@ first iteration solve unconstrained LQR
                hor.sim = 200;
                hor.length = 0;
    %             disp(sprintf('Initial horizon length is %d', hor.length))
                old.hor.length = hor.length;
                x = []; u = []; 
                x(:,1) = x_init;
                for i = 1:hor.sim
                    u(:,i) = -K*x(:,i);
                    x(:,i+1) = A*x(:,i) + B*u(:,i);
                end
                values.length = max(omega.ineq_1*x - repmat(omega.ineq_2,1,hor.sim+1),[],1);
                hor.length = min(find(values.length<=0));
                w = zeros(no.con.all,hor.sim);
                tilde.w = w;

            case 2  %@ second iteration build the KKT system
                old.x = x; old.u = u;
                old.hor.length = hor.length;

                % Solve the linear system 
                [sol,x_T,LS,g2,A21,A11inv] = solve_lin_sys_first(A111inv,Sinv,n,m,A,B,x_init,bar.Cx,bar.Cu,rho,tilde.w,hor.length);
                x = [sol(1:n,:) x_T];
                u = sol(n+1:end,:);

                % Forward propagation of the dynamics for [hor.length+1,hor.sim]
                [z1,v1] = sim_sys_dyns_2(x_T,hor.length,hor.sim,K,A,B);
                x(:,hor.length+1:hor.sim+1) = z1(:,1:end);
                u(:,hor.length+1:hor.sim) = v1;

                % Update running maximum of horizon length
                values.length = max(omega.ineq_1*x - repmat(omega.ineq_2,1,hor.sim+1),[],1);
                hor.length = max(old.hor.length,min(find(values.length<=0))); % running maximum of T(i)


            otherwise   %@ every subsequent iteration augment the KKT system if needed        
                old.x = x; old.u = u;
                dT = hor.length-old.hor.length;
                if dT>=1
                    old.hor.length = hor.length;

                    % Solve the linear system 
%                     old.nu = nu;
                    [sol,x_T,LS,g2,A21,A11inv] = solve_lin_sys_subs(dT,A111inv,Sinv,n,m,A,B,x_init,A21,bar.Cx,bar.Cu,rho,tilde.w,hor.length);
                    x = [sol(1:n,:) x_T];
                    u = sol(n+1:end,:);

                    % Forward propagation of the dynamics for [hor.length+1,hor.sim]
                    [z1,v1] = sim_sys_dyns_2(x_T,hor.length,hor.sim,K,A,B);
                    x(:,hor.length+1:hor.sim+1) = z1(:,1:end);
                    u(:,hor.length+1:hor.sim) = v1;

                    % Update running maximum of horizon length
                    values.length = max(omega.ineq_1*x - repmat(omega.ineq_2,1,hor.sim+1),[],1);
                    hor.length = max(old.hor.length,min(find(values.length<=0))); % running maximum of T(i)

                    if (mod(k,15) == 0)
                        time.x(:,:,time.count) = x;
                        time.count = time.count + 1;
                    end

                else

                    old.hor.length = hor.length;
                    % If Tmax has not changed, use prefactorized matrices to
                    % solve the linear system
                    g1 = [];
                    for t = 1:hor.length
                        g1(:,t) = [rho*bar.Cx'*tilde.w(:,t); ...
                                   rho*bar.Cu'*tilde.w(:,t)];
                    end
                    g1 = reshape(g1, (n+m)*hor.length, 1);
                    g1 = [g1; zeros(n,1)];

%                     old.nu = nu;
                    nu  =  -LS' \ (LS \ (g2-A21*A11inv*g1));
                    sol = A11inv*(g1-A21'*nu);
                    x_T = sol(hor.length*(n+m)+1:hor.length*m+(hor.length+1)*n);
                    sol = reshape(sol(1:(n+m)*hor.length),n+m,hor.length);

                    x = [sol(1:n,:) x_T];
                    u = sol(n+1:end,:);

                    % Forward propagation of the dynamics for [hor.length+1,hor.sim]
                    [z1,v1] = sim_sys_dyns_2(x_T,hor.length,hor.sim,K,A,B);
                    x(:,hor.length+1:hor.sim+1) = z1(:,1:end);
                    u(:,hor.length+1:hor.sim) = v1;

                    % Update running maximum of horizon length
                    values.length = max(omega.ineq_1*x - repmat(omega.ineq_2,1,hor.sim+1),[],1);
                    hor.length = max(old.hor.length,min(find(values.length<=0))); % running maximum of T(i)

                    if (mod(k,15) == 0)
                        time.x(:,:,time.count) = x;
                        time.count = time.count + 1;
                    end
                end

        end

    %% step 2
    sigma = min(0, -repmat(bar.c,1,hor.sim) + bar.Cx*x(:,1:hor.sim) + bar.Cu*u(:,1:hor.sim) - tilde.w/rho);

    %% step 3
    old.w = w;
    w = tilde.w + rho * (repmat(bar.c,1,hor.sim) - bar.Cx*x(:,1:hor.sim) - bar.Cu*u(:,1:hor.sim) + sigma);
    values.w = max(abs(w),[],1);
    hor.violate = max(find(values.w>1e-6));
    disp(sprintf('Violation length and horizon length are %d,%d', hor.violate,hor.length))
    old.alpha = alpha;
    alpha = (1+sqrt(1+4*alpha^2))/2;
% % %     % function evaluation for MFAMA
% % %     if k >2
% % %         diff = evaluate_F(rho,A11inv,A21,g2,g1,hor,no,bar,nu,w,old);
% % %         if diff > 0; w = old.w; else w = wup; end
% % %     end
    tilde.w = w;% + ((old.alpha-1)/alpha)*(w-old.w);

    %% keep latest horizon
    hor.length = max(old.hor.length,hor.length);
    hor.length_track(k) = hor.length;

    %% terminate
    if k > 1
        if norm(x-old.x,'fro') + norm(u-old.u,'fro') <= 1e-4 break; end
    end
    end
end
        
    
% % %           function diff = evaluate_F(rho,A11inv,A21,g2,g1,hor,no,bar,nu,w,old)
% % % %         if tilde.w <= 0
% % %             vect = -A21'*nu + (1/rho)*g1;            vectold = -A21'*old.nu + (1/rho)*g1;
% % %             valFstar = vect'*A11inv*vect + nu'*g2 - (reshape(w(:,1:hor.length),hor.length*no.con.all,1))'*kron(ones(hor.length,1),bar.c);
% % %             valFstarold = vectold'*A11inv*vectold + old.nu'*g2 - (reshape(old.w(:,1:hor.length),hor.length*no.con.all,1))'*kron(ones(hor.length,1),bar.c);
% % %             diff = valFstar - valFstarold;
% % % %         else
% % % %             valFstar = 1e7;
% % % %         end
% % %     end
        
        