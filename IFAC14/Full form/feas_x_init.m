function X_INIT = feas_x_init(n,m,Hf,hf,Q,R,A,B,Cx,Cu,cx,cu,S)

%% Initial state - check feasibility!
% % % X_INIT = [];
% % % for count = 1:3
% % %     for i = 1:50
% % %         x_init = .5*randn(n,1);
% % %         if (sum( (Cx*x_init <= cx)) == length(cx)) && (sum( (Hf*x_init <= hf) ) < size(Hf,1) )
% % %             break;
% % %         end
% % %     end
%     %% Verification
%     N = 150;
%     tic
%     cvx_begin
%     cvx_solver 'sdpt3'
%         variables X(n,N+1) U(m,N)
%         obj = 0;
%         for t = 1:N
%             obj = obj + .5*quad_form([X(:,t); U(:,t)], blkdiag(Q,R));
%         end
%         obj = obj + .5*quad_form(X(:,N+1), S);
%         minimize ( obj )
%         subject to 
%         for t = 1:N
%             X(:,t+1) == A*X(:,t) + B*U(:,t);
%             Cx*X(:,t)<= cx;
%             Cu*U(:,t)<= cu;
%         end
%         X(:,1) == x_init;
%         Cx*X(:,N+1) <= cx;
%     cvx_end
%     toc
% % %     
% % %     if (cvx_optval < 1e8)
% % %         X_INIT = [X_INIT; x_init];
% % %     end
% % % end



    X_INIT = [];
    N = 150;
    u = sdpvar(m,N);
    x0 = sdpvar(n,1);

    Constr = [];
    Object = 0;
    x = x0;
    for k = 1:N
     Object = Object + .5*x'*Q*x + .5*u(:,k)'*R*u(:,k);
     Constr = [Constr, Cu*u(:,k)<= cu, Cx*x <= cx];
     x = A*x + B*u(:,k);
    % %  x = A*x + B*u{k};
    % %  Object = Object + norm(Q*x,1) + norm(R*u{k},1);
    % %  Constr = [Constr, Cu*u{k}<= cu, Cx*x <= cx];
    end
    Object = Object + .5*x'*S*x;
    Constr = [Constr, Cx*x <= cx];

    % g=solvesdp([Constr,x0 == x_init], Object, sdpsettings('solver','cplex'));
    ops = sdpsettings('solver','cplex','verbose',2);
    controller = optimizer(Constr,Object,ops,x0,u);
    
    for count = 1:3e3
        for i = 1:50
            x_init = [1;-2] + .5*randn(n,1);
%             x_init = .5*randn(n,1);
            if (sum( (Cx*x_init <= cx)) == length(cx)) && (sum( (Hf*x_init <= hf) ) < size(Hf,1) )
                break;
            end
        end
        u = controller{x_init};
        if sum(isnan(u))==0
            X_INIT = [X_INIT; x_init];
        end
    end
% % %             
