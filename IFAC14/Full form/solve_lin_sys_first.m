function [sol,x_T,LS,g2,A21,A11inv] = solve_lin_sys_first(A111inv,Sinv,n,m,A,B,x_init,barCx,barCu,rho,tildew,horlength)
%% Solves the linear system at k=2 split Bregman iterate

A11inv = kron(eye(horlength),A111inv); % inv(A11)
A11inv = blkdiag(A11inv,Sinv);
temp2 = sparse([-A -B]);
temp3 = sparse([speye(n) sparse(n,m)]);
M1 = sparse([]); M2 = sparse([]);
for t = 1:horlength
    M1 = blkdiag(M1,temp2);
    M2 = blkdiag(M2,temp3);
end
M2 = blkdiag(M2,temp3);
M2 = M2(:,1:end-m);
M1 = ([[sparse(n,(n+m)*horlength); M1] sparse((horlength+1)*n,n)]);
A21 = M2 + M1;

A22 = 0*speye((horlength+1)*n,(horlength+1)*n);

% solve using block elimination
Schur = A22-A21*A11inv*A21';
LS = chol(-Schur,'lower');

g1 = [];
g2 = [x_init; zeros(horlength*n,1)];
for t = 1:horlength
    g1(:,t) = [rho*barCx'*tildew(:,t); ...
               rho*barCu'*tildew(:,t)];
end
g1 = reshape(g1, (n+m)*horlength, 1);
g1 = [g1; zeros(n,1)];

nu  =  -LS' \ (LS \ (g2-A21*A11inv*g1));
sol = A11inv*(g1-A21'*nu);
x_T = sol(horlength*(n+m)+1:horlength*m+(horlength+1)*n);
sol = reshape(sol(1:(n+m)*horlength),n+m,horlength);


