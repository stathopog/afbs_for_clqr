function [z,v] = sim_sys_dyns_2(x_T,horlength,horsim,K,A,B)

% Forward propagation of the dynamics for [hor.length+1,hor.sim] 
z_init = x_T;
z(:,1) = z_init;
for i = horlength+1:horsim
    v(:,i-horlength)   = -K*z(:,i-horlength);
    z(:,i-horlength+1) = A*z(:,i-horlength) + B*v(:,i-horlength);
end