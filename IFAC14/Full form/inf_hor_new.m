%% Infinite horizon MPC using ADMM - main

clear all;
close all;
randn('state',4);
rand('state',0);

MAXITER = 3e4;

%% Defining the problem parameters

% Generating a random system
% small
A = [1.988 -0.998; 1 0]; 
B = [0.125; 0];
n = size(A,1); m = size(B,2);
no.o = n;
C = eye(n);
% quad model
% [n,m,A,B,C,Cx,Cu,cx,cu] = quad_mpc;
% no.o = n;
    
Pt = randn(n+m,1); Pt = Pt.^2; 
% Q = C'*C+1e-4*eye(n);   R = eye(m); % quad
Q = eye(n); R = 10*eye(m); %small
N = zeros(n,m);

% 2 states
Cx = [C; -C];
Cu = [eye(m); -eye(m)];
no.con.all = size(blkdiag(Cx,Cu),1);

% % % Objective function's linear terms
% % q = zeros(n,1);
% % r = zeros(m,1);

% Constraints and matrices
umax = 8; % small
umin = -umax;
ymax = 3;
ymin = -ymax;
cx = [ones(no.o,1)*ymax; -ones(no.o,1)*ymin]; 
cu = [ones(m,1)*umax; -ones(m,1)*umin];

no.con.st = size(cx,1);
no.con.in = size(cu,1);

bar.Cx = [Cx; zeros(no.con.in,n)];
bar.Cu = [zeros(no.con.st,m);Cu];
bar.c = [cx; cu];
% cbar = [repmat([cx; cu],N,1); cx];

%% Compute terminal set
[K,S,E] = dlqr(A,B,Q,R,N);
H = [Cx; -Cu*K]; h = [cx; cu];
Omega{1} = polytope(Cx,cx);
Omega{2} = polytope(H,h);
% add_ineq = H*(A-B*K);
%pre{1} = polytope(add_ineq,h);
i = 2;
omega.ineq_1 = H;
% % omega.ineq_1 = [omega.ineq_1; add_ineq];
% % omega.ineq_2 = [h;h];
% Omega{2} = polytope(omega.ineq_1,omega.ineq_2);
while ~(Omega{i} == Omega{i-1})
    add_ineq = H*(A-B*K)^(i-1);
    omega.ineq_1 = [omega.ineq_1; add_ineq];
    i = i+1;
    omega.ineq_2 = repmat(h,i-1,1);
    Omega{i} = polytope(omega.ineq_1,omega.ineq_2);
end
count = i;
[Hf,hf] = double(Omega{count});

% %% Sample feasible space for initial state - check feasibility!
% X_INIT = feas_x_init(n,m,Hf,hf,Q,R,A,B,Cx,Cu,cx,cu,S);
% X_INIT = reshape(X_INIT,n,size(X_INIT,1)/n);
% pause;
% figure(1);
% plot(Omega{1},struct('shade',.1,'linewidth',3));
% hold on; plot(Omega{count},struct('shade',.5,'linewidth',3));

% all.x_init = load('../Data/X_INIT_quad');
X_INIT = load('../Data/X_INIT_2states');

%% Compute AMA parameters
bar.Acal = [bar.Cx bar.Cu];
tau = max(eig(inv(blkdiag(Q,R)))); % Lipschitz constant of f*(x,u)
Mu = max(svd(bar.Acal')); % Bound on Acal operator
rho = 1/(tau*Mu^2); % ama stepsize
alpha = 1;

%% Pre-factorizations for the linear system
Pt_prime = blkdiag(Q,R);  
A111inv = inv(Pt_prime);
dyn_eq = sparse([-A -B sparse(n,n)]) + sparse([sparse(n,m+n) speye(n)]);
Sinv = inv(S);

HTs = zeros(size(X_INIT.X_INIT,2),1);
ITERs = zeros(size(X_INIT.X_INIT,2),1);
for j = 1:size(X_INIT.X_INIT,2)
    x_init = X_INIT.X_INIT(:,j);
    time.x = [];
    time.count = 1;  
    %% Run (F)AMA
    tic
    [k,x,u,sigma,w,hor,time] = AMA(MAXITER,x_init,K,A,B,omega,A111inv,Sinv,n,m,no,time,bar,rho,alpha);
    toc
    HTs(j) = hor.length;
    ITERs(j) = k;
end


%% For 2-state system trajectories evolution
% % cls=['y',...
% %      'm',...
% %      'c',...
% %      'r',...
% %      'g',...
% %      'b'];
% %  
% % %%plotting
% % % plot trajectories for various x0
% % % for x0 \in Omega{1} but NOT \in O_infty and x+ NOT \in Omega{1}
% % x0 = X_INIT(:,2);
% % x(:,1) = x0;
% % plot(x0(1,1),x0(2,1),'*k', 'linewidth', 4);
% % for j = 1:6
% %     for t = 2:hor.sim+1
% %         plot(time.x(1,t,j),time.x(2,t,j),[cls(j),'*'],'linewidth', 1.5);
% %     end
% % end
% %     for t = 2:hor.sim+1
% %         plot(X(1,t),X(2,t),'*k','linewidth', 1.5);
% %     end
% % hold off;
% % 
% % gtitle = title('Convergence to the optimal trajectory as ADMM iterates');
% % % gylabel = ylabel('$x_2$');
% % % gxlabel = xlabel('$x_1$');
% % % set(gylabel,'Interpreter','Latex');
% % % set(gxlabel,'Interpreter','Latex');
% % set(gtitle,'Interpreter','Latex');