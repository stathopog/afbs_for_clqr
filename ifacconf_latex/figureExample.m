hist(randn(1000,1),20)
set(get(gca,'child'),'FaceColor',0.35*[1 1 1],'EdgeColor','k');

xlabel(' ');
ylabel(' ');
set(gca,'FontSize',25);
set(gca,'Position',[ 0.055 0.05 0.93 0.925 ]);
fp = fillPage(gcf, 'margins', [0 0 0 0], 'papersize', [11 8.5]);
print(gcf, '-dpdf', '-r300', 'Histogram.pdf')