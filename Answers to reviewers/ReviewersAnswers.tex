\documentclass[paper=a4, fontsize=11pt]{scrartcl}


%\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[protrusion=true,expansion=true]{microtype}	
\usepackage{cmbright}
\usepackage{multirow}
\usepackage{color}

%%% Custom sectioning (sectsty package)
\usepackage{sectsty}												% Custom sectioning (see below)
%\allsectionsfont{\centering \normalfont\scshape}	% Change font of al section commands
\allsectionsfont{\scshape}	% Change font of al section commands

%%% Custom headers/footers (fancyhdr package)
\usepackage{fancyhdr}
\pagestyle{fancyplain}
\fancyhead{}														% No page header
\fancyfoot[C]{}													% Empty
\fancyfoot[R]{\thepage}									% Pagenumbering
\renewcommand{\headrulewidth}{0pt}			% Remove header underlines
\renewcommand{\footrulewidth}{0pt}				% Remove footer underlines
\setlength{\headheight}{13.6pt}

%%% Maketitle metadata
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} 	% Horizontal rule
\newtheorem{remark}{Remark}
\newtheorem{theorem}{Theorem}
\title{
		%\vspace{-1in} 	
		\usefont{OT1}{bch}{b}{n}
		\normalfont \normalsize \textsc{School of random department names} \\ [25pt]
		\horrule{0.5pt} \\[0.4cm]
		\huge This is the title of the template report \\
		\horrule{2pt} \\[0.5cm]
}
\author{
		\normalfont 								\normalsize
        Firstname Lastname\\[-3pt]		\normalsize
        \today
}
\date{}

\usepackage{verbatim}
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\pgfplotsset{plot coordinates/math parser=false}
\usepackage{graphicx}
%\usepackage{natbib}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{algorithm}
\usepackage{algorithmicx}
\usepackage[noend]{algpseudocode}
\usepackage{mathrsfs}
\usepackage{hyperref}
\usepackage{color}
\usepackage{nomencl} 
\usepackage{accents}
\usepackage{caption}
\usepackage{subcaption}
\newcommand{\eg}{{\it e.g. }}	
\newcommand{\ie}{{\it i.e. }}	
\newcommand{\reals}{{\mathbb{R} }}	
\newcommand{\prob}{{\mathbb{P} }}  
\newcommand{\E}{{\mathbb{E} }} 
\DeclareMathOperator*{\argmin}{arg\,min}	
\newcommand{\tr}{{T }}	
\newcommand{\dist}{{\mathbf{dist} }}	
\newcommand{\std}{{\mathbf{std} }}  
\DeclareMathOperator*{\dom}{dom}  
\newcommand\munderbar[1]{%
  \underaccent{\bar}{#1}}
\DeclareMathOperator*{\var}{VaR}

%\newcommand{name}[num]{definition}


\newcommand{\pfpx}[4]{\frac{\partial #1_{#2}}{\partial #3_{#4}}}
\newtheorem{mydef}{Definition}
\newtheorem{mylem}{Lemma}
\newtheorem{myas}{Assumption}
\newtheorem{myrem}{Remark}
\newtheorem{mycor}{Corollary}
\newtheorem{thm}{Theorem}
\newtheorem{prop}{Proposition}


\title{Solving the Infinite-horizon Constrained LQR Problem using Accelerated Dual Proximal Methods: Reviewed version}


\begin{document}


\author{Giorgos Stathopoulos, Milan Korda, Colin N. Jones}

\maketitle

First, we would like to thank all the reviewers for their constructive comments. The main outcome was to revisit the paper, understand in more depth several aspects that were questioned, and evaluate the performance of the method by coding it up in C++.

Since there was a common request from all three reviewers to see computational results regarding the CLQR algorithm, we decided to bundle the related questions and answer them here. For the rest of the comments, individual answers are given in the subsequent sections.

\section{Computational results}
The CLQR method was coded in C++ and the findings are presented in Section~VI, subsection~C of the revisited manuscript. The code will be soon uploaded to Bitbucket. We would like to stress that, given the time limitations, this is not a fully optimized C++ implementation, but rather an indicator of the order of the time scales that the algorithm needs to execute.

A second point we would like to make concerns the difficulty to perform a fair comparison between CLQR and MPC. In the authors' opinion, CLQR is more than a suboptimal solution to the real infinite-horizon problem, in constrast to MPC. MPC assumes the knowledge of a horizon length that is sufficient to render the system stable, either taking it long enough (how long?) or by enforcing terminal constraints, with an apparent negative impact on the volume of the region of attraction. In this sense, we consider a `fair' comparison to be the following:
\begin{enumerate}
\item First sample the maximal control invariant set of the system at hand (either by having explicit knowledge of it, like in the first example of Section~VI, or by sampling for feasible initial conditions for the second example).
\item Identify the maximum horizon that is returned by the CLQR for each of those feasible problems. 
\item Apply MPC with the identified fixed horizon length (and no terminal constraint). Roll the horizon until the resulting initial condition resides inside the maximum positively invariant set of the closed-loop system operating under the LQ constroller.
\item Compare the time needed to solve the sequence of optimal control problems (MPC) versus the convergence time of the CLQR.
\end{enumerate}
If no external disturbances occur, the proposed approach has the advantage of executing only once, since open and closed loop solutions coincide. If disturbances occur, the proposed approach needs to be applied in closed-loop as well. Both cases are considered in Section~VI, subsection~C. In the first case, our CLQR approach can outperform several modern convex optimization solvers. In the second case it is not as competitive, which is expected since the generated code is not optimized at the same level as the code of those toolboxes. However, especially when taking into account the recent theoretical advancements concerning splitting schemes, we believe that the proposed approach can outperform traditional, set-based MPC, at least when it comes to tracking and regulation problems in linear settings.

\section{Replies to the reviewers}
\subsection{Reviewer 2}
\begin{itemize}
 \item \emph{It would be extremely interesting to compare the practical
performance of the proposed approach with the Interior-Point Method
(IPM) based approach in [3]. This comparison would help in evaluating
the practical relevance of the proposed approach. The reviewer is
however aware of the amount of work required to re-implement the
algorithm in [3], and of the difficulty of a fair comparison of such
different algorithms.}: 

Although we did not implement the method per se, we can safely draw some conclusions regarding its performance. In principle, in [3], a sequence of QPs is solved until a `sufficient' horizon length has been identified. If the QPs are solved using some interior point solver (as in Section~VI), then a similar performance is expected, probably better than the MPC approach since shorter horizons are used. At the end of the day, there are two factors that would decide which approach is faster: (i) The choice of the solver. For example, Interior point methods cannot be warm-started, so any information gained from the previous solve is not used. This can give significant competitive advantage to a first order method, solving the same sequence of problems, but warm-started at every previous solve. (ii) The policy used to increase the horizon along with the data of the specific problem. If a fine granulation is used (\eg, increase the horizon by one at each solve), the approach will be costly if the necessary horizon is too long. On the other hand, an aggressive increment of the horizon might lead to unnecessarily big problems. An attractive characteristic of the proposed approach is that it does not resort to such a `trial and error' procedure, rather the sufficient horizon is naturally generated in the process of executing the algorithm.

\item \emph{Page 7, Section 5B: authors say that "the factorization steps would
have to be performed several times until the correct horizon
$T^{\infty}$ has been identified". Since the number of factorizations
affects the practical performance of the method, it would be
interesting to have some data about the frequency of such events, as
e.g. in Figure 5 in [9].}:

A histogram of the factorization count is given in Section~VI.

\item \emph{Page 7, Section 5B, second-last paragraph: "An alternative would be
to directly perform a matrix inversion (for small to medium sized
systems) and only factorize once, when the horizon seems to have
stabilized." What do you mean with this? The numerical algorithm for
matrix inversion performs a matrix factorization.}:

The claim has been corrected.

\item \emph{Page 9, Figure 2, please add numbers on the x-axis and labels on both
axes.}:

Addressed. 
\end{itemize}

\subsection{Reviewer 3}
\begin{itemize}
 \item \emph{However, the technical content of the paper should be further
explained, especially in a more didactic way, for a non-specialist
audience in the field of nonsmooth optimization.
More precisely, I think that, for a full paper, a recall concerning
basic results and concepts on proximal gradient methods and dual
formulation would be helpful in this context.}: 

It is indeed the case that the paper assumes some background in convex optimization from the intereseted readers.
Due to space limitations it is quite difficult to cover all the material needed in order to render the approach fully
comprehensible from a non-expert.
We believe, however, that the most relevant points are adequately stressed and that the notation is kept fairly
clear throughout the passage. In addition, and following the reviewer's suggestion, we separated the problem statement (Section II) from the dualization procedure (Section III). Furthermore, we added a brief introduction to proximal minimization problems in 
Section~III, along with a corresponding reference [6]. The derivation of the dual function is performed in an analytic way (see equations (7), (8), (9) and (10)).

\item \emph{In equation 2, in the expression of matrices $C_i$ , a used symbol is not
defined.}:

The symbol is now defined.
\end{itemize}

\subsection{Reviewer 5}
\begin{itemize}
 \item \emph{The main theoretical contribution of the paper is Theorem 1, although
all the items in it are either very well known facts (items (i)-(iii))
or immediate consequences of them (item (iv)), or were already included
in the previous work [9] by the authors (item (v)).}: 

Our feeling is that the comment does not do justice to this work, probably and partly due to the way we presented some results.
More specifically, points (i) and (iii) of Theorem 1 are indeed well-known facts for the \emph{FISTA} method [13], [20].
However, it was not until very recently (actually at the time that this paper was being written) that \emph{weak convergence of the iterates of FISTA was proven in [13]}.
If not for this result, it would have been impossible to prove convergence of the accelerated state and input sequences to the optimal ones, as well as boundedness (point (v)) of the hitting times. 
In order to make this part more clear, the full proof is now transferred under point (v) of Theorem 1, as it was initially only provided in the appendix.
It is generally the case that, although there exist claims about the majority of the results regarding splitting methods carrying over to infinite-dimensional spaces, in practice many challenges arise when working in these spaces.

In addition, in this work we give a solid approach to compute the Lipschitz constant that is necessary for the provable convergence of the algorithm, something that was not treated in [9]. The result is
presented in Appendix~B. Finally, a more practical alternative to using a constant stepsize is suggested, while still being able to retain part of the theoretical guarantees presented in Theorem 1 (Appendix~C).

\emph{Moreover, statistics highlighting
both the average and maximum complexity, rather than just the average,
would surely be much more informative and give a concrete feeling of
the effectiveness of the approach as compared to finite horizon
techniques such as MPC.}:

Figures~(9),~(10) and~(11) provide the relevant information.

\emph{Page 3, second column, just before reference [15]: "Guler extended
Nesterov's results to the proximal point algorithm, handling the
minimization of the sum of a smooth and a nonsmooth function". This is
not exact, as the proximal point algorithm handles the minimization of
general proper, lower semicontinuous, convex functions rather than
problems with the structure mentioned by the authors. Moreover, the
mentioned extension by Guler is not discussed in reference [15], but
probably in `New proximal point algorithms for convex minimization',
Osman Guler, Siam J. Optimization, Vol. 2, No. 4, pp. 649-664, November
1992.}:

Thank you for the reference. The issue is addressed.

\emph{Algorithm 2, step c, initializes a >= 2, contradicting what is said
earlier about Algorithm 1, where a > 2 is required. The choice of a
should be made clear, and in case a > 2 is required the authors should
point out why this is the case as opposed to the classical choice a =
2.}:

The typo is fixed; $a>2$ is required. There is a more elaborate discussion regarding this choice in Section III. 
The requirement comes from Theorem~3 of [13], where it is shown that, in order for the iterates to converge, the condition $a>2$ (and not equal!) is necessary.
This is the reason why the proposed family of methods are variants of FISTA and not exactly equivalent. The authors also discuss how any $a>2$ would suffice for the convergence to hold.

\end{itemize}

\end{document}
