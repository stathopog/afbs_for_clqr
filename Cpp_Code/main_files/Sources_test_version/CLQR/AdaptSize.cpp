//
//  AdaptSize.cpp
//  Constrained-LQR
//
//  Created by Georgios on 09/09/15.
//
//

#include <fstream>
#include <cassert>
#include "AdaptSize.hpp"

using namespace arma;
using namespace std;

// Allocate memory to temporarily host data of truncated size newHor. 
int AdaptSize(data_dims* p_data_dims, data* p_data, temp_data* p_temp_data, int& newHor)
{
    // These variables have to be re-allocated @ every iteration since the size might change.
    p_temp_data->R = p_data->Rbar(span(0, p_data_dims->m*newHor-1), span(0, p_data_dims->m*newHor-1));
    //(p_temp_data->R).print("tempR =");
    p_temp_data->B = p_data->Bbar(span(0, p_data_dims->n*newHor-1), span(0, p_data_dims->m*newHor-1));
    //(p_temp_data->B).print("tempB =");
    mat temp1_Q = p_data->Qbar(span(0, p_data_dims->n*(newHor-1)-1), span(0, p_data_dims->n*(newHor-1)-1));
    p_temp_data->Q = BlkDiag(temp1_Q, p_data->S);
    //(p_temp_data->Q).print("tempQ =");
    p_temp_data->H = p_temp_data->B.t()*p_temp_data->Q*p_temp_data->B + p_temp_data->R;
    //(p_temp_data->H).print("tempH =");
    p_temp_data->A = p_data->Abar(span(0, p_data_dims->n*newHor-1), span(0, p_data_dims->n-1));
    //(p_temp_data->A).print("tempA =");
    p_temp_data->G = p_temp_data->A.t()*p_temp_data->Q*p_temp_data->B;
    p_temp_data->F = p_temp_data->A.t()*p_temp_data->Q*p_temp_data->A+p_data->Q;
    vec temp_r = p_temp_data->F*p_data->xinit;
    p_temp_data->r = 0.5*dot(p_data->xinit,temp_r);
    //cout << "constant term r is: " << p_temp_data->r << "\n";
    vec temp1_h = p_temp_data->G.t()*p_data->xinit;
    p_temp_data->h = temp1_h.rows(0,p_data_dims->m*newHor-1);
    //(p_temp_data->h).print("temph =");
    p_temp_data->C = p_data->Cbar(span(0, (p_data_dims->px+p_data_dims->pu)*newHor-1), span(0, p_data_dims->m*newHor-1));
    p_temp_data->d = p_data->dbar.rows(0, (p_data_dims->px+p_data_dims->pu)*newHor-1);
    //(p_temp_data->C).print("tempC =");
    // Re-factor
    p_temp_data->L = chol(p_temp_data->H, "lower");
    return(0);
}

// Method for constructing block diagonal matrix
sp_mat BlkDiag(mat& m1, mat& m2)
{
    sp_mat B(m1.n_rows+m2.n_rows,m1.n_cols+m2.n_cols);
    B(span(0,m1.n_rows-1),span(0,m1.n_cols-1)) = m1;
    B(span(m1.n_rows,m1.n_rows+m2.n_rows-1),span(m1.n_cols,m1.n_cols+m2.n_cols-1)) = m2;
    return B;
}

