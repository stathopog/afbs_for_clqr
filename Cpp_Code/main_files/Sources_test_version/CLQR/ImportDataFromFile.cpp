//
//  ImportDataFromFile.cpp
//  Constrained-LQR
//
//  Created by Georgios on 04/09/15.
//
//

#include <fstream>
#include <cassert>
#include "ImportDataFromFile.hpp"

using namespace arma;
using namespace std;

void ReadInData(data_dims* p_data_dims, string filePath)
{
    ifstream read_file;
    string temp = filePath + "sizes_data";
    cout << temp << "\n";
    read_file.open("/Users/georgios/Documents/Research/CLQR_via_FBS/Cpp_Code/Matlab_files/sizes_data");
    assert(read_file.is_open());
    read_file >> p_data_dims->n >> p_data_dims->m >> p_data_dims->px >> p_data_dims->pu >> p_data_dims->pf >> p_data_dims->N >> p_data_dims->beta;
    read_file.close();
}


void ReadInMatVecs(data_dims* p_data_dims, string filePath, data* p_data)
{
    p_data->A.load(filePath+"A_data.dat",csv_ascii);
    //p_data->A.print("A =");
    
    p_data->B.load(filePath+"B_data.dat",csv_ascii);

    p_data->Q.load(filePath+"Q_data.dat",csv_ascii);
    
    p_data->Abar.load(filePath+"Abar.dat",csv_ascii);
    //Abar.print("Abar =");
    
    p_data->Bbar.load(filePath+"Bbar.dat",csv_ascii);
    
    p_data->K.load(filePath+"K_data.dat",csv_ascii);
    
    p_data->Qbar.load(filePath+"Qbar.dat",csv_ascii);
    
    p_data->Rbar.load(filePath+"Rbar.dat",csv_ascii);
    
    p_data->Gbar.load(filePath+"Gbar.dat",csv_ascii);
    
    p_data->S.load(filePath+"S_data.dat",csv_ascii);
    
    p_data->Cbar.load(filePath+"Cbar.dat",csv_ascii);
    
    p_data->H.load(filePath+"H_data.dat",csv_ascii);
    
    p_data->xinit.load(filePath+"xinit_data.dat",csv_ascii);
    //p_data->xinit.print("xinit =");

    p_data->dbar.load(filePath+"dbar.dat",csv_ascii);

    p_data->hf.load(filePath+"hf_data.dat",csv_ascii);

    p_data->W.load(filePath+"W.dat",csv_ascii);
}

