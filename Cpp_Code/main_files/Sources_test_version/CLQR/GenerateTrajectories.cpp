//
//  GenerateTrajectories.cpp
//  Constrained-LQR
//
//  Created by Georgios on 09/09/15.
//
//

#include <fstream>
#include <cassert>
#include "GenerateTrajectories.hpp"

using namespace arma;
using namespace std;

// Perform the backtracking and return a local Lipschitz
void GeneratePrimalTrajectories(data_dims* p_data_dims, data* p_data, temp_data* p_temp_data, vars* p_vars, int& oldHor, int& newHor, int lastHor, int& T)
{
    // CHECK ASSERTIONS
    if (T == 0)
    {
        //p_vars->lambda.print("lambda =");
        if (any(p_vars->lambda)) // If warm-starting is on
        {
            //cout << "lasthorizon is: " << lastHor << "\n";
            newHor = lastHor;
            AdaptSize(p_data_dims, p_data, p_temp_data, newHor);
            vec w = p_data->W.rows(p_data_dims->px+p_data_dims->pu,(p_data_dims->px+p_data_dims->pu)*(newHor+1)-1);
            //w.print("w =");
            LinSysSolve(p_data_dims, p_data, p_temp_data, p_vars, w, newHor);
            mat tempu = p_vars->u;
            //tempu.print("tempu =");
            // Find hitting time
            SimDynSys(p_data_dims, tempu, p_vars, p_data, oldHor, newHor);
            AdaptSize(p_data_dims, p_data, p_temp_data, newHor);
        }
        else // cold start (zero multipliers)
        {
            // Find the first hitting time by forward simulation
            mat tempu = p_vars->u;
            SimDynSys(p_data_dims, tempu, p_vars, p_data, oldHor, newHor);
            AdaptSize(p_data_dims, p_data, p_temp_data, newHor);
            //        p_vars->x.cols(0,newHor).print("x =");
        //p_vars->u.cols(0,newHor-1).print("u =");
        }
    }
    else
    {
        // Solve linear system
        vec w = p_data->W.rows(0, (p_data_dims->px+p_data_dims->pu)*newHor-1);
        //w.print("w =");
        LinSysSolve(p_data_dims, p_data, p_temp_data, p_vars, w, newHor);
        mat tempu = p_vars->u;
        // Find hitting time
        SimDynSys(p_data_dims, tempu, p_vars, p_data, oldHor, newHor);
        //p_vars->x.cols(0,newHor).print("x =");
        //p_vars->u.cols(0,newHor-1).print("u =");
    }
}

/*****************************************************************************
 * Forward simulation of the dynamical system.                               *
 * The state trajectory evolves until it enters a positively invariant set   *
 * H*x(t)<=hf. The function outputs the hitting time 't' and updates         *
 * the global variables x and u.                                             *
 *****************************************************************************/
int SimDynSys(data_dims* p_data_dims, mat& tempu, vars* p_vars, data* p_data, int& oldHor, int& newHor)
{
    oldHor = newHor;
    int t = 0; // time index
    p_vars->x.col(0) = p_data->xinit; // initialize state x(0) at xinit
    vec tempcheck = p_data->H*p_data->xinit-p_data->hf; // checking set inclusion
    while (any(tempcheck > 0)) // while at least one positive element exists
    {
        if (t<oldHor)
        {
            p_vars->u.col(t) = tempu.col(t);
            p_vars->x.col(t+1) = p_data->A*p_vars->x.col(t) + p_data->B*tempu.col(t);
            t += 1;
            tempcheck = p_data->H*p_vars->x.col(t)-p_data->hf;
        }
        else
        {
            p_vars->u.col(t) = -p_data->K*p_vars->x.col(t);
            p_vars->x.col(t+1) = p_data->A*p_vars->x.col(t) + p_data->B*p_vars->u.col(t);
            t += 1;
            tempcheck = p_data->H*p_vars->x.col(t)-p_data->hf;
        }
    }
    if (t<=1) // H*xinit < hf, t=0
    {
        // cout << "newhor is " << newHor;
        p_vars->u.col(t) = -p_data->K*p_vars->x.col(t);
        p_vars->x.col(t+1) = p_data->A*p_vars->x.col(t) + p_data->B*p_vars->u.col(t);
        newHor = 0;
        cout << "xinit inside invariant set; use LQ controller \n";
        exit(1);
    }
    else // t>0
    {
        int Tstop = t;
        if (Tstop <= oldHor)
        {
            newHor = oldHor;
        }
        else
        {
            oldHor = newHor;
            newHor = Tstop;
        }
    }
    //cout << "newhor is " << newHor;
    return 0; // return new hitting time
}


/*****************************************************************************
 *                  Solve Linear System , Step 1                             *
 *****************************************************************************/
void LinSysSolve(data_dims* p_data_dims, data* p_data, temp_data* p_temp_data, vars* p_vars, vec& w, int& newHor)
{   
    vec tempHatLambda = p_vars->hatlambda.rows(0, (p_data_dims->px+p_data_dims->pu)*newHor-1); // temporary dual variable
    //tempHatLambda.print("tempHatLambda =");
   // cout << "size of hat lambda: " << size(tempHatLambda) << "\n";
   // cout << "size of w: " << size(diagmat(w)) << "\n";
    // Solve for inputs u and update global variable
    mat tempu = solve(trimatu(p_temp_data->L.t()), solve(trimatl(p_temp_data->L), p_temp_data->C.t()*diagmat(w)*tempHatLambda-p_temp_data->h) );
    tempu.reshape(p_data_dims->m, newHor); // reshape to matrix form
    //tempu.print("tempu =");
    p_vars->u(span(0, p_data_dims->m-1), span(0, newHor-1)) = tempu;
}

/*****************************************************************************
 *                  Dual update , Step 3 in the pdf                          *
 *****************************************************************************/

void GenerateDualTrajectories(data_dims* p_data_dims, vars* p_vars, vec& grad, int& newHor, double& rho)
{      
    p_vars->oldlambda.rows(0,(p_data_dims->px+p_data_dims->pu)*newHor-1) = p_vars->lambda.rows(0,(p_data_dims->px+p_data_dims->pu)*newHor-1);
    vec tempHatLambda = p_vars->hatlambda.rows(0, (p_data_dims->px+p_data_dims->pu)*newHor-1);
    vec tempgrad = tempHatLambda - grad*rho;
    p_vars->lambda.rows(0, (p_data_dims->px+p_data_dims->pu)*newHor-1) = arma::min(tempgrad, zeros((p_data_dims->px+p_data_dims->pu)*newHor));
}

