//
//  GenerateTrajectories.h
//  Constrained-LQR
//
//  Created by Georgios on 09/09/15.
//
//

#ifndef __Constrained_LQR__GenerateTrajectories__
#define __Constrained_LQR__GenerateTrajectories__

#include <stdio.h>
#include </opt/local/include/armadillo>
#include "AdaptSize.hpp"
#include "InitVars.hpp"

using namespace arma;
using namespace std;

//! Header for the GenerateTrajectories.cpp
/*!
 Contains four functions, GeneratePrimalTrajectories, SimDynSys, LinSysSolve and GenerateDualTrajectories
 */


//! function prototypes:

void GeneratePrimalTrajectories(data_dims* p_data_dims, data* p_data, temp_data* p_temp_data, vars* p_vars, int& oldHor, int& newHor, int lastHor, int& T); // Generate the trajectories, either by linear system solve or linear system solve + simulation

int SimDynSys(data_dims* p_data_dims, mat& tempu, vars* p_vars, data* p_data, int& oldHor, int& newHor); // Forward simulate dynamical system

void LinSysSolve(data_dims* p_data_dims, data* p_data, temp_data* p_temp_data, vars* p_vars, vec& w, int& newHor); // Solve linear system by FB substitution

void GenerateDualTrajectories(data_dims* p_data_dims, vars* p_vars, vec& grad, int& newHor, double& rho); // Dual update

#endif /* defined(__Constrained_LQR__GenerateTrajectories__) */
