//
//  Backtrack.h
//  Constrained-LQR
//
//  Created by Georgios on 09/09/15.
//
//

#ifndef __Constrained_LQR__Backtrack__
#define __Constrained_LQR__Backtrack__

#include <stdio.h>
#include </opt/local/include/armadillo>
#include "InitVars.hpp"
#include "AdaptSize.hpp"

using namespace arma;
using namespace std;

//! Header for the Backtrack.cpp
/*!
 Contains three functions, the main, a FunEval and a GradEval that evaluate function value and gradient
 */


//! function prototypes:

void Backtrack(data_dims* p_data_dims, data* p_data, temp_data* p_temp_data, vec& tempu, vars* p_vars, vec& w, int& newHor, double& Lip); // Performs backtracking and returns local Lipschitz constant

double FunEval(temp_data* p_temp_data, vec& w, vec& templambda); // Dual function evaluation

vec GradEval(temp_data* p_temp_data, vec& w, vec& tempu); // Dual function's gradient evaluation 

#endif /* defined(__Constrained_LQR__Backtrack__) */
