//
//  SaveDataToFile.cpp
//  Constrained-LQR
//
//  Created by Georgios on 04/09/15.
//
//

#include <fstream>
#include <cassert>
#include "SaveDataToFile.hpp"

using namespace arma;
using namespace std;


void WriteData(vars* p_vars, string filePath, uvec& HORZS, vec& TIMES, uvec& FACTS, uvec& ITERS)
{
    TIMES.save(filePath+"TIMES.dat",csv_ascii);
    
    HORZS.save(filePath+"HORZS.dat",csv_ascii);

    FACTS.save(filePath+"FACTS.dat",csv_ascii);
    
    ITERS.save(filePath+"ITERS.dat",csv_ascii);
    
    p_vars->x.save(filePath+"x_opt.dat",csv_ascii);
    
    p_vars->u.save(filePath+"u_opt.dat",csv_ascii);
}

