//
//  SolveCLQR.cpp
//  Constrained-LQR
//
//  Created by Georgios on 13/09/15.
//
//

#include <fstream>
#include <cassert>
#include "SolveCLQR.hpp"

#define MAXITER 1e4 // maximum number of iterations CLQR will perform
#define EPS 1e-2 // tolerance (for testing convergence)
#define a 5 // relaxation parameter for acceleration (alpha = (k-1)/(k+a))

using namespace arma;
using namespace std;

int SolveCLQR(data_dims* p_data_dims, data* p_data, vars* p_vars, uvec& HORZS, uvec& FACTS, vec& TIMES, uvec& ITERS, int& noProb)
{
    // Algorithmic initializations
    double rho = 1/p_data_dims->beta;
    double Lip = 1e-2;
    double alpha = 1; // over-relaxation parameter (see Chambolle FISTA)
    int oldHor = 0; int newHor = 0; // hitting time to be updated

    // Execution time begin
    high_resolution_clock::time_point t1 = high_resolution_clock::now();

    // Structure for varying-size matrices and vectors
    temp_data* p_temp_data = new temp_data;

    int facts = 0; // count factorization times
    for (int t=0; t<MAXITER; t++)
    {
        //printf("Iteration %d\t\n", t);

        /*********************************************************************
         *****            Step 1: Generate the trajectories            *******
         *********************************************************************/ 
        GeneratePrimalTrajectories(p_data_dims, p_data, p_temp_data, p_vars, oldHor, newHor, HORZS(noProb), t);

        int dT = newHor-oldHor;

        if ((dT > 0) && (t > 0)) // If horizon increased re-factorize
        {
            //cout << "newhor is: " << newHor << "\n";
            facts += 1;
            AdaptSize(p_data_dims, p_data, p_temp_data, newHor);
        }

        /**************************************************************
         *****           Step 2: Backtrack for stepsize         *******
         **************************************************************/
        vec tempu2 = vectorise(p_vars->u(span(0, p_data_dims->m-1), span(0, newHor-1)));
        vec w = p_data->W.rows(0, (p_data_dims->px+p_data_dims->pu)*newHor-1);
        Backtrack(p_data_dims, p_data, p_temp_data, tempu2, p_vars, w, newHor, Lip);
        rho = 1/Lip;
        //cout << "rho is: " << rho << "\n";
        vec grad = GradEval(p_temp_data, w, tempu2);
        //grad.print("grad=\n");


        /**************************************************************
         *****            Step 3: Dual update                   *******
         **************************************************************/
        GenerateDualTrajectories(p_data_dims, p_vars, grad, newHor, rho);
//p_vars->lambda.rows(0, (p_data_dims->px+p_data_dims->pu)*newHor-1).print("lambda =");
//p_vars->oldlambda.rows(0, (p_data_dims->px+p_data_dims->pu)*newHor-1).print("oldlambda =");

         /**************************************************************
         *****            Step 4: Acceleration                  *******
         **************************************************************/
        alpha = ((double)(t+1) / (double)(t+a+2));
        //cout << "alpha is\n" << alpha;
        vec tempLambda = p_vars->lambda.rows(0, (p_data_dims->px+p_data_dims->pu)*newHor-1);
        vec tempOldLambda = p_vars->oldlambda.rows(0, (p_data_dims->px+p_data_dims->pu)*newHor-1);
        p_vars->hatlambda.rows(0, (p_data_dims->px+p_data_dims->pu)*newHor-1) = tempLambda + alpha*(tempLambda-tempOldLambda);
        //p_vars->hatlambda.rows(0, (p_data_dims->px+p_data_dims->pu)*newHor-1).print("hatlambda =");

        /**************************************************************
         *****            Step 5: Checking convergence          *******
         **************************************************************/
        if ( (norm(p_vars->lambda - p_vars->oldlambda,2) <= EPS) || (t == MAXITER-1) ) 
        {
            // Projection on the set of active constraints
            ProjectActiveSet(p_data_dims, p_data, p_temp_data, p_vars, newHor);
            
            // Recover optimal horizon length
            mat optLambda = p_vars->lambda.rows(0, (p_data_dims->px+p_data_dims->pu)*newHor-1);
            optLambda.reshape(p_data_dims->px+p_data_dims->pu,newHor);
            rowvec index = arma::min(optLambda,0);
            if (index(newHor-1)<0)
            {
                HORZS(noProb) = newHor;
            }
            else 
            {
                HORZS(noProb) = arma::max(find(index==0))-1;
            }
            
            // Execution time end
            high_resolution_clock::time_point t2 = high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
            TIMES(noProb) = duration;
            cout <<  "The CLQR algorithm needed " << TIMES(noProb) << " micros to converge" "\n";

            // Printing
            std::cout <<  "Vector u optimal is: " << "\n" << p_vars->u(span(0, p_data_dims->m-1), span(0, newHor-1)) << "\n";
            std::cout <<  "Vector x optimal is: " << "\n" << p_vars->x(span(0, p_data_dims->n-1), span(0, newHor)) << "\n";
            FACTS(noProb) = facts+1;
            ITERS(noProb) = t;
            std::cout <<  "Identified horizon length T is: " << HORZS(noProb) << "\n";
            std::cout <<  "Total number of factorizations: " << FACTS(noProb) << "\n";
            std::cout <<  "Total number of iterations: " << ITERS(noProb) << "\n";
            return 0; 
        }
    } 
    return 0;     
}