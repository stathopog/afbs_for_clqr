//
//  main.cpp
//  Constrained Linear Quadratic Regulator
//
//  Created by Georgios on 11/10/14.
//  Copyright (c) 2014 Georgios. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <cassert>
#include <string>
#include <chrono>
#include "ImportDataFromFile.hpp"
#include "AdaptSize.hpp"
#include "Backtrack.hpp"
#include "SolveCLQR.hpp"
#include "SaveDataToFile.hpp"
#include </opt/local/include/armadillo>

# define PERTURB 0.0 // percentage of initial state perturbation

using namespace std;
using namespace arma;

int main(int argc, const char * argv[])
{
    // Read in data (scalars)
    string filePath = "/Users/georgios/Documents/Research/CLQR_via_FBS/Cpp_Code/Matlab_files/";
    data_dims* p_data_dims = new data_dims;
    ReadInData(p_data_dims, filePath);
    
    // Read in data (matrices and vectors)
    data* p_data = new data;
    ReadInMatVecs(p_data_dims, filePath, p_data);

    // Initialize vars @ 0
    vars* p_vars = new vars;
    InitVarsCold(p_data_dims, p_vars);

    // Keep track of horizon lengths, factorizations and durations of execution
    int M = 1;
    uvec HORZS(M,fill::zeros); // horizon lengths
    uvec FACTS(M,fill::zeros); // factorizations
    vec TIMES(M,fill::zeros); // durations in ms
    uvec ITERS(M,fill::zeros); // iterations


    // Main execution
    for (int k=0; k<M; k++) 
    {
        cout << "\n***Solving problem " << k << "..." << "\n\n";

        // main
        SolveCLQR(p_data_dims, p_data, p_vars, HORZS, FACTS, TIMES, ITERS, k);

        // perturb initial state
        p_data->xinit = p_vars->x.col(1) + PERTURB*(1-2*randu())*p_vars->x.col(1);
        p_data->xinit.print("xinit is");

        // warm-start
        //InitVarsWarm(p_data_dims, p_vars);
        //p_vars->x.cols(0,HORZS(k)).print("x =");
        //p_vars->u.cols(0,HORZS(k)-1).print("u =");
    }   

    // reports
    cout << "\n\nAverage time: " << mean(TIMES) << " micro-seconds over " << M << " solves.\n";
    cout << "Max time: " << max(TIMES) << " micro-seconds over " << M << " solves.\n";
    cout << "Average No. of factorizations: " << mean(FACTS) << "\n";
    cout << "Max No. of factorizations: " << max(FACTS) << "\n";
    cout << "Average No. of iterations: " << mean(ITERS) << "\n";
    cout << "Max No. of iterations: " << max(ITERS) << "\n";

    // save to file
    WriteData(p_vars, filePath, HORZS, TIMES, FACTS, ITERS);

    return(0);
}