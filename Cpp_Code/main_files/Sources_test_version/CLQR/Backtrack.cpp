//
//  Backtrack.cpp
//  Constrained-LQR
//
//  Created by Georgios on 09/09/15.
//
//

#include <fstream>
#include <cassert>
#include "Backtrack.hpp"

using namespace arma;
using namespace std;

// Perform the backtracking and return a local Lipschitz
void Backtrack(data_dims* p_data_dims, data* p_data, temp_data* p_temp_data, vec& tempu, vars* p_vars, vec& w, int& newHor, double& Lip)
{
    // CHECK ASSERTIONS
    vec tempHatLambda = p_vars->hatlambda.rows(0, (p_data_dims->px+p_data_dims->pu)*newHor-1); // temporary dual variable
    double eta = 1.2;
    vec g = GradEval(p_temp_data, w, tempu); // @ hat.lambda
    double h = FunEval(p_temp_data, w, tempHatLambda); // @ hat.lambda
    vec tempzero(tempHatLambda.n_rows, fill::zeros); 
    vec p_L = arma::min(tempHatLambda-(1/Lip)*g, tempzero);
    double F = FunEval(p_temp_data, w, p_L); // @ p_L
    double Q_L = h + dot(p_L-tempHatLambda, diagmat(w) * g) + 0.5*Lip * dot(p_L-tempHatLambda,diagmat(w)*(p_L-tempHatLambda));
    int kkk = 0;
    double diff = F - Q_L - 1e-8;
    while ( (diff>=0) && (Lip <= p_data_dims->beta) && (kkk <= 100) )
    {
        Lip = eta*Lip;
        vec tempzero(tempHatLambda.n_rows, fill::zeros); 
        vec p_L = arma::min(tempHatLambda-(1/Lip)*g, tempzero);
        double F = FunEval(p_temp_data, w, p_L); // @ p_L
        //cout << "F is: " << F << "\n";
        double Q_L = h + dot(p_L-tempHatLambda, diagmat(w) * g) + 0.5*Lip * dot(p_L-tempHatLambda,diagmat(w)*(p_L-tempHatLambda));
        //cout << "Q_L is: " << Q_L << "\n";
        if ( Lip >= p_data_dims->beta )
        {
            Lip = p_data_dims->beta;
        }
        kkk +=1;
        diff = F - Q_L;
    }
}

// Function evaluation
double FunEval(temp_data* p_temp_data, vec& w, vec& templambda)
{
    vec temp1 = p_temp_data->C.t()*diagmat(w)*templambda;
    vec temp2 = solve(trimatu(p_temp_data->L.t()),solve(trimatl(p_temp_data->L), p_temp_data->h));
    double h = 0.5*dot(temp1,solve(trimatu(p_temp_data->L.t()),solve(trimatl(p_temp_data->L), temp1)))-dot(temp1,temp2)-dot(templambda,diagmat(w)*p_temp_data->d)+0.5*dot(p_temp_data->h,temp2)-p_temp_data->r; 
    return h;
}

// Gradient evaluation
vec GradEval(temp_data* p_temp_data, vec& w, vec& tempu)
{
   vec g = diagmat(w)*(p_temp_data->C*tempu - p_temp_data->d);
   return g;
}

