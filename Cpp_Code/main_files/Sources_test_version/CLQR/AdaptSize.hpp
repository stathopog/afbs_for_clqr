//
//  AdaptSize.h
//  Constrained-LQR
//
//  Created by Georgios on 09/09/15.
//
//

#ifndef __Constrained_LQR__AdaptSize__
#define __Constrained_LQR__AdaptSize__

#include <stdio.h>
#include </opt/local/include/armadillo>
#include "ImportDataFromFile.hpp"

using namespace arma;
using namespace std;

//! Header for the AugmentSize.cpp
/*!
 Contains two structures, one for adapting the size once the horizon is updated and a supplementary routine for building block-diagonal arma:: matrices
 */


//! struct containing standard problem data
typedef struct temp_problem_data {
    mat R, B, H, A, G, F, C, L;
    sp_mat Q;
    vec h, d;
    double r;
}temp_data;

//! function prototypes:

int AdaptSize(data_dims* p_data_dims, data* p_data, temp_data* p_temp_data, int& newHor); // reads matrices and vectors provided in .txt files

sp_mat BlkDiag(mat& m1, mat& m2); // creates block diagonal sparse matrix from two other matrices

#endif /* defined(__Constrained_LQR__AdaptSize__) */
