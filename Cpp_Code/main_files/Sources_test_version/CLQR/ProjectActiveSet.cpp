//
//  ProjectActiveSet.cpp
//  Constrained-LQR
//
//  Created by Georgios on 04/10/15.
//
//

#include <fstream>
#include <cassert>
#include "ProjectActiveSet.hpp"

using namespace arma;
using namespace std;

// Perform projection on the active set
void ProjectActiveSet(data_dims* p_data_dims, data* p_data, temp_data* p_temp_data, vars* p_vars, int& newHor)
{
    // Find active set
    uvec activeVec = find(p_vars->lambda);
    uvec cols = linspace<uvec>(0,p_temp_data->C.n_cols-1,p_temp_data->C.n_cols);
    mat activeC  = p_temp_data->C.submat(activeVec, cols);

    // Form KKT matrix
    mat tempKKT1 = join_horiz(p_temp_data->H, activeC.t());
    mat tempKKT2 = join_horiz(activeC, zeros(activeVec.n_rows,activeVec.n_rows));
    mat KKT = join_vert(tempKKT1,tempKKT2);
    vec rhsKKT = join_vert(-p_temp_data->h,p_temp_data->d(activeVec));

    // Solve linear system
    vec solKKT = solve(KKT,rhsKKT);
    mat tempu = solKKT(span(0, p_data_dims->m*(newHor-1)));  
    tempu.reshape(p_data_dims->m, newHor); // reshape to matrix form
    p_vars->u(span(0, p_data_dims->m-1), span(0, newHor-1)) = tempu;
    p_vars->x(span::all,span(1, newHor)) = p_data->A*p_vars->x(span::all,span(0, newHor-1)) + p_data->B*p_vars->u(span::all,span(0,newHor-1));
}



