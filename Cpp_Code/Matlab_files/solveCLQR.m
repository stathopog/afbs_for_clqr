function [] = solveCLQR(dat,bar,counter,perturbation)
% Solve once with the CLQR
    global X_INIT;
    global clqrStats;
% %     bar.cxx = bar.cx-bar.Cx*bar.A*X_INIT(:,counter);
% %     const.c = [];
% %     for jjj = 1:dat.N
% %         const.c = [const.c; bar.cu((jjj-1)*dat.no.con.u+1:jjj*dat.no.con.u,:); bar.cxx((jjj-1)*dat.no.con.x+1:jjj*dat.no.con.x,:)];
% %     end
    delete xinit_data;
    csvwrite('xinit_data.dat',X_INIT(:,counter));
    delete sizes_data;
    fi1 = fopen('sizes_data','w');
    fprintf(fi1,'%u ',dat.nx); fprintf(fi1,'%u ',dat.nu);
    fprintf(fi1,'%u ',dat.px); fprintf(fi1,'%u ',dat.pu); fprintf(fi1,'%u ',dat.pf); fprintf(fi1,'%u ',dat.N);
    fprintf(fi1,'%6.6f ',dat.beta); fprintf(fi1,'%6.6f ',dat.w);
    fclose(fi1);
%     delete dbar;
%     csvwrite('dbar.dat',const.c);

    delete H_data;
    csvwrite('H_data.dat',dat.Hf)
    delete hf_data;
    csvwrite('hf_data.dat',dat.hf)


    delete perturb;
    csvwrite('perturb.dat',perturbation);
    % access Cpp folder and run clqr code
    cd ../main_files_R/build_test_version_R;
    system('make');
    cd Sources_test_version/CLQR;
    system('./clqr');
%    cd /Users/georgios/Documents/Research/GitCode/Cpp_Code/Matlab_files;
    clqrStats.HORZS{counter} = csvread('CLQR_HORZS.dat');
    tempTime = csvread('CLQR_TIMES.dat');
    clqrStats.TIMES{counter} = tempTime./1e6;
    clqrStats.FACTS{counter} = csvread('FACTS.dat');
    clqrStats.ITERS{counter} = csvread('CLQR_ITERS.dat');
    clqrStats.Xopt{counter}  = csvread('CLQR_x_opt.dat');
    clqrStats.Uopt{counter}  = csvread('CLQR_u_opt.dat');
    clqrStats.OptVal{counter}  = csvread('CLQR_optval.dat');
    cd /Users/georgios/Documents/Research/CLQR_via_FBS/Cpp_Code/Matlab_files;
end