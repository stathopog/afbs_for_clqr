function plotNumerics(cplexStats, mosekStats, qpgenStats)

global clqrStats;

for jjj = 1:size(clqrStats.ITERS,2)
    clqrAverIters(jjj) = mean(clqrStats.ITERS{jjj});
    clqrMaxTimes(jjj)  = max(clqrStats.TIMES{jjj});
end
    
%% Histogram of factorization & iteration count for some initial conditions
figure(1);
hist(clqrAverIters);
hcg2 = title('Iteration count for CLQR');
xlabcg2 = xlabel('# of iterations'); ylabcg2 = ylabel('# of problems');
set(hcg2,'Interpreter','Tex');  
set(xlabcg2,'Interpreter','Tex');
set(ylabcg2,'Interpreter','Tex');
set(get(gca,'child'),'FaceColor',0.35*[1 1 1],'EdgeColor','k');
set(hcg2, 'FontSize', 16);
set(xlabcg2, 'FontSize', 16);
set(ylabcg2, 'FontSize', 16);
muITERs = mean(clqrAverIters); %The mean
%Overlay the mean
hold on
plot([muITERs,muITERs],ylim,'k--','LineWidth',2)
hold off

figure(2);
hist(cell2mat(clqrStats.FACTS));
hcg2 = title('Factorization count for CLQR');
xlabcg2 = xlabel('# of factorizations'); ylabcg2 = ylabel('# of problems');
set(hcg2,'Interpreter','Tex');  
set(xlabcg2,'Interpreter','Tex');
set(ylabcg2,'Interpreter','Tex');
set(get(gca,'child'),'FaceColor',0.35*[1 1 1],'EdgeColor','k');
set(hcg2, 'FontSize', 16);
set(xlabcg2, 'FontSize', 16);
set(ylabcg2, 'FontSize', 16);
muFACTs = mean(cell2mat(clqrStats.FACTS)); %The mean
%Overlay the mean
hold on
plot([muFACTs,muFACTs],ylim,'k--','LineWidth',2)
hold off

%% Box&Whiskers plotting
figure(3);
h = cat(2,clqrMaxTimes',cplexStats.averageTime',mosekStats.averageTime',qpgenStats.averageTime');
% h = {clqrStats.TIMES';sedumiStats.tElapsed';cplexStats.tElapsed';mosekStats.tElapsed';qpgenStats.tElapsed'};
boxplot(h,'labels',{'CLQR','CPLEX', 'MOSEK', 'QPGEN'});
% hleg = legend('Basic', 'Preconditioned', 'Prec.+Adapt/Restart');
hcg = title('Box Plots: Timings for the different solvers');
ylim([0 0.08])
set(hcg,'Interpreter','Tex'); 
set(hcg, 'FontSize', 16);
ylabcg = ylabel('Time in ms');
set(ylabcg,'Interpreter','Tex'); 
set(ylabcg, 'FontSize', 16);
% set(hleg,'FontSize', 14);
% set(hleg, 'FontName', 'Helvetica')
