function [solver] = solveIPM(dat,counter,simLength,opt_pair,solver,clqr,pert)
% Solve with Yalmip using several IPM solvers
        global X_INIT;
        X = zeros(dat.nx,simLength);  U = zeros(dat.nu,simLength);
        tStart = tic;
        kkk = 1;
        X(:,1) = X_INIT(:,counter);
        yal_xinit = X_INIT(:,counter);
        while (any(dat.Hf*yal_xinit-dat.hf>0))
            kkk = kkk + 1;
            disp(kkk)
            [yal_z,errorcode] = opt_pair{yal_xinit};
            if errorcode
               yalmiperror(errorcode)
               break
            end
            % Store trajectories
            PROBS(counter) = kkk; % number of OCP solved
            X(:,kkk) = double(yal_z(1:dat.nx,2));
            U(:,kkk-1) = double(yal_z(dat.nx+1:end,1));
            yal_xinit = double(yal_z(1:dat.nx,2))+pert(kkk)*double(yal_z(1:dat.nx,2));
        end
        solver.tElapsed = toc(tStart);
        solver.averageTime = toc(tStart)/kkk;
        solver.Xopt  = X;   solver.Uopt  = U;
        solver.errorX = norm(X-clqr.Xopt{counter}) / norm(X);
        solver.errorU = norm(U-clqr.Uopt{counter}) / norm(U);
end