%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compares AFBS_CLQR approach with generic solvers%
% CPLEX, MOSEK, QPGEN. A Riccati iteration is used%
% to to solve the linear systems at each iteration%
% of the algorithm. This approach is not the most %
% numerically efficient, but leads to simple and  %
% clean implementations.                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;
addpath('/Users/georgios/Documents/Research/CLQR_via_FBS/Cpp_Code/Matlab_files')
rand('state',0);
disturb = 'yes';

%% Read-in data
data;
load('X_INIT_toy.mat');
global X_INIT;
global clqrStats;
global mpcStats;
if strcmp(disturb,'yes')
    pert_vec = [zeros(length(X_INIT),1) 0.04*(1-2*rand(length(X_INIT),dat.N-1))]'; % perturb initial state for each initial condition
elseif strcmp(disturb,'no')
    pert_vec = [zeros(length(X_INIT),1) 0.00*(1-2*rand(length(X_INIT),dat.N-1))]'; % perturb initial state for each initial condition
else 
    error('Type yes or no to assign a 2% perturbation in the initial state');
end

%% Solve for 50 initial conditions
for i = 1:200
    dat.x_init = X_INIT(:,i);
% % %         lambda0 = zeros(dat.no.con.all,dat.N);
% % %         init = 'cold';
% % %         horlen = 1;
% % %         kkk = 1;
% % %         while (any(dat.Hf*dat.x_init-dat.hf>0))
% % %             time.x = {};
% % %             time.count = 1; 
% % %             [inf.k,inf.x,inf.u,inf.lambda,inf.hor,~,inf.rPrimal,~,~,~,~] =...
% % %             FastDualGradient_Riccati(dat.N,dat,dat.x_init,time,1e4,[],[],init,lambda0,horlen-1);
% % %             if (inf.hor.length<=1), break; end
% % % 
% % %             idx = min(inf.lambda,[],1);
% % %             if idx(end)<0
% % %                 horlen = inf.hor.length;
% % %             else
% % %                 horlen = max(find(idx~=0));
% % %             end
% % %             CLQR.OPT_HORZ{i}(kkk) = horlen; % optimal horizon length (dual variable)
% % %             CLQR.HORZS{i}(kkk) = inf.hor.length; % horizon length as output of CLQR
% % %             CLQR.ITERS{i}(kkk) = inf.k;
% % %             CLQR.X{i}(:,kkk) = inf.x(:,1);
% % %             CLQR.U{i}(:,kkk) = inf.u(:,1);
% % %             CLQR.res{i}(kkk) = inf.rPrimal;
% % %             lambda0 = dat.w*[inf.lambda(:,2:end) zeros(dat.no.con.all,dat.N+1-size(inf.lambda,2))];
% % % 
% % %             dat.x_init = inf.x(:,2) + pert_vec(kkk,i)*inf.x(:,2);
% % %             kkk = kkk + 1;
% % %             init = 'warm';
% % %         end

    %% Solve in receding horizon fashion with CLQR
    solveCLQR(dat,bar,i,pert_vec(:,i));
    simLength = size(clqrStats.Xopt{i},2);
    T = max(clqrStats.HORZS{i}); % keep maximum horizon encountered
    
    %% MPC FISTA
    solveMPC(dat,bar,2*T,i,pert_vec(:,i));

% % %     %% IPM - Parsing
% % %     [con,yal_obj,yal_xinit,yal_x,yal_u] = parseIPM(dat,T);

% % %     %% IPM - Solving
% % %     % Solving  - Case I: CPLEX
% % %     ops1 = sdpsettings('verbose',0,'solver','+cplex');
% % %     opt_pair1 = optimizer(con,yal_obj,ops1,yal_xinit,[yal_x; yal_u]);
% % %     [solver] = solveIPM(dat,i,simLength,opt_pair1,'cplex',clqrStats,pert_vec(:,i));
% % %     cplexStats.tElapsed(i) = solver.tElapsed;
% % %     cplexStats.averageTime(i) = solver.averageTime;
% % %     cplexStats.Xopt{i} = solver.Xopt;
% % %     cplexStats.Uopt{i} = solver.Uopt;
% % %     cplexStats.errorX(i) = solver.errorX;
% % %     cplexStats.errorU(i) = solver.errorU;
% % % 
% % %     % Solving  - Case III: MOSEK
% % %     ops3 = sdpsettings('verbose',0,'solver','+gurobi');
% % %     opt_pair3 = optimizer(con,yal_obj,ops3,yal_xinit,[yal_x; yal_u]);
% % %     [solver] = solveIPM(dat,i,simLength,opt_pair3,'mosek',clqrStats,pert_vec(:,i));
% % %     mosekStats.tElapsed(i) = solver.tElapsed;
% % %     mosekStats.averageTime(i) = solver.averageTime;
% % %     mosekStats.Xopt{i} = solver.Xopt;
% % %     mosekStats.Uopt{i} = solver.Uopt;
% % %     mosekStats.errorX(i) = solver.errorX;
% % %     mosekStats.errorU(i) = solver.errorU;

% % %     %% SA - Parsing
% % %     [QP_reform,~] = parseSA(dat,T);
% % %     % Solving - QPgen
% % %    [solver] = solveSA(dat,i,simLength,QP_reform.QP.MPC,'qpgen',clqrStats,T,pert_vec(:,i));
% % %     qpgenStats.tElapsed(i) = solver.tElapsed;
% % %     qpgenStats.averageTime(i) = solver.averageTime;
% % %     qpgenStats.Xopt{i} = solver.Xopt;
% % %     qpgenStats.Uopt{i} = solver.Uopt;
% % %     qpgenStats.errorX(i) = solver.errorX;
% % %     qpgenStats.errorU(i) = solver.errorU;

% % %     %% Print some indicative results
% % %         [CLQR.HORZS{i}; clqrStats.HORZS{i}(1:length(CLQR.HORZS{i}))']
% % % %         pause;
% % %         [CLQR.ITERS{i}; clqrStats.ITERS{i}(1:length(CLQR.ITERS{i}))']
% % % %         pause;
% % %         [CLQR.X{i}; clqrStats.Xopt{i}(:,1:size(CLQR.X{i},2));...
% % %             zeros(1,size(CLQR.X{i},2)); cplexStats.Xopt{i}(:,1:size(CLQR.X{i},2));...
% % %             zeros(1,size(CLQR.X{i},2)); mosekStats.Xopt{i}(:,1:size(CLQR.X{i},2));...
% % %             zeros(1,size(CLQR.X{i},2)); qpgenStats.Xopt{i}(:,1:size(CLQR.X{i},2))]
% % % %         pause;
% % %         [CLQR.U{i}; clqrStats.Uopt{i}(:,1:size(CLQR.U{i},2));...
% % %             zeros(1,size(CLQR.U{i},2)); cplexStats.Uopt{i}(:,1:size(CLQR.U{i},2));...
% % %             zeros(1,size(CLQR.U{i},2)); mosekStats.Uopt{i}(:,1:size(CLQR.U{i},2));...
% % %             zeros(1,size(CLQR.U{i},2)); qpgenStats.Uopt{i}(:,1:size(CLQR.U{i},2))]
end

optValuesRatio = clearNumerics_CLQR_VS_MPC(19999);
plotNumerics(cplexStats, mosekStats, qpgenStats);





