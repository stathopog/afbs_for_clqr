function [] = solveMPC(dat,bar,horizon,counter,perturbation)
% Solve once with the CLQR
    global X_INIT;
    global mpcStats;
% %     bar.cxx = bar.cx-bar.Cx*bar.A*X_INIT(:,counter);
% %     const.c = [];
% %     for jjj = 1:dat.N
% %         const.c = [const.c; bar.cu((jjj-1)*dat.no.con.u+1:jjj*dat.no.con.u,:); bar.cxx((jjj-1)*dat.no.con.x+1:jjj*dat.no.con.x,:)];
% %     end
    delete xinit_data;
    csvwrite('xinit_data.dat',X_INIT(:,counter));
    delete sizes_data;
    fi1 = fopen('sizes_data','w');
    fprintf(fi1,'%u ',dat.nx); fprintf(fi1,'%u ',dat.nu);
    fprintf(fi1,'%u ',dat.px); fprintf(fi1,'%u ',dat.pu); fprintf(fi1,'%u ',dat.px); fprintf(fi1,'%u ',horizon);
    fprintf(fi1,'%6.6f ',dat.beta); fprintf(fi1,'%6.6f ',1);
    fclose(fi1);
%     delete dbar;
%     csvwrite('dbar.dat',const.c);

% Un-comment for no terminal set
    delete H_data;
    csvwrite('H_data.dat',dat.Cx)
    delete hf_data;
    csvwrite('hf_data.dat',dat.cx)

    delete perturb;
    csvwrite('perturb.dat',perturbation);
    % access Cpp folder and run mpc code
    cd ../main_files_mpc/build_test_version_mpc;
    system('make');
    cd Sources_test_version/MPC;
    system('./mpc');
    cd /Users/georgios/Documents/Research/CLQR_via_FBS/Cpp_Code/Matlab_files;
    mpcStats.HORZS{counter} = horizon;
    tempTime = csvread('MPC_TIMES.dat');
    mpcStats.TIMES{counter} = tempTime./1e6;
    mpcStats.ITERS{counter} = csvread('MPC_ITERS.dat');
    mpcStats.Xopt{counter}  = csvread('MPC_x_opt.dat');
    mpcStats.Uopt{counter}  = csvread('MPC_u_opt.dat');
    mpcStats.OptVal{counter}  = csvread('MPC_optval.dat');
end