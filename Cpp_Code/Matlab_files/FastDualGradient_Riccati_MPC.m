function [k,x,u,lambda,rPrimal,rho,val] = FastDualGradient_Riccati_MPC(N,dat,xinit,MAXITER,lambda0,lambdaf)

optTol1 = 1e-5;
% lambda = zeros(no.con.all*N,1);
lambda = lambda0;
old.lambda = lambda;
old.lambdaf = lambdaf;
hat.lambda = lambda;
hat.lambdaf = lambdaf;
Lip = 1e-2;
a = 5;
dat.w = 1;

for k = 1:MAXITER  
%% acceleration step
    alpha = (k-1) / (k+a);
    hat.lambda = lambda + alpha*(lambda-old.lambda);
    hat.lambdaf = lambdaf + alpha*(lambdaf-old.lambdaf);
    [x,u] = TV_Riccati(N,dat,hat.lambda,hat.lambdaf,xinit);
    Lip = Backtracking(xinit, x, u, dat, hat.lambda, hat.lambdaf, N, Lip);
    rho = 1 / Lip; % condition for convergence
    [~, g, gf] = funOracle(x, u, dat, hat.lambda, hat.lambdaf, N);
    old.lambda = lambda;
    lambda = min(0, hat.lambda - rho*g);
    lambdaf = min(0, hat.lambdaf - rho*gf);
    u = reshape(u,dat.nu,N);
    old.x = x; old.u = u;

    res = (-hat.lambda+old.lambda);

    %% terminate 
    if k > 2
        rPrimal = norm(res);  % generalized gradient check
        if  ( rPrimal < optTol1 )
            val = 0;
            for ttt = 1:N
                val = val + 0.5*x(:,ttt)'*dat.Q*x(:,ttt) + 0.5*u(:,ttt)'*dat.R*u(:,ttt);
            end
            val = val + 0.5*x(:,ttt+1)'*dat.S*x(:,ttt+1);
            break; 
        elseif (rPrimal > 1e4) || (k==MAXITER)
            val = 0;
            sprintf('Infeasible problem or too many iterations')
            break;
        end
    end
end
                
        
        
        