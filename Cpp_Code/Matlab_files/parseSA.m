function [QP_reform,alg_data] = parseSA(dat,T)
% Parse for QPgen (splitting algorithm)
 % specify dynamics
        MPC.Adyn = dat.A;
        MPC.Bdyn = dat.B;

        % specify cost
        MPC.Q = dat.Q;
        MPC.R = dat.R;
        MPC.Qf = dat.S;

        % indicate that linear cost is parametric
        MPC.gt = 1;

        % specify hard state constraints
        MPC.Cx = eye(dat.nx);
        MPC.X.Ub = ones(dat.nx,1)*dat.xmax;
        MPC.X.Lb = ones(dat.nx,1)*dat.xmin;

        % specify hard input constraints
        MPC.Cu = eye(dat.nu);
        MPC.U.Ub = ones(dat.nu,1)*dat.umax;
        MPC.U.Lb = ones(dat.nu,1)*dat.umin;

        % control horizon
        MPC.N = T;

        % decrease optimality tolerance
        opts.rel_tol = 1e-5;

        % run code generator
        cvx_solver mosek;
        [QP_reform,alg_data] = run_code_gen_MPC(MPC,opts);
end