function optValuesRatio = clearNumerics_CLQR_VS_MPC(maxiter)
global clqrStats;
global mpcStats;

clqrStats.Infeasible = 0;   mpcStats.Infeasible = 0;
clqr.tempTimes = [];    mpc.tempTimes = [];
clqr.tempIters = [];    mpc.tempIters = [];
validIdx = [];

for i = 1:length(clqrStats.ITERS)
    switch any(clqrStats.ITERS{i}==maxiter) 
        case 1
            clqrStats.Infeasible = clqrStats.Infeasible + 1;
            mpcStats.Infeasible = mpcStats.Infeasible + 1;
        case 0
            if any(mpcStats.ITERS{i}==maxiter) ||...
                    (norm(mpcStats.Xopt{i}(:,1)-clqrStats.Xopt{i}(:,1)) >= 1e-4)
                mpcStats.Infeasible = mpcStats.Infeasible + 1;
            else
                clqr.tempTimes = [clqr.tempTimes; clqrStats.TIMES{i}];
                mpc.tempTimes = [mpc.tempTimes; mpcStats.TIMES{i}];
                clqr.tempIters = [clqr.tempIters; clqrStats.ITERS{i}];
                mpc.tempIters = [mpc.tempIters; mpcStats.ITERS{i}];
                validIdx = [validIdx; i];
            end
     end
end

clqrStats.Iters.mean = mean(clqr.tempIters);
clqrStats.Iters.med = median(clqr.tempIters);
clqrStats.Times.mean = mean(clqr.tempTimes);
clqrStats.Times.med = median(clqr.tempTimes);
mpcStats.Iters.mean = mean(mpc.tempIters);
mpcStats.Iters.med = median(mpc.tempIters);
mpcStats.Times.mean = mean(mpc.tempTimes);
mpcStats.Times.med = median(mpc.tempTimes);
optValuesRatio = cell2mat(clqrStats.OptVal(validIdx))./cell2mat(mpcStats.OptVal(validIdx));
       
end
            
        
        
