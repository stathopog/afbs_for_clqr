function [dat] = upper_bound_beta(T,model,dat)
%%%%%% Computes an upper bound of the stepsize rho = 2 / beta, %%%%%%%%%%%%
%%%%%% where beta <= |C|^2 |H^-1|.                             %%%%%%%%%%%% 

% To compute |C| = sigma(Cu) + Hinf(sys), choose weight w such that A
% is stable
if ( max(eig(model.A))>1 )
    dat.w = 1/(1.1*max(eig(model.A)));
    tempA = dat.w * model.A;
else
    dat.w = 1;
    tempA = model.A;
end
m = size(model.u.penalty.H,1);
sys = ss(tempA,model.B,dat.Cx,zeros(size(dat.Cx,1),m),1);
sigmas = sigma(sys);
for i = 1:m
    sigmas = max(sigmas);
end
Hinf = sigmas;


% Hinf = eps;
% for i = 1:m
%     H{i} = ss2tf(A,B,Cx,zeros(size(Cx,1),m),i);
%     Hinf = max(Hinf,norm(H{i},inf));
% end
Cnorm = norm(dat.Cu,2) + Hinf;

% Need |B'QB| <= |B|^2|Q| < min(eig(R))
% if ( min(eig(Q)) == 0 )
%     for w2 = 1:-1e-3:1e-6
%         Atest = w2 * A; 
%         H2 = ss2tf(Atest,B,eye(size(Atest,1)),zeros(size(Atest,1),1));
%         H2inf = norm(H2,inf);
%         if ( H2inf <= sqrt( 0.95*min(eig(R)) / max(eig(Q)) ) )
%             Bnorm = sqrt( 0.95*min(eig(R)) / max(eig(Q)) );
%             A = Atest;
%             % Re-compute weight as w = min{w1,w2}
%             H1 = ss2tf(A,B,Cx,zeros(size(Cx,1),1));
%             H1inf = norm(H1,inf);
%             Cnorm = norm(Cu,2) + H1inf;
%             invRnorm = 1/min(eig(R));
%             invHnorm = invRnorm / (1-invRnorm*Bnorm^2*max(eig(Q)));
%             w = min(w1,w2);
%             break;
%         end
%     end
% else
%     invHnorm = invRnorm * ;
% end

dat.beta = Cnorm^2 * (1 / min(eig(model.u.penalty.H)));

for iii = 1:T
    dat.W((iii-1)*dat.no.con.all+1:iii*dat.no.con.all) = ones(dat.no.con.all,1)*dat.w^(iii-1);
end
            
            
    




