function [con,yal_obj,yal_xinit,yal_x,yal_u] = parseIPM(dat,T,terminal)
% Parse for Yalmip
        yal_x = sdpvar(dat.nx,T+1);
        yal_u = sdpvar(dat.nu,T+1);
        yal_xinit = sdpvar(dat.nx,1);
        con = []; 
        con = [con,yal_u <= dat.umax];
        con = [con,yal_u >= dat.umin];
        con = [con,yal_x <= dat.xmax];
        con = [con,yal_x >= dat.xmin];
        yal_obj = 0;
        for t = 1:T
            yal_obj = yal_obj + .5*yal_x(:,t)'*dat.Q*yal_x(:,t) + .5*yal_u(:,t)'*dat.R*yal_u(:,t);
        end
        yal_obj = yal_obj + 0.5*yal_x(:,T+1)'*dat.S*yal_x(:,T+1);
        con = [con,yal_x(:,2:end) == dat.A*yal_x(:,1:end-1) + dat.B*yal_u(:,1:end-1)];
        con = [con,yal_x(:,1) == yal_xinit]; 
end