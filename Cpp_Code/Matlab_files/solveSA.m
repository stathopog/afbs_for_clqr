function [solver] = solveSA(dat,counter,simLength,MPC,solver,clqr,T,pert)
% Solve with QPgen
        global X_INIT;
        X = zeros(dat.nx,simLength);  U = zeros(dat.nu,simLength);
        gt = zeros(T*(length(MPC.Q)+length(MPC.R)),1);
        tStart = tic;
        kkk = 1;
        X(:,1) = X_INIT(:,counter);
        qpgen_xinit = X_INIT(:,counter);
        Iter = 0;
        while (any(dat.Hf*qpgen_xinit-dat.hf>0))
            kkk = kkk + 1;
            disp(kkk)
            [sol,iter] = qp_mex(gt,qpgen_xinit);
            Iter = Iter + iter;
            % Store trajectories
            PROBS(counter) = kkk; % number of OCP solved
            X(:,kkk) = sol(1:dat.nx,1);
            U(:,kkk-1) = sol(T*dat.nx+1:T*dat.nx+dat.nu,1);
            qpgen_xinit = sol(1:dat.nx,1)+pert(kkk)*sol(1:dat.nx,1);
        end
        solver.tElapsed = toc(tStart);
        solver.averageTime = toc(tStart)/kkk;
        solver.Xopt  = X;   solver.Uopt  = U;
        solver.errorX = norm(X-clqr.Xopt{counter}) / norm(X);
        solver.errorU = norm(U-clqr.Uopt{counter}) / norm(U);
        solver.averageIter = Iter/kkk;
end